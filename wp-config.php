<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'acj_indochine');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'i^7Aq={(6VdT]@V.RR-tO<hNG 9=yh7.<C)YC,$p|K Z(@/.9c^M9Y`dLqwILKt{');
define('SECURE_AUTH_KEY',  '/.GPhM6]{>YHBA-Q&:4A2(jiT9ujwe;`a>?&-#{K_L9<XlO s][BZf)fckiwqar2');
define('LOGGED_IN_KEY',    'B~8cbY8*nEm7<~_n+(#un/xBPH;b/?R)S8oiOHl[qeMGJ6h/-*y#1s/y|zbhB|en');
define('NONCE_KEY',        ';e3I+U}d^%1/Ug}wOG|ltW})B7h)zRt>b#bKX[7p+Rl.INOG#BA9]q|BcTEcIc(i');
define('AUTH_SALT',        '}(q}4n~9ZP#kuzf9!?J#H/kB=LSC@D&vKqS?yfNM|Wm;Cf8q4^v_l+)b4:!2}X^h');
define('SECURE_AUTH_SALT', 'IhAP5-$-[a_2oFxa-+2>/)H|>`FGdxbDk2(*gUUrXAN}b-@r).?DhZA0p3thQD%g');
define('LOGGED_IN_SALT',   '2@il[*dX;U%e6}`!dxRTQ?wZ+$mb55#&raa#4L^DTq.9mjEzXB|$eVtI-|w-oKvv');
define('NONCE_SALT',       '0A*%3U!KtFM|)*|#AUOM.l#{AlrpCA.G?TO%R0!-jIItjOQ1>+-jFM*gK@SVWwl+');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'acj_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
