		<footer class="footer-black">
			<div class="container footer-info">
				<div class="col-md-4">
					<div class="footer-info-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-phone-icon.png" alt="Phone" />
					</div>
					<div class="footer-info-text">
						<h4><?php _e( "Số điện thoại", THEMEDOMAIN ) ?></h4>
						<p><?php
							if(function_exists('ot_get_option')){
								echo ot_get_option('so_dien_thoai');
							}
							?></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4">
					<div class="footer-info-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-map-marker-icon.png" alt="Địa chỉ" />
					</div>
					<div class="footer-info-text">
						<h4><?php _e( "Địa chỉ", THEMEDOMAIN ) ?></h4>
						<p><?php
							if(function_exists('ot_get_option')){
								echo ot_get_option('dia_chi');
							}
							?></p>
					</div>
					<div class="clear"></div>
				</div>
				<div class="col-md-4">
					<div class="footer-info-icon">
						<img src="<?php echo get_template_directory_uri(); ?>/assets/images/footer-email-icon.png" alt="Email" />
					</div>
					<div class="footer-info-text">
						<h4><?php _e( "Thư điện tử", THEMEDOMAIN ) ?></h4>
						<p><?php
							if(function_exists('ot_get_option')){
								echo ot_get_option('thu_dien_tu');
							}
							?></p>
					</div>
					<div class="clear"></div>
				</div>
			</div>
		</footer>
		<footer class="footer-widget">
			<div class="container">
				<?php if ( is_active_sidebar( 'footer-area' ) ) : ?>
					<?php dynamic_sidebar( 'footer-area' ); ?>
				<?php endif; ?>
			</div>
		</footer>
		<footer class="footer-black">
			<div class="container text-center">
				<p><?php
					if(function_exists('ot_get_option')){
						echo ot_get_option('copyright');
					}
					?></p>
				<div class="footer-menu">
					<?php
					$defaults = array(
						'theme_location'  => 'footer',
						'menu'            => '',
						'container'       => 'ul',
						'container_class' => '',
						'container_id'    => '',
						'menu_class'      => '',
						'menu_id'         => 'footer-menu-bottom',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'before'          => '',
						'after'           => '',
						'link_before'     => '',
						'link_after'      => '',
						'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
						'depth'           => 0,
						'walker'          => ''
					);

					wp_nav_menu( $defaults );
					?>
				</div><!-- END .footer-menu -->
			</div>
		</footer>
		<footer class="footer-red">

		</footer>
		<?php wp_footer() ?>
		<script type="application/javascript">
			jQuery(document).ready(function($){
				$('a').tooltip()
			});
		</script>
	</body>
</html>