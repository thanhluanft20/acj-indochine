msgid ""
msgstr ""
"Project-Id-Version: ACJ Theme\n"
"POT-Creation-Date: 2016-01-26 15:00+0700\n"
"PO-Revision-Date: 2016-01-26 15:00+0700\n"
"Last-Translator: \n"
"Language-Team: \n"
"Language: vi\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Poedit 1.8.6\n"
"X-Poedit-Basepath: ..\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Poedit-SourceCharset: UTF-8\n"
"X-Poedit-KeywordsList: __;_e;_x\n"
"X-Poedit-SearchPath-0: .\n"

#: content-search-news.php:36 templates/tintuc.php:36
msgid "Tin tức mới nhất"
msgstr ""

#: content-search-product.php:28 woocommerce/archive-product.php:28
#: woocommerce/single-product.php:28
msgid "Sản phẩm"
msgstr ""

#: content-search-product.php:29 woocommerce/archive-product.php:29
#: woocommerce/single-product.php:29
msgid ""
"ACJ INDOCHINE hân hạnh giới thiệu đến quý khách các sản phẩm của công ty"
msgstr ""

#: content-single-tuvan-kythuat.php:54 content-single.php:62
msgid "TIN TỨC GẦN ĐÂY"
msgstr ""

#: content-single-tuvan-kythuat.php:67
msgid "Đọc thêm"
msgstr ""

#: content-single.php:27
msgid "Chia Sẻ"
msgstr ""

#: footer.php:8
msgid "Số điện thoại"
msgstr ""

#: footer.php:17
msgid "Địa chỉ"
msgstr ""

#: footer.php:27
msgid "Thư điện tử"
msgstr ""

#: functions.php:87
msgid "Menu chính"
msgstr ""

#: functions.php:88
msgid "Menu cuối trang"
msgstr ""

#: functions.php:132
msgid "Email Khách Hàng"
msgstr ""

#: functions.php:134 functions.php:144 functions.php:153
msgid "Tất cả các khung bạn muốn đặt ở dưới trang"
msgstr ""

#: functions.php:142
msgid "Khu vực cuối trang"
msgstr ""

#: functions.php:151
msgid "Thanh bên trang dịch vụ"
msgstr ""

#: functions.php:160
msgid "Thanh bên trang sản phẩm"
msgstr ""

#: functions.php:162
msgid "Tất cả các khung bạn muốn đặt ở bên trái trang sản phẩm"
msgstr ""

#: functions.php:183
msgid "Tùy chỉnh giao diện"
msgstr ""

#: functions.php:202 functions.php:203 functions.php:204 functions.php:205
#: functions.php:206 functions.php:229
msgid "Tư Vấn Kỹ Thuật"
msgstr ""

#: functions.php:207
msgid "Tư Vấn Kỹ Thuật hiện tại:"
msgstr ""

#: functions.php:208
msgid "Tất cả"
msgstr ""

#: functions.php:209
msgid "Thêm mới tư vấn kỹ thuật"
msgstr ""

#: functions.php:210 functions.php:304
msgid "Thêm mới"
msgstr ""

#: functions.php:211
msgid "Tư vấn kỹ thuật mới"
msgstr ""

#: functions.php:212
msgid "Sửa"
msgstr ""

#: functions.php:213
msgid "Cập nhật"
msgstr ""

#: functions.php:214
msgid "Xem"
msgstr ""

#: functions.php:215
msgid "Tìm kiếm"
msgstr ""

#: functions.php:216 functions.php:310 functions.php:359 functions.php:407
msgid "Không tìm thấy"
msgstr ""

#: functions.php:217 functions.php:311 functions.php:360 functions.php:408
msgid "Không tìm thấy trong thùng rác"
msgstr ""

#: functions.php:218 functions.php:312 functions.php:361 functions.php:409
msgid "Hình ảnh nổi bật"
msgstr ""

#: functions.php:219 functions.php:313 functions.php:362 functions.php:410
msgid "Đặt hình ảnh nổi bật"
msgstr ""

#: functions.php:220 functions.php:314 functions.php:363 functions.php:411
msgid "Xóa hình ảnh nổi bật"
msgstr ""

#: functions.php:221
msgid "Sử dụng hình này làm hình ảnh nổi bật"
msgstr ""

#: functions.php:222
msgid "Chèn thêm"
msgstr ""

#: functions.php:223
msgid "Đăng tải tư vấn kỹ thuật này"
msgstr ""

#: functions.php:224
msgid "Danh sách tư vấn kỹ thuật"
msgstr ""

#: functions.php:225
msgid "Danh mục tư vấn kỹ thuật"
msgstr ""

#: functions.php:226
msgid "Lọc danh sách tư vấn kỹ thuật"
msgstr ""

#: functions.php:230
msgid "Tất cả thông tin về tư vấn và kỹ thuật"
msgstr ""

#: functions.php:249 functions.php:250 functions.php:251 functions.php:252
#: functions.php:276 functions.php:277
msgid "Sliders"
msgstr ""

#: functions.php:253
msgid "Slider Archives"
msgstr ""

#: functions.php:254
msgid "Parent Slider:"
msgstr ""

#: functions.php:255
msgid "All Sliders"
msgstr ""

#: functions.php:256
msgid "Add New Slider"
msgstr ""

#: functions.php:257
msgid "Add New"
msgstr ""

#: functions.php:258
msgid "New Slider"
msgstr ""

#: functions.php:259
msgid "Edit Slider"
msgstr ""

#: functions.php:260
msgid "Update Slider"
msgstr ""

#: functions.php:261
msgid "View Slider"
msgstr ""

#: functions.php:262
msgid "Search Slider"
msgstr "Tìm kiếm slider"

#: functions.php:263
msgid "Not found"
msgstr "Không tìm thấy"

#: functions.php:264
msgid "Not found in Trash"
msgstr "Không tìm thấy trong giỏ hàng"

#: functions.php:265
msgid "Featured Image"
msgstr "Hình ảnh đại diện"

#: functions.php:266
msgid "Set featured image"
msgstr "Đặt hình ảnh đại diện"

#: functions.php:267
msgid "Remove featured image"
msgstr "Xóa hình ảnh đại diện"

#: functions.php:268
msgid "Use as featured image"
msgstr "Sử dụng hình hình ảnh đại diện"

#: functions.php:269
msgid "Insert into item"
msgstr "Thêm item"

#: functions.php:270
msgid "Uploaded to this item"
msgstr "Đăng mặt hàng này"

#: functions.php:271
msgid "Sliders list"
msgstr "Danh sách slider"

#: functions.php:272
msgid "Sliders list navigation"
msgstr ""

#: functions.php:273
msgid "Filter sliders list"
msgstr ""

#: functions.php:296 functions.php:297 functions.php:298 functions.php:299
#: functions.php:323 functions.php:324
msgid "Tuyển dụng"
msgstr ""

#: functions.php:300
msgid "Tuyển dụng Lưu trữ"
msgstr ""

#: functions.php:301
msgid "Tuyển dụng hiện tại:"
msgstr ""

#: functions.php:302
msgid "Tất cả tuyển dụng"
msgstr ""

#: functions.php:303
msgid "Thêm mới tuyển dụng"
msgstr ""

#: functions.php:305
msgid "Tuyển dụng mới"
msgstr ""

#: functions.php:306
msgid "Sửa tuyển dụng"
msgstr ""

#: functions.php:307
msgid "Cập nhật tuyển dụng"
msgstr ""

#: functions.php:308
msgid "Xem tuyển dụng"
msgstr ""

#: functions.php:309
msgid "Tìm kiếm tuyển dụng"
msgstr ""

#: functions.php:315 functions.php:364 functions.php:412
msgid "Sử dụng với hình ảnh nổi bật"
msgstr ""

#: functions.php:316 functions.php:365
msgid "Chèn tin tuyển dụng"
msgstr ""

#: functions.php:317 functions.php:366 functions.php:414
msgid "Đăng tin này"
msgstr ""

#: functions.php:318 functions.php:367 functions.php:415
msgid "Danh sách tin"
msgstr ""

#: functions.php:319
msgid "Danh sách menu tuyển dụng"
msgstr ""

#: functions.php:320
msgid "Lọc tin tuyển dụng"
msgstr ""

#: functions.php:345 functions.php:346 functions.php:347 functions.php:348
#: functions.php:372 inc/widgets/class-dich-vu-widget.php:15
#: inc/widgets/class-dich-vu-widget.php:16
msgid "Dịch Vụ"
msgstr ""

#: functions.php:349
msgid "Dịch vụ lưu trử"
msgstr ""

#: functions.php:350
msgid "Dịch vụ cha"
msgstr ""

#: functions.php:351 single-tin-dich-vu.php:35
msgid "Tất cả dịch vụ"
msgstr ""

#: functions.php:352 functions.php:354
msgid "Thêm Mới dịch vụ"
msgstr ""

#: functions.php:353 functions.php:401
msgid "Thêm Mới "
msgstr ""

#: functions.php:355
msgid "Sửa dịch vụ"
msgstr ""

#: functions.php:356
msgid "Cập nhật dịch vụ"
msgstr ""

#: functions.php:357
msgid "Xem dịch vụ"
msgstr ""

#: functions.php:358
msgid "Tìm kiếm dịch vụ"
msgstr ""

#: functions.php:368
msgid "Danh sách menu dịch vụ"
msgstr ""

#: functions.php:369
msgid "Lọc tin dịch vụ"
msgstr ""

#: functions.php:373
msgid "Post Type Description"
msgstr "Mô tả ngắn "

#: functions.php:393 functions.php:394 functions.php:395 functions.php:396
#: functions.php:420 functions.php:727
msgid "Kết quả kiểm tra"
msgstr ""

#: functions.php:397
msgid "Kết quả kiểm tra lưu trử"
msgstr ""

#: functions.php:398
msgid "Kết quả kiểm tra cha"
msgstr ""

#: functions.php:399
msgid "Tất cả Kết quả kiểm tra"
msgstr ""

#: functions.php:400 functions.php:402
msgid "Thêm Mới Kết quả kiểm tra"
msgstr ""

#: functions.php:403
msgid "Sửa Kết quả kiểm tra"
msgstr ""

#: functions.php:404
msgid "Cập nhật Kết quả kiểm tra"
msgstr ""

#: functions.php:405
msgid "Xem Kết quả kiểm tra"
msgstr ""

#: functions.php:406
msgid "Tìm kiếm Kết quả kiểm tra"
msgstr ""

#: functions.php:413
msgid "Chèn tin Kết quả kiểm tra"
msgstr ""

#: functions.php:416
msgid "Danh sách menu Kết quả kiểm tra"
msgstr ""

#: functions.php:417
msgid "Lọc tin Kết quả kiểm tra"
msgstr ""

#: functions.php:421
msgid "Mô tả kết quả kiểm tra"
msgstr ""

#: functions.php:442
msgid "Tùy chỉnh tuyển dụng"
msgstr ""

#: functions.php:449
msgid "Tùy chỉnh dịch vụ"
msgstr ""

#: functions.php:456
msgid "Icon trang chủ"
msgstr ""

#: functions.php:463
msgid "Tùy chỉnh trang"
msgstr ""

#: functions.php:469
msgid "Tùy chỉnh kết quả kiểm tra"
msgstr ""

#: functions.php:483 functions.php:541
msgid "Chọn Icon"
msgstr ""

#: functions.php:519 templates/tuyendung.php:36
msgid "Vị trí"
msgstr ""

#: functions.php:523 templates/tuyendung.php:37 woocommerce/cart/cart.php:30
msgid "Số lượng"
msgstr ""

#: functions.php:527 templates/tuyendung.php:38
msgid "Trạng thái"
msgstr ""

#: functions.php:566 functions.php:846 functions.php:878
#: inc/product-custom.php:136 inc/taxonomy-options.php:158
#: inc/taxonomy-options.php:232
msgid "Choose an image"
msgstr "Chọn hình ảnh"

#: functions.php:568 functions.php:848 functions.php:880
#: inc/product-custom.php:138 inc/taxonomy-options.php:160
#: inc/taxonomy-options.php:234
msgid "Use image"
msgstr "Sử dụng hình này"

#: functions.php:641
msgid "Tiêu đề"
msgstr ""

#: functions.php:645
msgid "Mô tả điều hướng"
msgstr ""

#: functions.php:654
msgid "Trang chủ"
msgstr ""

#: functions.php:715 inc/product-custom.php:5
msgid "Hiệu suất"
msgstr ""

#: functions.php:721 inc/taxonomy-options.php:96 inc/taxonomy-options.php:97
#: inc/taxonomy-options.php:106
msgid "Kích thước"
msgstr ""

#: functions.php:772
msgid "Kiểm tra cho lốp xe"
msgstr ""

#: functions.php:775
msgid "Chọn lốp xe"
msgstr ""

#: functions.php:785
msgid "Ngày kiểm tra"
msgstr ""

#: functions.php:791
msgid "Thứ hạng"
msgstr ""

#: functions.php:797
msgid "Hãng kiểm tra"
msgstr ""

#: functions.php:803
msgid "Logo hãng kiểm tra"
msgstr ""

#: functions.php:808 functions.php:824 inc/product-custom.php:37
#: inc/product-custom.php:83 inc/taxonomy-options.php:133
#: inc/taxonomy-options.php:207
msgid "Upload/Add image"
msgstr "Đăng/Thêm hình ảnh"

#: functions.php:813
msgid "Tên xe kiểm tra"
msgstr ""

#: functions.php:819
msgid "Hình xe minh họa"
msgstr ""

#: header.php:64
msgid "Ngôn ngữ"
msgstr ""

#: header.php:113
msgid "Đường dây nóng"
msgstr ""

#: inc/product-custom.php:10
msgid "Thông tin thêm"
msgstr ""

#: inc/product-custom.php:31 inc/product-custom.php:77
msgid "Icon"
msgstr ""

#: inc/product-custom.php:38 inc/product-custom.php:84
#: inc/taxonomy-options.php:134 inc/taxonomy-options.php:208
msgid "Remove image"
msgstr "Xóa hình ảnh"

#: inc/product-custom.php:43 inc/product-custom.php:89
msgid "Tên hiệu suất:"
msgstr ""

#: inc/product-custom.php:47 inc/product-custom.php:93
msgid "Bình chọn:"
msgstr ""

#: inc/product-custom.php:204 woocommerce/content-single-product.php:63
msgid "Loại lốp"
msgstr ""

#: inc/product-custom.php:208 inc/product-custom.php:212
msgid "Extreme Conditions"
msgstr "Điều kiện khắc nghiệt"

#: inc/product-custom.php:214 sidebar-shop.php:50
msgid "Năm sản xuất"
msgstr ""

#: inc/product-search.php:6
msgid "Tìm kiếm lốp xe"
msgstr ""

#: inc/product-search.php:9 inc/widgets/class-san-pham-widget.php:18
msgid "Sản Phẩm"
msgstr ""

#: inc/taxonomy-options.php:18 inc/taxonomy-options.php:19
#: inc/taxonomy-options.php:28 woocommerce/content-single-product.php:62
msgid "Loại xe"
msgstr ""

#: inc/taxonomy-options.php:20
msgid "Tìm kiếm loại xe"
msgstr ""

#: inc/taxonomy-options.php:21
msgid "Tất cả loại xe"
msgstr ""

#: inc/taxonomy-options.php:22
msgid "Loại xe cha"
msgstr ""

#: inc/taxonomy-options.php:23
msgid "Loại xe cha:"
msgstr ""

#: inc/taxonomy-options.php:24
msgid "Sửa loại xe"
msgstr ""

#: inc/taxonomy-options.php:25
msgid "Cập nhật loại xe"
msgstr ""

#: inc/taxonomy-options.php:26 inc/taxonomy-options.php:27
msgid "Thêm mới loại xe"
msgstr ""

#: inc/taxonomy-options.php:44 inc/taxonomy-options.php:45
#: inc/taxonomy-options.php:54 index.php:78 sidebar-shop.php:32
#: templates/home.php:93
msgid "Hãng xe"
msgstr ""

#: inc/taxonomy-options.php:46
msgid "Tìm kiếm hãng xe"
msgstr ""

#: inc/taxonomy-options.php:47
msgid "Tất cả hãng xe"
msgstr ""

#: inc/taxonomy-options.php:48
msgid "Hãng xe cha"
msgstr ""

#: inc/taxonomy-options.php:49
msgid "Hãng xe cha:"
msgstr ""

#: inc/taxonomy-options.php:50
msgid "Sửa hãng xe"
msgstr ""

#: inc/taxonomy-options.php:51
msgid "Cập nhật hãng xe"
msgstr ""

#: inc/taxonomy-options.php:52 inc/taxonomy-options.php:53
msgid "Thêm mới hãng xe"
msgstr ""

#: inc/taxonomy-options.php:70 inc/taxonomy-options.php:71
#: inc/taxonomy-options.php:80 index.php:83 sidebar-shop.php:41
#: templates/home.php:104
msgid "Mẫu xe"
msgstr ""

#: inc/taxonomy-options.php:72
msgid "Tìm kiếm mẫu xe"
msgstr ""

#: inc/taxonomy-options.php:73
msgid "Tất cả mẫu xe"
msgstr ""

#: inc/taxonomy-options.php:74
msgid "Mẫu xe cha"
msgstr ""

#: inc/taxonomy-options.php:75
msgid "Mẫu xe cha:"
msgstr ""

#: inc/taxonomy-options.php:76
msgid "Sửa mẫu xe"
msgstr ""

#: inc/taxonomy-options.php:77
msgid "Cập nhật mẫu xe"
msgstr ""

#: inc/taxonomy-options.php:78 inc/taxonomy-options.php:79
msgid "Thêm mới mẫu xe"
msgstr ""

#: inc/taxonomy-options.php:98
msgid "Tìm kiếm kích thước"
msgstr ""

#: inc/taxonomy-options.php:99
msgid "Tất cả kích thước"
msgstr ""

#: inc/taxonomy-options.php:100
msgid "Kích thước cha"
msgstr ""

#: inc/taxonomy-options.php:101
msgid "Kích thước cha:"
msgstr ""

#: inc/taxonomy-options.php:102
msgid "Sửa kích thước"
msgstr ""

#: inc/taxonomy-options.php:103
msgid "Cập nhật kích thước"
msgstr ""

#: inc/taxonomy-options.php:104 inc/taxonomy-options.php:105
msgid "Thêm mới kích thước"
msgstr ""

#: inc/taxonomy-options.php:129 inc/taxonomy-options.php:203
msgid "Thumbnail"
msgstr ""

#: inc/widgets/class-dich-vu-widget.php:74
#: inc/widgets/class-kien-thuc-widget.php:74
#: inc/widgets/class-san-pham-widget.php:99
msgid "New title"
msgstr ""

#: inc/widgets/class-dich-vu-widget.php:78
#: inc/widgets/class-kien-thuc-widget.php:78
#: inc/widgets/class-san-pham-widget.php:103
msgid "Title:"
msgstr ""

#: inc/widgets/class-kien-thuc-widget.php:15
#: inc/widgets/class-kien-thuc-widget.php:16
msgid "Kiến Thức"
msgstr ""

#: inc/widgets/class-san-pham-widget.php:19
msgid "wedget này hiển thị các sản phẩm..."
msgstr ""

#: index.php:23 templates/home.php:28
msgid "Chi tiết"
msgstr ""

#: index.php:51 templates/home.php:56
msgid "TÌM LỐP XE PHÙ HỢP"
msgstr ""

#: index.php:55 sidebar-shop.php:5 templates/home.php:60
msgid "Tìm theo xe"
msgstr ""

#: index.php:56 sidebar-shop.php:61 templates/home.php:61
msgid "Tìm theo size"
msgstr ""

#: index.php:64
msgid "Lốp xe tải"
msgstr ""

#: index.php:68
msgid "Lốp ôtô"
msgstr ""

#: index.php:72
msgid "Lốp xe máy"
msgstr ""

#: index.php:88 templates/home.php:115
msgid "Năm sản xuất xe"
msgstr ""

#: index.php:91 templates/home.php:121
msgid "Tìm lốp xe"
msgstr ""

#: index.php:112 templates/home.php:188
msgid "Các sản phẩm và dịch vụ"
msgstr ""

#: index.php:142
msgid "CHÀO MỪNG ĐẾN VỚI ACJ INDOCHINE"
msgstr ""

#: index.php:199 templates/home.php:226
msgid "Sản phẩm mới"
msgstr ""

#: sidebar-shop.php:2
msgid "Tìm lốp xe phù hợp"
msgstr ""

#: sidebar-shop.php:55 sidebar-shop.php:83 templates/home.php:146
msgid "TÌM LỐP XE"
msgstr ""

#: single-tuvankythuat.php:38 single.php:41
msgid "Tin tức đọc nhiều nhất"
msgstr ""

#: single.php:90 templates/home.php:259
msgid "Bán chạy nhất"
msgstr ""

#: templates/home.php:249
msgid "No products found"
msgstr ""

#: templates/lienlac.php:43
msgid "Form liên lạc"
msgstr ""

#: templates/tuyendung.php:35
msgid "Ngày tạo"
msgstr ""

#: woocommerce/cart/cart-empty.php:18
msgid "Your cart is currently empty."
msgstr ""

#: woocommerce/cart/cart-empty.php:22
msgid "Return To Shop"
msgstr ""

#: woocommerce/cart/cart-shipping.php:20
#, php-format
msgid "Shipping #%d"
msgstr ""

#: woocommerce/cart/cart-shipping.php:22 woocommerce/cart/cart-shipping.php:97
msgid "Shipping"
msgstr ""

#: woocommerce/cart/cart-shipping.php:59
msgid "Please use the shipping calculator to see available shipping methods."
msgstr ""

#: woocommerce/cart/cart-shipping.php:63
msgid ""
"Please continue to the checkout and enter your full address to see if there "
"are any available shipping methods."
msgstr ""

#: woocommerce/cart/cart-shipping.php:67
msgid "Please fill in your details to see available shipping methods."
msgstr ""

#: woocommerce/cart/cart-shipping.php:76 woocommerce/cart/cart-shipping.php:82
msgid ""
"There are no shipping methods available. Please double check your address, "
"or contact us if you need any help."
msgstr ""

#: woocommerce/cart/cart-totals.php:17
msgid "TC: "
msgstr ""

#: woocommerce/cart/cart-totals.php:29
#, php-format
msgid " (taxes estimated for %s)"
msgstr ""

#: woocommerce/cart/cart-totals.php:32
#, php-format
msgid ""
"Note: Shipping and taxes are estimated%s and will be updated during checkout "
"based on your billing and shipping information."
msgstr ""

#: woocommerce/cart/cart.php:26
msgid "SST"
msgstr ""

#: woocommerce/cart/cart.php:28
msgid "Tên sản phẩm"
msgstr ""

#: woocommerce/cart/cart.php:29
msgid "Đơn giá"
msgstr ""

#: woocommerce/cart/cart.php:31
msgid "Tổng"
msgstr ""

#: woocommerce/cart/cart.php:52 woocommerce/cart/mini-cart.php:40
msgid "Remove this item"
msgstr ""

#: woocommerce/cart/cart.php:144
msgid "Mua Tiếp"
msgstr ""

#: woocommerce/cart/cross-sells.php:40
msgid "You may be interested in&hellip;"
msgstr ""

#: woocommerce/cart/mini-cart.php:19
msgid "Giỏ hàng"
msgstr ""

#: woocommerce/cart/mini-cart.php:19
msgid "sản phẩm"
msgstr "sản phẩm"

#: woocommerce/cart/mini-cart.php:63
msgid "Subtotal"
msgstr "Tổng cộng "

#: woocommerce/cart/mini-cart.php:70
msgid "View Cart"
msgstr "Xem giỏ hàng"

#: woocommerce/cart/mini-cart.php:72
msgid "Checkout"
msgstr "Thanh toán"

#: woocommerce/cart/mini-cart.php:76
msgid "No products in the cart."
msgstr ""

#: woocommerce/cart/proceed-to-checkout-button.php:16
msgid "Proceed to Checkout"
msgstr ""

#: woocommerce/cart/shipping-calculator.php:24
msgid "Calculate Shipping"
msgstr ""

#: woocommerce/cart/shipping-calculator.php:30
msgid "Select a country&hellip;"
msgstr ""

#: woocommerce/cart/shipping-calculator.php:54
msgid "Select a state&hellip;"
msgstr ""

#: woocommerce/cart/shipping-calculator.php:87
msgid "Update Totals"
msgstr ""

#: woocommerce/global/form-login.php:26
msgid "Username or email"
msgstr ""

#: woocommerce/global/form-login.php:30
msgid "Password"
msgstr ""

#: woocommerce/global/form-login.php:42
msgid "Remember me"
msgstr ""

#: woocommerce/global/form-login.php:46
msgid "Lost your password?"
msgstr ""

#: woocommerce/loop/no-products-found.php:17
msgid "No products were found matching your selection."
msgstr ""

#: woocommerce/loop/result-count.php:30
msgid "Showing the single result"
msgstr ""

#: woocommerce/loop/result-count.php:32
#, php-format
msgid "Showing all %d results"
msgstr ""

#: woocommerce/loop/result-count.php:34
#, php-format
msgid "Showing %1$d&ndash;%2$d of %3$d results"
msgstr ""

#: woocommerce/loop/sale-flash.php:19
#: woocommerce/single-product/sale-flash.php:19
msgid "Sale!"
msgstr ""

#: woocommerce/single-product/add-to-cart/simple.php:50
msgid "Hỗ trợ nhanh"
msgstr ""

#: woocommerce/single-product/add-to-cart/variable.php:23
msgid "This product is currently out of stock and unavailable."
msgstr ""

#: woocommerce/single-product/add-to-cart/variable.php:34
msgid "Clear selection"
msgstr ""

#: woocommerce/single-product/meta.php:26
msgid "SKU:"
msgstr ""

#: woocommerce/single-product/meta.php:26
msgid "N/A"
msgstr ""

#: woocommerce/single-product/product-attributes.php:29
msgid "Weight"
msgstr ""

#: woocommerce/single-product/product-attributes.php:36
msgid "Dimensions"
msgstr ""

#: woocommerce/single-product/product-image.php:37
msgid "Placeholder"
msgstr ""

#: woocommerce/single-product/rating.php:27
#, php-format
msgid "Rated %s out of 5"
msgstr ""

#: woocommerce/single-product/rating.php:29
#, php-format
msgid "out of %s5%s"
msgstr ""

#: woocommerce/single-product/related.php:42
msgid "Related Products"
msgstr ""

#: woocommerce/single-product/review.php:29
#, php-format
msgid "Rated %d out of 5"
msgstr ""

#: woocommerce/single-product/review.php:30
msgid "out of 5"
msgstr ""

#: woocommerce/single-product/review.php:37
msgid "Your comment is awaiting approval"
msgstr ""

#: woocommerce/single-product/review.php:46
msgid "verified owner"
msgstr ""

#: woocommerce/single-product/tabs/additional-information.php:16
msgid "Additional Information"
msgstr "Thông tin thêm"

#: woocommerce/single-product/tabs/description.php:16
msgid "Product Description"
msgstr "Mô tả sản phẩm"

#: woocommerce/single-product/tabs/hieu-suat.php:5
msgid "Không có thông tin về hiệu suất cho sản phẩm này"
msgstr ""

#: woocommerce/single-product/tabs/hieu-suat.php:46
msgid "STANDARD"
msgstr ""

#: woocommerce/single-product/tabs/hieu-suat.php:47
msgid "GOOD"
msgstr ""

#: woocommerce/single-product/tabs/hieu-suat.php:48
msgid "OPTIUM"
msgstr ""

#: woocommerce/single-product/tabs/hieu-suat.php:49
msgid "EXCELLENT"
msgstr ""

#: woocommerce/single-product/tabs/hieu-suat.php:50
msgid "SUPERIOR"
msgstr ""

#: woocommerce/single-product/tabs/ket-qua-kiem-tra.php:51
msgid "Tìm hiểu thêm"
msgstr ""

#: woocommerce/single-product/tabs/ket-qua-kiem-tra.php:64
msgid "Quay lại"
msgstr ""

#: woocommerce/single-product/up-sells.php:43
msgid "You may also like&hellip;"
msgstr ""
