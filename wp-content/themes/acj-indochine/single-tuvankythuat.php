<?php get_header(); ?>
    <div class="container-flue">
        <div class="top-banner">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-top-tintuc.png" alt=""/>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-5 col-md-5 col-sm-6 col-xm-6 column">
                        <h3 class="title">Tin tức</h3>
                        <p class="description">ACJ INDOCHINE chia sẻ đến quí vị nhưng thông tin bổ ích</p>
                    </div>
                    <div class="col-lg-7 col-md-7 col-sm-6 col-xm-6 column">
                        <ol class="breadcrumb">
                            <li><a href="#">Home</a></li>
                            <li><a href="#">Tin tức</a></li>
                        </ol>
                    </div>
                </div>

            </div>
        </div>
    </div>
    <div class="container tintuc">
        <div id="main" class="main col-md-9">
            <?php get_template_part('content','single');?>
        </div><!--END main-->

        <div id="sidebar" class="sidebar col-md-3">
            <div class="widget">
                <div class="search">
                    <input type="text" name="search" id="search" placeholder="Nhập từ khoá tìm kiếm..."/>
                    <button type="submit"><span class="glyphicon glyphicon-search"></span></button>
                </div>
            </div>
            <div class="widget tintuc">
                <div class="heading-title">
                    <h3><span><?php _e('Tin tức đọc nhiều nhất', THEMEDOMAIN)?></span></h3>
                </div>
                <div class="ul-widget">
                    <ul class="ul-widget-info">
                        <?php
                        $argc = array(
                            'posts_per_page'    => 5,
                            'posts_type'        =>'post',
                            'meta_key'          => '_acj_view',
                            'orderby'           => 'meta_value_number',
                            'order'             => 'DESC'
                        );

                        $the_query = new WP_Query( $argc );
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                ?>
                                <li>
                                    <div class="pr-img">
                                        <?php if( has_post_thumbnail('acj_reads') ) {?>
                                            <a title="" data-original-title="" href="<?php the_permalink();?>"><?php echo get_the_post_thumbnail('acj_reads');?></a>
                                        <?php }else {?>
                                            <a title="" data-original-title="" href="<?php the_permalink();?>"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/no-image.jpg" alt=""/></a>
                                        <?php } ?>

                                    </div>
                                    <div class="pr-des">
                                        <p>
                                            <a title="" data-original-title="" href="<?php the_permalink();?>"><?php echo get_the_title();?></a>
                                        </p>
                                        <p>
                                            <?php echo wp_trim_words(get_the_excerpt(), 10); ?>
                                        </p>
                                        <a class="read-more" href="<?php the_permalink();?>"><?php _e('Đọc thêm',THEMEDOMAIN);?><span class="glyphicon glyphicon-menu-right"></span></a>
                                    </div>
                                </li>
                            <?php                                                                                                                                                                     }
                        } else {
                            // no posts found
                        }
                        /* Restore original Post Data */
                        wp_reset_postdata();?>
                    </ul>
                </div>

            </div>
            <div class="widget">
                <div class="heading-title">
                    <h3><span>Bán chạy nhất</span></h3>
                </div>
                <div class="widget-products">
                    <ul class="ul-widget-products">
                        <li>
                            <div class="pr-img">
                                <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                            </div>
                            <div class="pr-des">
                                <p>
                                    <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                                </p>
                                <p>
                                    <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="pr-img">
                                <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                            </div>
                            <div class="pr-des">
                                <p>
                                    <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                                </p>
                                <p>
                                    <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="pr-img">
                                <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                            </div>
                            <div class="pr-des">
                                <p>
                                    <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                                </p>
                                <p>
                                    <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="pr-img">
                                <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                            </div>
                            <div class="pr-des">
                                <p>
                                    <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                                </p>
                                <p>
                                    <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                                </p>
                            </div>
                        </li>
                        <li>
                            <div class="pr-img">
                                <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                            </div>
                            <div class="pr-des">
                                <p>
                                    <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                                </p>
                                <p>
                                    <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                                </p>
                            </div>
                        </li>

                    </ul>
                </div><!-- END .widget-products -->
            </div>
        </div><!-- END .sidebar -->
    </div><!-- END .container.tintuc -->
<?php get_footer(); ?>