<?php
/**
 * Template Name: Tin Tuc
 */
?>
<?php get_header(); ?>

    <div class="container-flue">
        <div class="top-banner">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-top-tintuc.png" alt=""/>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xm-6 column">
                        <?php
                        $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                        $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                        $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                        if(empty($page_title)){ $page_title = get_the_title(); }
                        if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                        ?>
                        <h1 class="title"><?php echo $page_title; ?></h1>
                        <p class="description"><?php echo $description; ?></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xm-6 column text-right">
                        <?php do_action( 'theme_breadcrumb' ); ?>
                    </div>
                </div>
            </div>
        </div><!--END breadcrumbs-->
    </div>
    <div class="container tintuc">
        <div class="news-recent top">
            <div class="heading-title">
                <h3><span><?php _e('Tin tức mới nhất', THEMEDOMAIN)?></span></h3>
            </div>
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-12 column">
                    <?php
                    $argc=array(
                        'posts_per_page'=>1,
                        'posts_type'   => 'posts',
                        'orderby'      => 'date'
                    );
                    $the_query = new WP_Query( $argc );
                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) {
                            $the_query->the_post();
                            ?>
                            <?php the_post_thumbnail('first_news_post');?>
                            <p class="out-title"><a  class="title" href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></p>
                            <p><?php echo get_the_excerpt();?></p>
                            <a class="read-more" href="<?php echo get_the_permalink();?>"> <?php _e('Đọc thêm',THEMEDOMAIN);?><span class="glyphicon glyphicon-menu-right"></span></a>
                        <?php                                                                                                                                                                     }
                    } else {
                        // no posts found
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();?>

                </div>
                <div class="col-lg-6 col-md-6 col-sm-12 column">
                    <div class="row">
                        <?php
                        $argc=array(
                            'posts_per_page'=>2,
                            'posts_type'   => 'posts',
                            'orderby'      => 'date',
                            'offset'       =>1
                        );
                        $the_query = new WP_Query( $argc );
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                                ?>
                                <div class="col-lg-6 col-md-6 col-sm-12 column">
                                    <!--                        <img class="img-responsive text-center" src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/tt-tinmoi-1.png" alt=""/>-->
                                    <?php echo get_the_post_thumbnail('normal_post');?>
                                    <p class="out-title"><a  class="title" href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></p>
                                    <p><?php echo get_the_excerpt();?></p>
                                    <a class="read-more" href="<?php echo get_the_permalink();?>"><?php _e('Đọc thêm',THEMEDOMAIN);?><span class="glyphicon glyphicon-menu-right"></span></a>

                                </div>
                            <?php                                                                                                                                                                     }
                        } else {
                            // no posts found
                        }
                        /* Restore original Post Data */
                        wp_reset_postdata();?>

                    </div>
                </div>
            </div><!--END row-->
        </div><!--END news-recent-->


        <div>
            <div class="heading-title tintuc-title">
                <ul class="nav nav-tabs ">
                    <?php
                    // load all 'category' terms for the post
                    $value = get_field('number', 'category');
                    //                echo "<pre>";
                    //                var_dump($value);
                    //                echo "</pre>";
                    $args = array(
                        'type'                     => 'post',
                        'child_of'                 => 0,
                        'parent'                   => '',
                        'orderby'                  => 'id',
                        'order'                    => 'DESC',
                        'hide_empty'               => false,
                        'hierarchical'             => 1,
                        'exclude'                  => '',
                        'include'                  => '',
                        'number'                   => '',
                        'taxonomy'                 => 'category',
                        'pad_counts'               => false

                    );
                    $catSlug = array();

                    $categories = get_categories( $args );
                    $active='';
                    $i=0;
                    foreach($categories as $cat){
                        $catSlug[] = $cat->slug;
                        ?>
                        <li <?php if ($i==0) echo "class='active'"?> class="" ><a href="<?php echo"#". $cat->slug;?>" data-toggle="tab"><?php echo $cat->cat_name;?></a></li>
                        <?php
                        $i++;
                    }
                    ?>
                </ul>
            </div>


            <div class="tab-content news-recent">
                <?php
                $i=0;
                foreach($catSlug as $slug){
                    ?>
                    <div class="tab-pane fade <?php if ($i==0) echo 'in active';?>"   id="<?php echo $slug;?>">
                        <div class="row">
                            <?php
                            $args = array(
                                'post_type'      => 'post',
                                'post_status'    => 'publish',
                                'posts_per_page'            => 4,
                                'paged'                     => get_query_var('paged'),
                                'tax_query' => array(
                                    array(
                                        'taxonomy' => 'category',
                                        'field'    => 'slug',
                                        'terms'    => $slug,
                                    ),
                                ),

                            );

                            $the_query = new WP_Query( $args );
                            if ( $the_query->have_posts() ) {
                                while ( $the_query->have_posts() ) {
                                    $the_query->the_post();
                                    ?>
                                    <div class="col-lg-3 col-md-3 col-sm-12">
                                        <!--                        <img class="img-responsive text-center" src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/tt-tinmoi-1.png" alt=""/>-->
                                        <p class="img"><?php the_post_thumbnail('normal_post');?></p>
                                        <p class="out-title"><a  class="title" href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></p>
                                        <p><?php echo wp_trim_words(get_the_excerpt(), 10); ?></p>
                                        <a class="read-more" href="<?php echo get_the_permalink();?>"><?php _e('Đọc thêm',THEMEDOMAIN);?><span class="glyphicon glyphicon-menu-right"></span></a>

                                    </div>
                                <?php                                                                                                                                                                     }
                            } else {
                                // no posts found
                            }
                            /* Restore original Post Data */
                            wp_reset_postdata();?>

                        </div>
                        <nav class="text-right">
                            <?php echo wp_pagenavi(array( 'query' => $the_query ));?>
                        </nav>
                    </div>
                    <?php
                    $i++;
                }
                ?>
            </div>


        </div><!--END -->
    </div><!--END container-->

<?php get_footer(); ?>