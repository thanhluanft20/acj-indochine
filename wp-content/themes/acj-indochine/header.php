<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <?php wp_head(); ?>

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
    <style type="text/css">
        .language-current {
            display: inline-block;
        }
        .current-flag {
            margin: 0 10px;
        }
    </style>
</head>
<body <?php body_class()?>>
<header>
    <div class="top-menu">
        <div class="container">
            <div class="col-md-6 col-sm-5">
                <ul class="header-socials">

                    <li><a target="_blank"  class="face-icon" href="<?php
                        if(function_exists('ot_get_option')){
                            echo ot_get_option('face');
                        }
                        ?>" data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a></li>

                    <li><a target="_blank"  class="twitter-icon" href="<?php
                        if(function_exists('ot_get_option')){
                            echo ot_get_option('twitter');
                        }

                        ?>" data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a></li>

                    <li><a target="_blank"  class="instagram-icon" href="<?php
                        if(function_exists('ot_get_option')){
                            echo ot_get_option('instagram');
                        }
                        ?>" data-toggle="tooltip" data-placement="bottom" title="Instagram"><i class="fa fa-instagram"></i></a></li>

                    <li><a target="_blank"  class="face-icon" href="<?php
                        if(function_exists('ot_get_option')){
                            echo ot_get_option('google');
                        }
                        ?>" data-toggle="tooltip" data-placement="bottom" title="Google Plus"><i class="fa fa-google-plus"></i></a></li>

                </ul>
            </div><!-- END .col-md-6 -->
            <div class="col-md-6 col-sm-7 text-right">
                <div class="widget_shopping_cart_content"></div>
                <ul class="top-right list-language">
                    <li>
                        <div class="language-title">
                            <?php _e( 'Ngôn ngữ', THEMEDOMAIN ); ?>
                        </div>
                        <div class="language-current">
                            <?php
                            $currentlang = pll_current_language();
                            $term = get_term_by( 'slug', $currentlang, 'language' );
                            $term_languages = get_terms('term_language', array('hide_empty' => false));
                            $term_languages = empty($term_languages) || is_wp_error($term_languages) ?
                                array() : array_combine(wp_list_pluck($term_languages, 'slug'), $term_languages);
                            $lang = new PLL_Language($term, $term_languages['pll_' . $term->slug]);
                            echo '<span class="current-flag">'.$lang->flag.'</span>';
                            echo '<span class="current-title">'.$lang->name.'</span>';
                            ?>
                        </div>
                        <ul>
                            <?php if( function_exists('language_selector_flags') ): language_selector_flags(); endif; ?>
                        </ul>
                    </li>
                </ul>
            </div><!-- END .col-md-6 -->
        </div><!-- END .container -->
    </div><!-- END .top-menu -->
    <div class="top-banner">
        <div class="container">
            <div class="col-md-2 logo">
                <a href="<?php echo home_url(); ?>" class="logo"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo.png" alt="<?php bloginfo( 'name' ); ?>" /></a>
            </div><!-- END .col-md-2 -->
            <div class="col-md-5">
                <?php
                    if ( is_front_page() && is_home() ) : ?>
                        <h1 class="site-title"><?php bloginfo( 'name' ); ?></h1>
                    <?php else : ?>
                        <p class="site-title"><?php bloginfo( 'name' ); ?></p>
                    <?php endif;
                    $description = get_bloginfo( 'description', 'display' );
                    if ( $description || is_customize_preview() ) : ?>
                        <p class="site-description"><?php echo $description; ?></p>
                    <?php endif;
                ?>
            </div><!-- END .col-md-4 -->
            <div class="col-md-3">
                <p class="top-info">
                    <i class="top-map-marker-icon"></i>
                    <?php echo ot_get_option('address');?>
                </p>
            </div><!-- END .col-md-4 -->
            <div class="col-md-2 phone">
                <p class="top-info">
                    <i class="top-phone-icon"></i>
                    <?php _e( 'Đường dây nóng', THEMEDOMAIN ); ?>:<br />
                    <strong class="top-hotline"> <?php echo ot_get_option('hotline');?></strong>
                </p>
            </div><!-- END .col-md-2 -->
        </div><!-- END .container -->
    </div><!-- END .top-banner -->
</header>
<nav class="navbar navbar-custom">
    <div class="container">
        <!-- Brand and toggle get grouped for better mobile display -->
        <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
                <span class="sr-only">Toggle navigation</span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>

        <!-- Collect the nav links, forms, and other content for toggling -->
        <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <?php
                $defaults = array(
                    'theme_location'  => 'primary',
                    'menu'            => '',
                    'container'       => 'ul',
                    'container_class' => '',
                    'container_id'    => '',
                    'menu_class'      => 'nav navbar-nav',
                    'menu_id'         => '',
                    'echo'            => true,
                    'fallback_cb'     => 'wp_page_menu',
                    'before'          => '',
                    'after'           => '',
                    'link_before'     => '',
                    'link_after'      => '',
                    'items_wrap'      => '<ul id="%1$s" class="%2$s">%3$s</ul>',
                    'depth'           => 0,
                    'walker'          => new H_Primary_Menu_Walker()
                );

                wp_nav_menu( $defaults );
            ?>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>