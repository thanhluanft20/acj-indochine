<?php
/**
 * Template name: Dich vu chi tiet
 */
?>
<?php get_header(); ?>
    <div class="container-flue">
        <div class="top-banner">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-dichvu.png" alt=""/>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column">
                        <?php
                        $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                        $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                        $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                        if(empty($page_title)){ $page_title = get_the_title(); }
                        if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                        ?>
                        <h1 class="title"><?php echo $page_title; ?></h1>
                        <p class="description"><?php echo $description; ?></p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column text-right">
                        <?php do_action( 'theme_breadcrumb' ); ?>
                    </div>
                </div>
            </div>
        </div><!--END breadcrumbs-->
    </div><!--END container-flue-->

    <div class="container dichvuchitiet">
        <div class="row">
            <div class="col-lg-3">
                <div class="all-service">
                    <h3 class="title"> <?php _e('Tất cả dịch vụ', THEMEDOMAIN)?></h3>
                    <ul class="nav nav-tabs nav-stacked">
                        <?php
                        $argc =array(
                            'post_type'         => 'tin-dich-vu',
                            'orderby'           => 'date',
                            'post_per_page'     => -1
                        );
                        $current_id= get_the_ID();
                        $the_query = new WP_Query( $argc );
                        if ( $the_query->have_posts() ) {
                            while ( $the_query->have_posts() ) {
                                $the_query->the_post();
                            ?>
                            <li <?php if($current_id == get_the_ID()) echo "class='active'"?>><a   href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>
                          <?php }
                        } else {
                            // no posts found
                        }
                        /* Restore original Post Data */
                        wp_reset_postdata();
                        ?>
                    </ul>
                </div>
                <div class="address">
                    <?php if ( is_active_sidebar( 'service-sidebar' ) ) : ?>
                            <?php dynamic_sidebar( 'service-sidebar' ); ?>
                    <?php endif; ?>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="tab-content">
                    <?php
                    if( have_posts() ):
                        while(have_posts()){
                            the_post();
                            the_content();
                        }
                    endif;
                    ?>

                </div><!-- END tab-content-->
            </div><!--- END col-lg-9-->
        </div><!-- END row-->
    </div><!--END container-->
<?php get_footer(); ?>