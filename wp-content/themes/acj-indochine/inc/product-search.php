<?php
add_filter( 'woocommerce_page_title', 'acj_change_product_search_title' );
function acj_change_product_search_title( $page_title )
{
    if ( is_search() ) {
        $page_title = __( 'Tìm kiếm lốp xe', THEMEDOMAIN );
    }
    if( $page_title == 'Sản Phẩm'){
        $page_title = __( 'Sản Phẩm', THEMEDOMAIN );
    }
   return $page_title;
}
add_action( 'template_include', 'acj_include_template', 99 );
function acj_include_template( $template ){
    if ( is_search( ) && isset( $_REQUEST['search_type'] ) && $_REQUEST['search_type'] == 'product' ) {
        $new_template = locate_template( array( 'content-search-product.php' ) );
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }
    if( isset( $_REQUEST['search_type'] ) && $_REQUEST['search_type'] == 'product' ){
        $new_template = locate_template( array( 'content-search-product.php' ) );
        if ( '' != $new_template ) {
            return $new_template ;
        }
    }

    return $template;
}