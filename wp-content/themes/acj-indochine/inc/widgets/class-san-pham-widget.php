<?php
add_action('widgets_init', function () {
    register_widget('San_Pham_Widget');
});

/**
 * Adds My_Widget widget.
 */
class San_Pham_Widget extends WP_Widget
{
    /**
     * Register widget with WordPress.
     */
    function __construct()
    {
        parent::__construct(
            'san_pham', // Base ID
            __('Sản Phẩm', THEMEDOMAIN), // Name
            array('description' => __('wedget này hiển thị các sản phẩm...', THEMEDOMAIN),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        /**my code*/


        $taxonomy = 'loai_xe';
        $orderby = 'name';
        $show_count = 0;      // 1 for yes, 0 for no
        $pad_counts = 0;      // 1 for yes, 0 for no
        $hierarchical = 1;      // 1 for yes, 0 for no
        $title = '';
        $empty = 0;

        $args_cat = array(
            'hide_empty' => $empty
        );
        $all_categories = get_terms( $taxonomy );?>
        <ul class="ul-widget-products">
            <?php foreach ($all_categories as $cat) {
                if ($cat->parent == 0) { ?>
                    <li><a href="<?php echo get_term_link($cat->slug, $taxonomy); ?>"> <?php echo $cat->name; ?></a></li>
                <?php
                }
            }
            ?>
            <div class="clear"></div>
        </ul>
        <?php
        /**my code*/
        ?>
        <div class="icon-footer">
            <ul class="footer-socials">
                <li>
                    <a target="_blank" class="face-icon" href="<?php echo ot_get_option('face'); ?>"
                       data-toggle="tooltip" data-placement="bottom" title="Facebook"><i class="fa fa-facebook"></i></a>
                </li>
                <li>
                    <a target="_blank" class="twitter-icon" href="<?php echo ot_get_option('twitter'); ?>"
                       data-toggle="tooltip" data-placement="bottom" title="Twitter"><i class="fa fa-twitter"></i></a>
                </li>
                <li>
                    <a target="_blank" class="instagram-icon" href="<?php echo ot_get_option('instagram'); ?>"
                       data-toggle="tooltip" data-placement="bottom" title="Instagram"><i
                            class="fa fa-instagram"></i></a>
                </li>
                <li>
                    <a target="_blank" class="face-icon" href="<?php echo ot_get_option('google'); ?>"
                       data-toggle="tooltip" data-placement="bottom" title="Google Plus"><i
                            class="fa fa-google-plus"></i></a></li>
                <div class="clear"></div>
            </ul>
        </div>
        <?php
        /** End my code */
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public
    function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', THEMEDOMAIN);
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>

    <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public
    function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
} // class My_Widget

