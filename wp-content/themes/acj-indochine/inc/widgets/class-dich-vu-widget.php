<?php
add_action('widgets_init', function () {
    register_widget('Dich_Vu_Widget');
});

class Dich_Vu_Widget extends WP_Widget
{
    /**
     * Register widget with WordPress.
     */
    function __construct()
    {
        parent::__construct(
            'dich_vu', // Base ID
            __('Dịch Vụ', THEMEDOMAIN), // Name
            array('description' => __('Dịch Vụ', THEMEDOMAIN),) // Args
        );
    }

    /**
     * Front-end display of widget.
     *
     * @see WP_Widget::widget()
     *
     * @param array $args Widget arguments.
     * @param array $instance Saved values from database.
     */
    public function widget($args, $instance)
    {

        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        /**my code*/
        $dich_vu = array(
            'post_type' => 'tin-dich-vu',
            'posts_per_page' => 5,
            'orderby' => 'date'
        );
        $loop = new WP_Query($dich_vu);

        if ($loop->have_posts()) {
            ?>
            <ul class="ul-widget-products">
                <?php
                while ($loop->have_posts()) : $loop->the_post();
                    ?>
                    <li><a href="<?php the_permalink(); ?>"> <?php the_title(); ?></a></li>
                <?php endwhile; ?>
                <div class="clear"></div>
            </ul>
        <?php
        }
        wp_reset_postdata();
        ?>
        <?php
        /** End my code */
        echo $args['after_widget'];
    }

    /**
     * Back-end widget form.
     *
     * @see WP_Widget::form()
     *
     * @param array $instance Previously saved values from database.
     */
    public function form($instance)
    {
        if (isset($instance['title'])) {
            $title = $instance['title'];
        } else {
            $title = __('New title', THEMEDOMAIN);
        }
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>"
                   name="<?php echo $this->get_field_name('title'); ?>" type="text"
                   value="<?php echo esc_attr($title); ?>">
        </p>

    <?php
    }

    /**
     * Sanitize widget form values as they are saved.
     *
     * @see WP_Widget::update()
     *
     * @param array $new_instance Values just sent to be saved.
     * @param array $old_instance Previously saved values from database.
     *
     * @return array Updated safe values to be saved.
     */
    public function update($new_instance, $old_instance)
    {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title'])) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
} // class My_Widget