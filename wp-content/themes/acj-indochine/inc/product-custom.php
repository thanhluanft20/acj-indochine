<?php
add_filter( 'woocommerce_product_data_tabs', 'acj_hieu_suat_product_tab' );
function acj_hieu_suat_product_tab( $tabs = array() ){
    $tabs['hieu_suat'] = array(
        'label'  => __( 'Hiệu suất', THEMEDOMAIN ),
        'target' => 'acj_hieu_suat_product_tab_panel',
        'class'  => array(  ),
    );
    $tabs['thong_tin_them'] = array(
        'label'  => __( 'Thông tin thêm', THEMEDOMAIN ),
        'target' => 'acj_more_info_product_tab_panel',
        'class'  => array(  ),
    );
    return $tabs;
}
function acj_hieu_suat_product_tab_panel(){
    global $post;
    wp_enqueue_script('jquery');
    wp_enqueue_media();
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    $acj_hieu_suat = get_post_meta( $post->ID, 'acj_hieu_suat', true );
    $image = get_template_directory_uri().'/assets/images/placeholder.png';
    ?>
    <div id="acj_hieu_suat_product_tab_panel" class="panel woocommerce_options_panel">
        <?php if( empty( $acj_hieu_suat ) ): ?>
            <?php  for( $i=0; $i <= 5; $i++ ): ?>
                <h3 id="tab-title-<?php echo $i; ?>" class="tab-title">Dry</h3>
                <div id="tab-content-<?php echo $i; ?>" class="tab-content options_group">
                    <div class="acj_icon form-field">
                        <label><?php _e( 'Icon', THEMEDOMAIN ); ?></label>
                        <div class="acj_hieu_suat_thumbnail" style="float: left; margin-right: 10px;margin-left: 10px;">
                            <img src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" />
                        </div>
                        <div style="line-height: 60px;">
                            <input type="hidden" class="acj_hieu_suat_thumbnail_id" name="acj_hieu_suat[<?php echo $i; ?>][thumbnail_id]" />
                            <button type="button" class="upload_image_button button" data-id="<?php echo $i; ?>"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
                            <button type="button" class="remove_image_button button" data-id="<?php echo $i; ?>"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
                        </div>
                    </div><!-- END .acj_icon -->
                    <div class="acj_info">
                        <p class="form-field">
                            <label for="ten-hieu-suat-<?php echo $i; ?>"><?php _e( "Tên hiệu suất:", THEMEDOMAIN ) ?></label>
                            <input name="acj_hieu_suat[<?php echo $i; ?>][name]" id="ten-hieu-suat-<?php echo $i; ?>" type="text" value="Dry" />
                        </p>
                        <p class="form-field">
                            <label for="binh-chon-hieu-suat-<?php echo $i; ?>"><?php _e( "Bình chọn:", THEMEDOMAIN ) ?></label>
                            <select name="acj_hieu_suat[<?php echo $i; ?>][rate]" id="binh-chon-hieu-suat-<?php echo $i; ?>">
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                                <option value="4">4</option>
                                <option value="5">5</option>
                                <option value="6">6</option>
                                <option value="7">7</option>
                                <option value="8">8</option>
                                <option value="9">9</option>
                                <option value="10">10</option>
                            </select>
                        </p>
                    </div><!-- END .acj_info -->
                </div><!-- END .tab-content -->
            <?php endfor; ?>
        <?php else: ?>
            <?php foreach( $acj_hieu_suat as $i=>$hs ): ?>
                <?php
                    $thumbnail_id = $hs['thumbnail_id'];
                    if ( $thumbnail_id ) {
                        $image = wp_get_attachment_thumb_url( $thumbnail_id );
                    } else {
                        $image = get_template_directory_uri().'/assets/images/placeholder.png';
                    }
                ?>
                <h3 id="tab-title-<?php echo $i; ?>" class="tab-title"><?php echo $hs['name']; ?></h3>
                <div id="tab-content-<?php echo $i; ?>" class="tab-content options_group">
                    <div class="acj_icon form-field">
                        <label><?php _e( 'Icon', THEMEDOMAIN ); ?></label>
                        <div class="acj_hieu_suat_thumbnail" style="float: left; margin-right: 10px;margin-left: 10px;">
                            <img src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" />
                        </div>
                        <div style="line-height: 60px;">
                            <input type="hidden" class="acj_hieu_suat_thumbnail_id" name="acj_hieu_suat[<?php echo $i; ?>][thumbnail_id]" value="<?php echo $hs['thumbnail_id']; ?>" />
                            <button type="button" class="upload_image_button button" data-id="<?php echo $i; ?>"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
                            <button type="button" class="remove_image_button button" data-id="<?php echo $i; ?>"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
                        </div>
                    </div><!-- END .acj_icon -->
                    <div class="acj_info">
                        <p class="form-field">
                            <label for="ten-hieu-suat-<?php echo $i; ?>"><?php _e( "Tên hiệu suất:", THEMEDOMAIN ) ?></label>
                            <input name="acj_hieu_suat[<?php echo $i; ?>][name]" id="ten-hieu-suat-<?php echo $i; ?>" type="text" value="<?php echo $hs['name']; ?>" />
                        </p>
                        <p class="form-field">
                            <label for="binh-chon-hieu-suat-<?php echo $i; ?>"><?php _e( "Bình chọn:", THEMEDOMAIN ) ?></label>
                            <select name="acj_hieu_suat[<?php echo $i; ?>][rate]" id="binh-chon-hieu-suat-<?php echo $i; ?>">
                                <option <?php selected( $hs['rate'], 1 ); ?> value="1">1</option>
                                <option <?php selected( $hs['rate'], 2 ); ?> value="2">2</option>
                                <option <?php selected( $hs['rate'], 3 ); ?> value="3">3</option>
                                <option <?php selected( $hs['rate'], 4 ); ?> value="4">4</option>
                                <option <?php selected( $hs['rate'], 5 ); ?> value="5">5</option>
                                <option <?php selected( $hs['rate'], 6 ); ?> value="6">6</option>
                                <option <?php selected( $hs['rate'], 7 ); ?> value="7">7</option>
                                <option <?php selected( $hs['rate'], 8 ); ?> value="8">8</option>
                                <option <?php selected( $hs['rate'], 9 ); ?> value="9">9</option>
                                <option <?php selected( $hs['rate'], 10 ); ?> value="10">10</option>
                            </select>
                        </p>
                    </div><!-- END .acj_info -->
                </div><!-- END .tab-content -->
            <?php endforeach; ?>
        <?php endif; ?>
    </div>
    <script type="text/javascript">

        // Only show the "remove image" button when needed
        /*if ( ! jQuery( '#acj_hieu_suat_thumbnail_id' ).val() || jQuery( '#acj_hieu_suat_thumbnail_id' ).val() == '0' ) {
            jQuery( '.remove_image_button' ).hide();
        }*/

        // Uploading files
        var file_frame;
        var ID = null;

        jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

            event.preventDefault();
            ID = jQuery(this).data('id');

            // If the media frame already exists, reopen it.
            if ( file_frame ) {
                file_frame.open();
                return;
            }

            // Create the media frame.
            file_frame = wp.media.frames.downloadable_file = wp.media({
                title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                button: {
                    text: '<?php _e( "Use image", "woocommerce" ); ?>'
                },
                multiple: false
            });

            // When an image is selected, run a callback.
            file_frame.on( 'select', function() {
                var attachment = file_frame.state().get( 'selection' ).first().toJSON();
                var dis = jQuery('#tab-content-'+ID);
                dis.find('.acj_hieu_suat_thumbnail_id').val( attachment.id );
                dis.find('.acj_hieu_suat_thumbnail').find( 'img' ).attr( 'src', attachment.sizes.full.url );
                dis.find('.remove_image_button').show();
            });

            // Finally, open the modal.
            file_frame.open();
        });

        jQuery( document ).on( 'click', '.remove_image_button', function() {
            var dis = jQuery(this).parent();
            dis.find('.acj_hieu_suat_thumbnail_id').val( '' );
            dis.prev('.acj_hieu_suat_thumbnail').find( 'img' ).attr( 'src', '<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>' );
            jQuery( this ).hide();
            return false;
        });
        jQuery( document).on( 'click', '.tab-title', function(){
            if( jQuery( this).hasClass('active') ) {
                jQuery(this).removeClass('active');
                jQuery(this).next('.tab-content').hide();
                return false;
            }else{
                jQuery('.tab-title').removeClass('active');
                jQuery('.tab-content').removeClass('active').hide();
                jQuery(this).addClass('active');
                jQuery(this).next('.tab-content').show();
                return false;
            }
        });

    </script>
    <style type="text/css">
        .panel .tab-content {
            display: none;
        }
        h3.tab-title {
            padding: 10px;
            background: #f1f1f1;
            margin-bottom: 3px;
            margin-top: 0;
            cursor: pointer;
        }
    </style>
    <?php

}
add_action( 'woocommerce_product_data_panels', 'acj_hieu_suat_product_tab_panel');

function acj_more_info_product_tab_panel(){
    global $post;
    $_loai_lop = get_post_meta( $post->ID, '_loai_lop', true );
    $_extreme = get_post_meta( $post->ID, '_extreme', true );
    $_nam_san_xuat = get_post_meta( $post->ID, '_nam_san_xuat', true );
    ?>
    <div id="acj_more_info_product_tab_panel" class="panel woocommerce_options_panel">
        <div class="options_group">
            <p class="form-field _loai_lop">
                <label for="_loai_lop"><?php _e( "Loại lốp", THEMEDOMAIN ); ?></label>
                <input type="text" class="short" style="" name="_loai_lop" id="_loai_lop" value="<?=$_loai_lop?>" />
            </p>
            <p class="form-field _extreme">
                <label for="_extreme"><?php _e( "Extreme Conditions", THEMEDOMAIN ); ?></label>
                <input type="text" class="short" style="" name="_extreme" id="_extreme" value="<?=$_extreme?>" />
            </p>
            <p class="form-field _nam_san_xuat">
                <label for="_nam_san_xuat"><?php _e( "Extreme Conditions", THEMEDOMAIN ); ?></label>
                <select name="_nam_san_xuat" id="_nam_san_xuat">
                    <option value=""><?php _e( "Năm sản xuất", THEMEDOMAIN ); ?></option>
                    <?php for( $i = 1970; $i <= date('Y'); $i++ ): ?>
                        <option <?php selected( $i, $_nam_san_xuat ); ?> value="<?php echo $i; ?>"><?php echo $i; ?></option>
                    <?php endfor; ?>
                </select>
            </p>
        </div>
    </div>
    <?php
}
add_action( 'woocommerce_product_data_panels', 'acj_more_info_product_tab_panel');

add_action( 'save_post', 'acj_save_product_data');
function acj_save_product_data( $post_id ){
    if( isset( $_POST['acj_hieu_suat'] ) ){
        update_post_meta( $post_id, 'acj_hieu_suat', $_POST['acj_hieu_suat'] );
    }
    if( isset( $_POST['_loai_lop'] ) ){
        update_post_meta( $post_id, '_loai_lop', $_POST['_loai_lop'] );
    }
    if( isset( $_POST['_extreme'] ) ){
        update_post_meta( $post_id, '_extreme', $_POST['_extreme'] );
    }
    if( isset( $_POST['_nam_san_xuat'] ) ){
        update_post_meta( $post_id, '_nam_san_xuat', $_POST['_nam_san_xuat'] );
    }
}