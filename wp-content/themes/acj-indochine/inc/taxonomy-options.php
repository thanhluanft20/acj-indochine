<?php

function acj_init(){
    $taxs = apply_filters('add_image_to_taxonomies', array('loai_xe', 'hang_xe', 'mau_xe'));
    foreach ($taxs as $tax) {
        add_action($tax . '_add_form_fields', 'taxonomy_image_meta_box_new' );
        add_action($tax . '_edit_form_fields', 'taxonomy_image_meta_box_edit' );

        add_action('create_' . $tax, 'taxonomy_image_meta_box_save' );
        add_action('edited_' . $tax, 'taxonomy_image_meta_box_save' );
    }
}
add_action( 'init', 'acj_init', 1 );

function create_product_taxonomies(){
    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Loại xe', 'taxonomy general name', THEMEDOMAIN ),
        'singular_name'     => _x( 'Loại xe', 'taxonomy singular name', THEMEDOMAIN ),
        'search_items'      => __( 'Tìm kiếm loại xe', THEMEDOMAIN ),
        'all_items'         => __( 'Tất cả loại xe', THEMEDOMAIN ),
        'parent_item'       => __( 'Loại xe cha', THEMEDOMAIN ),
        'parent_item_colon' => __( 'Loại xe cha:', THEMEDOMAIN ),
        'edit_item'         => __( 'Sửa loại xe', THEMEDOMAIN ),
        'update_item'       => __( 'Cập nhật loại xe', THEMEDOMAIN ),
        'add_new_item'      => __( 'Thêm mới loại xe', THEMEDOMAIN ),
        'new_item_name'     => __( 'Thêm mới loại xe', THEMEDOMAIN ),
        'menu_name'         => __( 'Loại xe', THEMEDOMAIN ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'loai-xe' ),
    );

    register_taxonomy( 'loai_xe', array( 'product' ), $args );

    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Hãng xe', 'taxonomy general name', THEMEDOMAIN ),
        'singular_name'     => _x( 'Hãng xe', 'taxonomy singular name', THEMEDOMAIN ),
        'search_items'      => __( 'Tìm kiếm hãng xe', THEMEDOMAIN ),
        'all_items'         => __( 'Tất cả hãng xe', THEMEDOMAIN ),
        'parent_item'       => __( 'Hãng xe cha', THEMEDOMAIN ),
        'parent_item_colon' => __( 'Hãng xe cha:', THEMEDOMAIN ),
        'edit_item'         => __( 'Sửa hãng xe', THEMEDOMAIN ),
        'update_item'       => __( 'Cập nhật hãng xe', THEMEDOMAIN ),
        'add_new_item'      => __( 'Thêm mới hãng xe', THEMEDOMAIN ),
        'new_item_name'     => __( 'Thêm mới hãng xe', THEMEDOMAIN ),
        'menu_name'         => __( 'Hãng xe', THEMEDOMAIN ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'hang-xe' ),
    );

    register_taxonomy( 'hang_xe', array( 'product' ), $args );

    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Mẫu xe', 'taxonomy general name', THEMEDOMAIN ),
        'singular_name'     => _x( 'Mẫu xe', 'taxonomy singular name', THEMEDOMAIN ),
        'search_items'      => __( 'Tìm kiếm mẫu xe', THEMEDOMAIN ),
        'all_items'         => __( 'Tất cả mẫu xe', THEMEDOMAIN ),
        'parent_item'       => __( 'Mẫu xe cha', THEMEDOMAIN ),
        'parent_item_colon' => __( 'Mẫu xe cha:', THEMEDOMAIN ),
        'edit_item'         => __( 'Sửa mẫu xe', THEMEDOMAIN ),
        'update_item'       => __( 'Cập nhật mẫu xe', THEMEDOMAIN ),
        'add_new_item'      => __( 'Thêm mới mẫu xe', THEMEDOMAIN ),
        'new_item_name'     => __( 'Thêm mới mẫu xe', THEMEDOMAIN ),
        'menu_name'         => __( 'Mẫu xe', THEMEDOMAIN ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'mau-xe' ),
    );

    register_taxonomy( 'mau_xe', array( 'product' ), $args );

    // Add new taxonomy, make it hierarchical (like categories)
    $labels = array(
        'name'              => _x( 'Kích thước', 'taxonomy general name', THEMEDOMAIN ),
        'singular_name'     => _x( 'Kích thước', 'taxonomy singular name', THEMEDOMAIN ),
        'search_items'      => __( 'Tìm kiếm kích thước', THEMEDOMAIN ),
        'all_items'         => __( 'Tất cả kích thước', THEMEDOMAIN ),
        'parent_item'       => __( 'Kích thước cha', THEMEDOMAIN ),
        'parent_item_colon' => __( 'Kích thước cha:', THEMEDOMAIN ),
        'edit_item'         => __( 'Sửa kích thước', THEMEDOMAIN ),
        'update_item'       => __( 'Cập nhật kích thước', THEMEDOMAIN ),
        'add_new_item'      => __( 'Thêm mới kích thước', THEMEDOMAIN ),
        'new_item_name'     => __( 'Thêm mới kích thước', THEMEDOMAIN ),
        'menu_name'         => __( 'Kích thước', THEMEDOMAIN ),
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'kich-thuoc' ),
    );

    register_taxonomy( 'kich_thuoc', array( 'product' ), $args );
}
add_action( 'init', 'create_product_taxonomies', 0 );

function taxonomy_image_meta_box_new( $term ){
    wp_enqueue_script('jquery');
    wp_enqueue_media();
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    ?>
    <div class="form-field">
        <label><?php _e( 'Thumbnail', 'woocommerce' ); ?></label>
        <div id="acj_tax_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>" width="60px" height="60px" /></div>
        <div style="line-height: 60px;">
            <input type="hidden" id="acj_tax_thumbnail_id" name="acj_tax_thumbnail_id" value="<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>" />
            <button type="button" class="upload_image_button button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
            <button type="button" class="remove_image_button button"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
        </div>
        <label><?php _e( 'Thumbnail Gray', THEMEDOMAIN ); ?></label>
        <div id="acj_tax_thumbnail_gray" style="float: left; margin-right: 10px;"><img src="<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>" width="60px" height="60px" /></div>
        <div style="line-height: 60px;">
            <input type="hidden" id="acj_tax_thumbnail_gray_id" name="acj_tax_thumbnail_gray_id" value="0" />
            <button type="button" class="upload_image_button_gray button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
            <button type="button" class="remove_image_button_gray button"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
        </div>
        <script type="text/javascript">

            // Only show the "remove image" button when needed
            if ( ! jQuery( '#acj_tax_thumbnail_id' ).val() || jQuery( '#acj_tax_thumbnail_id' ).val() == '0' ) {
                jQuery( '.remove_image_button' ).hide();
            }
            // Only show the "remove image" button when needed
            if ( ! jQuery( '#acj_tax_thumbnail_gray_id' ).val() || jQuery( '#acj_tax_thumbnail_gray_id' ).val() == '0' ) {
                jQuery( '.remove_image_button_gray' ).hide();
            }

            // Uploading files
            var file_frame;

            jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    file_frame.open();
                    return;
                }

                // Create the media frame.
                file_frame = wp.media.frames.downloadable_file = wp.media({
                    title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                    button: {
                        text: '<?php _e( "Use image", "woocommerce" ); ?>'
                    },
                    multiple: false
                });

                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    var attachment = file_frame.state().get( 'selection' ).first().toJSON();

                    jQuery( '#acj_tax_thumbnail_id' ).val( attachment.id );
                    jQuery( '#acj_tax_thumbnail' ).find( 'img' ).attr( 'src', attachment.sizes.full.url );
                    jQuery( '.remove_image_button' ).show();
                });

                // Finally, open the modal.
                file_frame.open();
            });

            jQuery( document ).on( 'click', '.remove_image_button', function() {
                jQuery( '#acj_tax_thumbnail' ).find( 'img' ).attr( 'src', '<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>' );
                jQuery( '#acj_tax_thumbnail_id' ).val( '' );
                jQuery( '.remove_image_button' ).hide();
                return false;
            });

            var file_frame2;

            jQuery( document ).on( 'click', '.upload_image_button_gray', function( event ) {

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame2 ) {
                    file_frame2.open();
                    return;
                }

                // Create the media frame.
                file_frame2 = wp.media.frames.downloadable_file = wp.media({
                    title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                    button: {
                        text: '<?php _e( "Use image", "woocommerce" ); ?>'
                    },
                    multiple: false
                });

                // When an image is selected, run a callback.
                file_frame2.on( 'select', function() {
                    var attachment = file_frame2.state().get( 'selection' ).first().toJSON();

                    jQuery( '#acj_tax_thumbnail_gray_id' ).val( attachment.id );
                    jQuery( '#acj_tax_thumbnail_gray' ).find( 'img' ).attr( 'src', attachment.sizes.full.url );
                    jQuery( '.remove_image_button' ).show();
                });

                // Finally, open the modal.
                file_frame2.open();
            });

            jQuery( document ).on( 'click', '.remove_image_button', function() {
                jQuery( '#acj_tax_thumbnail_gray' ).find( 'img' ).attr( 'src', '<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>' );
                jQuery( '#acj_tax_thumbnail_gray_id' ).val( '' );
                jQuery( '.remove_image_button_gray' ).hide();
                return false;
            });

        </script>
        <div class="clear"></div>
    </div>
    <?php
}
function taxonomy_image_meta_box_edit( $term ){
    wp_enqueue_script('jquery');
    wp_enqueue_media();
    wp_enqueue_script('media-upload');
    wp_enqueue_script('thickbox');
    $thumbnail_id = absint( get_term_meta( $term->term_id, 'acj_tax_thumbnail_id', true ) );
    if ( $thumbnail_id ) {
        $image = wp_get_attachment_thumb_url( $thumbnail_id );
    } else {
        $image = get_template_directory_uri().'/assets/images/placeholder.png';
    }

    $thumbnail_gray_id = absint( get_term_meta( $term->term_id, 'acj_tax_thumbnail_gray_id', true ) );
    if ( $thumbnail_gray_id ) {
        $image_gray = wp_get_attachment_thumb_url( $thumbnail_gray_id );
    } else {
        $image_gray = get_template_directory_uri().'/assets/images/placeholder.png';
    }
    ?>
    <div class="form-field">
        <label><?php _e( 'Thumbnail', 'woocommerce' ); ?></label>
        <div id="acj_tax_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( $image ); ?>" width="60px" height="60px" /></div>
        <div style="line-height: 60px;">
            <input type="hidden" id="acj_tax_thumbnail_id" name="acj_tax_thumbnail_id" value="<?php echo $thumbnail_id; ?>" />
            <button type="button" class="upload_image_button button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
            <button type="button" class="remove_image_button button"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
        </div>
        <label><?php _e( 'Thumbnail Gray', THEMEDOMAIN ); ?></label>
        <div id="acj_tax_thumbnail_gray" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( $image_gray ); ?>" width="60px" height="60px" /></div>
        <div style="line-height: 60px;">
            <input type="hidden" id="acj_tax_thumbnail_gray_id" name="acj_tax_thumbnail_gray_id" value="<?php echo $thumbnail_gray_id; ?>" />
            <button type="button" class="upload_image_button_gray button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
            <button type="button" class="remove_image_button_gray button"><?php _e( 'Remove image', 'woocommerce' ); ?></button>
        </div>
        <script type="text/javascript">

            // Only show the "remove image" button when needed
            if ( ! jQuery( '#acj_tax_thumbnail_id' ).val() || jQuery( '#acj_tax_thumbnail_id' ).val() == '0' ) {
                jQuery( '.remove_image_button' ).hide();
            }
            // Only show the "remove image" button when needed
            if ( ! jQuery( '#acj_tax_thumbnail_gray_id' ).val() || jQuery( '#acj_tax_thumbnail_gray_id' ).val() == '0' ) {
                jQuery( '.remove_image_button_gray' ).hide();
            }

            // Uploading files
            var file_frame;

            jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    file_frame.open();
                    return;
                }

                // Create the media frame.
                file_frame = wp.media.frames.downloadable_file = wp.media({
                    title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                    button: {
                        text: '<?php _e( "Use image", "woocommerce" ); ?>'
                    },
                    multiple: false
                });

                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    var attachment = file_frame.state().get( 'selection' ).first().toJSON();

                    jQuery( '#acj_tax_thumbnail_id' ).val( attachment.id );
                    jQuery( '#acj_tax_thumbnail' ).find( 'img' ).attr( 'src', attachment.sizes.full.url );
                    jQuery( '.remove_image_button' ).show();
                });

                // Finally, open the modal.
                file_frame.open();
            });

            jQuery( document ).on( 'click', '.remove_image_button', function() {
                jQuery( '#acj_tax_thumbnail' ).find( 'img' ).attr( 'src', '<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>' );
                jQuery( '#acj_tax_thumbnail_id' ).val( '' );
                jQuery( '.remove_image_button' ).hide();
                return false;
            });

            var file_frame2;

            jQuery( document ).on( 'click', '.upload_image_button_gray', function( event ) {

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame2 ) {
                    file_frame2.open();
                    return;
                }

                // Create the media frame.
                file_frame2 = wp.media.frames.downloadable_file = wp.media({
                    title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                    button: {
                        text: '<?php _e( "Use image", "woocommerce" ); ?>'
                    },
                    multiple: false
                });

                // When an image is selected, run a callback.
                file_frame2.on( 'select', function() {
                    var attachment = file_frame2.state().get( 'selection' ).first().toJSON();

                    jQuery( '#acj_tax_thumbnail_gray_id' ).val( attachment.id );
                    jQuery( '#acj_tax_thumbnail_gray' ).find( 'img' ).attr( 'src', attachment.sizes.full.url );
                    jQuery( '.remove_image_button' ).show();
                });

                // Finally, open the modal.
                file_frame2.open();
            });

            jQuery( document ).on( 'click', '.remove_image_button', function() {
                jQuery( '#acj_tax_thumbnail_gray' ).find( 'img' ).attr( 'src', '<?php echo get_template_directory_uri().'/assets/images/placeholder.png'; ?>' );
                jQuery( '#acj_tax_thumbnail_gray_id' ).val( '' );
                jQuery( '.remove_image_button_gray' ).hide();
                return false;
            });

        </script>
        <div class="clear"></div>
    </div>
    <?php
}
function taxonomy_image_meta_box_save( $termID){
    if ( isset( $_POST['acj_tax_thumbnail_id'] ) ) {
        update_term_meta( $termID, 'acj_tax_thumbnail_id', $_POST['acj_tax_thumbnail_id'] );
        update_term_meta( $termID, 'acj_tax_thumbnail_gray_id', $_POST['acj_tax_thumbnail_gray_id'] );
    }
}