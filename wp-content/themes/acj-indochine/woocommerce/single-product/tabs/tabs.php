<?php
/**
 * Single Product tabs
 *
 * @author  WooThemes
 * @package WooCommerce/Templates
 * @version 2.4.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Filter tabs and allow third parties to add their own
 *
 * Each tab is an array containing title, callback and priority.
 * @see woocommerce_default_product_tabs()
 */
$tabs = apply_filters( 'woocommerce_product_tabs', array() );

if ( ! empty( $tabs ) ) : ?>

	<div class="heading-title tintuc-title">
		<ul class="nav nav-tabs">
			<?php $i=1; foreach ( $tabs as $key => $tab ) : if( $i == 1 ) $class= ' active'; else $class = ''; ?>
				<li class="<?php echo esc_attr( $key ); ?>_tab<?php echo $class; ?>" >
					<a href="#tab-<?php echo esc_attr( $key ); ?>" data-toggle="tab" data-original-title="<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?>" title="<?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?>"><?php echo apply_filters( 'woocommerce_product_' . $key . '_tab_title', esc_html( $tab['title'] ), $key ); ?></a>
				</li>
			<?php $i++; endforeach; ?>
		</ul>
	</div>
	<div class="tab-content">
		<?php $j=1; foreach ( $tabs as $key => $tab ) : if( $j == 1 ) $class= ' in active'; else $class = ''; ?>
			<div class="tab-pane fade<?php echo $class; ?>" id="tab-<?php echo esc_attr( $key ); ?>">
				<?php call_user_func( $tab['callback'], $key, $tab ); ?>
			</div>
		<?php $j++; endforeach; ?>
	</div>

<?php endif; ?>
