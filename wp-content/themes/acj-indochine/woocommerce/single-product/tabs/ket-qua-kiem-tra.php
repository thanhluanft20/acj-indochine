<?php
    global $post;
    $args = array(
        'post_type' => 'tin-ket-qua-kiem-tra',
        'posts_per_page' => -1,
        'meta_query' => array(
            array(
                'key'     => '_acj_product',
                'value'   => $post->ID,
                'compare' => '=',
            ),
        ),
    );
    $query = new WP_Query( $args );
    if( $query->have_posts() ):
        ?>
        <div class="list-test-result">
        <?php
        while( $query->have_posts() ): $query->the_post();
            $_acj_product = get_post_meta( get_the_ID(), '_acj_product', true );
            $_acj_date = get_post_meta( get_the_ID(), '_acj_date', true );
            $_acj_rate = get_post_meta( get_the_ID(), '_acj_rate', true );
            $_acj_company = get_post_meta( get_the_ID(), '_acj_company', true );
            $_acj_logo = get_post_meta( get_the_ID(), '_acj_logo', true );
            $_acj_name_car = get_post_meta( get_the_ID(), '_acj_name_car', true );
            $_acj_car_logo = get_post_meta( get_the_ID(), '_acj_car_logo', true );
            $_acj_logo_thumb_id = absint( $_acj_logo );
            if ( $_acj_logo_thumb_id ) {
                $image_logo = wp_get_attachment_thumb_url( $_acj_logo_thumb_id );
            } else {
                $image_logo = get_template_directory_uri().'/assets/images/placeholder.png';
            }

            $_acj_car_logo_thumb_id = absint( $_acj_car_logo );
            if ( $_acj_logo_thumb_id ) {
                $image_car_logo = wp_get_attachment_thumb_url( $_acj_car_logo_thumb_id );
            } else {
                $image_car_logo = get_template_directory_uri().'/assets/images/placeholder.png';
            }
            ?>
            <div class="test-result">
                <div class="col-lg-3">
                    <p class="test-result-company-image">
                        <img src="<?=$image_logo;?>" alt="" />
                    </p>
                </div>
                <div class="col-lg-6">
                    <p class="test-result-date"><?=$_acj_date;?></p>
                    <p class="test-result-rank"><?=$_acj_rate;?></p>
                    <p class="test-result-company"><?=$_acj_company;?></p>
                    <p class="test-result-button"><a href="#detail-item-<?php the_ID(); ?>" class="button show-detail-test-result"><?php _e( "Tìm hiểu thêm", THEMEDOMAIN ); ?></a></p>
                </div>
                <div class="col-lg-3 text-center">
                    <p class="test-result-image"><img src="<?=$image_car_logo;?>" /></p>
                    <p class="test-result-vehicle"><?=$_acj_name_car;?></p>
                </div>
            </div>
        <?php endwhile; ?>
        </div><!-- END .list-test-result -->
        <div class="test-result-details">
            <?php while( $query->have_posts() ): $query->the_post(); ?>
                <div id="detail-item-<?php the_ID(); ?>" class="test-result-detail-item">
                    <?php the_content(); ?>
                    <p><a href="" class="button quay-lai"><?php _e( "Quay lại", THEMEDOMAIN ); ?></a></p>
                </div><!-- END .test-result-detail-item -->
            <?php endwhile; ?>
        </div><!-- END .test-result-details -->
    <?php endif; ?>
<?php wp_reset_postdata(); ?>
<script type="text/javascript">
    jQuery( document ).ready(function($){
        $(".button.show-detail-test-result").click(function(){
            var ID = $(this).attr('href');
            $(".list-test-result").hide();
            $(ID).show(500);
            return false;
        });
        $(".button.quay-lai").click(function(){
            $(".list-test-result").show(500);
            $(".test-result-detail-item").hide();
            return false;
        });
    });
</script>