<?php
    global $post;
    $acj_hieu_suat = get_post_meta( $post->ID, 'acj_hieu_suat', true );
    if( empty( $acj_hieu_suat ) ){
        _e( 'Không có thông tin về hiệu suất cho sản phẩm này', THEMEDOMAIN );
        return;
    }
    foreach( $acj_hieu_suat as $i=>$hs ): ?>
<?php
    $thumbnail_id = $hs['thumbnail_id'];
    if ( $thumbnail_id ) {
        $image = wp_get_attachment_thumb_url( $thumbnail_id );
    } else {
        $image = get_template_directory_uri().'/assets/images/placeholder.png';
    }
?>
<div class="row">
    <div class="col-lg-3 col-xs-3">
        <p class="name-attr">
            <span class="hs-img-icon"><img style="width:25px ; height:25px; " class="" src="<?php echo $image; ?>" alt=""/></span>
            <span><?php echo $hs['name']; ?></span>
        </p>
    </div>
    <div class="col-lg-9 col-xs-9">
        <div class="hieusuats">
            <ul>
                <?php for( $i = 1; $i <= 5; $i++ ): ?>
                    <?php if( $hs['rate'] / 2 == $i-0.5 ){ ?>
                        <li class="hieusuat hieusuat-col-<?php echo $i; ?>"><div class="col-inner w50"></div></li>
                    <?php }elseif( $hs['rate'] / 2 >= $i ){  ?>
                        <li class="hieusuat hieusuat-col-<?php echo $i; ?>"><div class="col-inner"></div></li>
                    <?php }else{?>
                        <li class="hieusuat hieusuat-col-<?php echo $i; ?>"></li>
                    <?php } ?>
                <?php endfor; ?>
                <div class="clear"></div>
            </ul>
        </div>
    </div>
</div>
<?php endforeach; ?>
<div class="row">
    <div class="col-lg-9 col-lg-offset-3">
        <div class="hieusuats bottom-nav">
            <ul>
                <li class="hieusuat hieusuat-col-1"><?php _e( 'STANDARD', THEMEDOMAIN); ?></li>
                <li class="hieusuat hieusuat-col-2"><?php _e( 'GOOD', THEMEDOMAIN); ?></li>
                <li class="hieusuat hieusuat-col-3"><?php _e( 'OPTIUM', THEMEDOMAIN); ?></li>
                <li class="hieusuat hieusuat-col-4"><?php _e( 'EXCELLENT', THEMEDOMAIN); ?></li>
                <li class="hieusuat hieusuat-col-5"><?php _e( 'SUPERIOR', THEMEDOMAIN); ?></li>
                <div class="clear"></div>
            </ul>
        </div>
    </div>
</div>