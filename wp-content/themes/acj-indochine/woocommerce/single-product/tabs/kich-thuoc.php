<div class="tab-kich-thuoc">
    <div class="panel-group" id="accordion">
        <?php
            global $post;
            $args = array('parent'=> 0);
            $terms = wp_get_post_terms( $post->ID, 'kich_thuoc', $args );
            if( !empty( $terms ) ){ $i=1;
                foreach( $terms as $term ){
                    //if( $term->parent != 0) continue;
                    $class = '';
                    if( $i==1 ) $class =' in';
                    ?>
                    <div class="panel kich-thuoc">
                        <h4 class="panel-title-kich-thuoc">
                            <a data-toggle="collapse" data-parent="#accordion" href="#term-<?php echo $term->term_id; ?>">
                                <span class="kich-thuoc-title"><?php echo $term->name; ?></span>
                                <span class="kich-thuoc-icon"></span>
                            </a>
                        </h4>
                        <div id="term-<?php echo $term->term_id; ?>" class="panel-collapse collapse<?=$class;?>">
                            <div class="panel-body text-center">
                                <?php
                                    $child_args = array('parent'=> $term->term_id);
                                    $child_terms = wp_get_post_terms( $post->ID, 'kich_thuoc', $child_args );
                                ?>
                                <?php if( !empty( $child_terms ) ): ?>
                                <div class="row">
                                    <?php $j=1; foreach( $child_terms as $child_term ): ?>
                                        <div class="col-lg-3"><?=$child_term->name;?></div>
                                        <?php if( $j % 4 == 0):?>
                                            </div>
                                            <div class="row">
                                        <?php endif; ?>
                                    <?php $j++; endforeach; ?>
                                </div>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i++;
                }
            }
        ?>
    </div>
</div><!-- END .tab-kich-thuoc -->