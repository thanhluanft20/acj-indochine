<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header(); ?>
<div class="container-flue">
	<div class="top-banner">
		<?php
			if( function_exists('is_shop') ) {
				if( is_shop() ) {
					$pageID = get_option('woocommerce_shop_page_id');
					echo get_post_meta( $pageID, '_page_top_setting_content', true );
				}
			}

		?>
	</div>
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-6 col-xm-6 column">
					<?php
					$content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
					$page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
					$description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
					if(empty($page_title)){ $page_title = __( 'Sản phẩm', THEMEDOMAIN ); }
					if(empty($description)){ $description = __( 'ACJ INDOCHINE hân hạnh giới thiệu đến quý khách các sản phẩm của công ty', THEMEDOMAIN ); }
					?>
					<h1 class="title"><?php echo $page_title; ?></h1>
					<p class="description"><?php echo $description; ?></p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xm-6 column text-right">
					<?php do_action( 'theme_breadcrumb' ); ?>
				</div>
			</div>
		</div>
	</div><!--END breadcrumbs-->
</div><!--END container-flue-->
<div class="container sanpham">
	<div class="row">
		<div class="col-lg-3">
			<?php
			/**
			 * woocommerce_sidebar hook
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action( 'woocommerce_sidebar' );
			?>
		</div>
		<div class="col-lg-9">
			<?php
			/**
			 * woocommerce_before_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 */
			do_action( 'woocommerce_before_main_content' );
			?>
			<?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

				<div class="heading-title product-archive-title">
					<h3><span><?php woocommerce_page_title(); ?></span></h3>
				</div>

			<?php endif; ?>
			<div class="row sanphams">
				<?php
				/**
				 * woocommerce_archive_description hook
				 *
				 * @hooked woocommerce_taxonomy_archive_description - 10
				 * @hooked woocommerce_product_archive_description - 10
				 */
				do_action( 'woocommerce_archive_description' );
				?>

				<?php if ( have_posts() ) : ?>
					<div class="col-md-12">
						<?php
						/**
						 * woocommerce_before_shop_loop hook
						 *
						 * @hooked woocommerce_result_count - 20
						 * @hooked woocommerce_catalog_ordering - 30
						 */
						do_action( 'woocommerce_before_shop_loop' );
						?>
					</div>

					<?php woocommerce_product_loop_start(); ?>

					<?php woocommerce_product_subcategories(); ?>

					<?php while ( have_posts() ) : the_post(); ?>

						<?php wc_get_template_part( 'content', 'product' ); ?>

					<?php endwhile; // end of the loop. ?>

					<?php woocommerce_product_loop_end(); ?>

					<?php
					/**
					 * woocommerce_after_shop_loop hook
					 *
					 * @hooked woocommerce_pagination - 10
					 */
					do_action( 'woocommerce_after_shop_loop' );
					?>

				<?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

					<div class="col-md-12">
						<?php wc_get_template( 'loop/no-products-found.php' ); ?>
					</div>

				<?php endif; ?>

				<?php
				/**
				 * woocommerce_after_main_content hook
				 *
				 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
				 */
				do_action( 'woocommerce_after_main_content' );
				?>

			</div>
		</div>
	</div>
</div>
<?php get_footer();?>
