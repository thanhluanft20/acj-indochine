<?php
/**
 * The Template for displaying all single products.
 *
 * Override this template by copying it to yourtheme/woocommerce/single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}
get_header(); ?>
<div class="container-flue">
	<div class="top-banner">
		<img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-sanpham.png" alt=""/>
	</div>
	<div class="breadcrumbs">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-md-8 col-sm-6 col-xm-6 column">
					<?php
					$content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
					$page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
					$description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
					if(empty($page_title)){ $page_title = __( 'Sản phẩm', THEMEDOMAIN ); }
					if(empty($description)){ $description = __( 'ACJ INDOCHINE hân hạnh giới thiệu đến quý khách các sản phẩm của công ty', THEMEDOMAIN ); }
					?>
					<h3 class="title"><?php echo $page_title; ?></h3>
					<p class="description"><?php echo $description; ?></p>
				</div>
				<div class="col-lg-4 col-md-4 col-sm-6 col-xm-6 column text-right">
					<?php do_action( 'theme_breadcrumb' ); ?>
				</div>
			</div>
		</div>
	</div><!--END breadcrumbs-->
</div><!--END container-flue-->
<div class="container sanpham">
	<div class="row">
		<div class="col-lg-3">
			<?php
			/**
			 * woocommerce_sidebar hook
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action( 'woocommerce_sidebar' );
			?>
		</div><!--END- col-lag-3-->

		<div class="col-lg-9">
			<?php
			/**
			 * woocommerce_before_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
			 * @hooked woocommerce_breadcrumb - 20
			 */
			do_action( 'woocommerce_before_main_content' );
			?>

			<?php while ( have_posts() ) : the_post(); ?>

				<?php wc_get_template_part( 'content', 'single-product' ); ?>

			<?php endwhile; // end of the loop. ?>

			<?php
			/**
			 * woocommerce_after_main_content hook
			 *
			 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
			 */
			do_action( 'woocommerce_after_main_content' );
			?>

			<?php
			/**
			 * woocommerce_sidebar hook
			 *
			 * @hooked woocommerce_get_sidebar - 10
			 */
			do_action( 'woocommerce_sidebar' );
			?>
		</div>
	</div>
</div>
<?php get_footer();?>
