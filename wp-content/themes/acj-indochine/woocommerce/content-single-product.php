<?php
/**
 * The template for displaying product content in the single-product.php template
 *
 * Override this template by copying it to yourtheme/woocommerce/content-single-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     1.6.4
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<?php
	/**
	 * woocommerce_before_single_product hook
	 *
	 * @hooked wc_print_notices - 10
	 */
	 do_action( 'woocommerce_before_single_product' );

	 if ( post_password_required() ) {
	 	echo get_the_password_form();
	 	return;
	 }
?>
<div class="row info">
	<div class="col-lg-4">
		<?php
		/**
		 * woocommerce_before_single_product_summary hook
		 *
		 * @hooked woocommerce_show_product_sale_flash - 10
		 * @hooked woocommerce_show_product_images - 20
		 */
		do_action( 'woocommerce_before_single_product_summary' );
		?>
	</div>
	<div class="col-lg-8">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/sp-img-title.png" alt=""/>
		<div>
			<p class="title"><?php the_title(); ?></p>
			<div class="product-single-des">
				<?php the_content(); ?>
			</div>
		</div>
		<ul class="row loai">
			<?php
				global $post;
				$_loai_lop = get_post_meta( $post->ID, '_loai_lop', true );
				$_extreme = get_post_meta( $post->ID, '_extreme', true );
				$terms = wp_get_post_terms( $post->ID, 'loai_xe' );
				$_loai_xe = array();
				foreach($terms as $term_single) {
					$_loai_xe[] = $term_single->name;
				}
			?>
			<li class="col-lg-4 loaixe"><p><span><?php _e( "Loại xe", THEMEDOMAIN ); ?>: </span><?php echo implode(', ', $_loai_xe ); ?> </p></li>
			<li class="col-lg-4 loailop"><p><span><?php _e( "Loại lốp", THEMEDOMAIN ); ?>: </span><?=$_loai_lop;?> </p></li>
			<li class="col-lg-4 extreme"><p><?=$_extreme;?></p></li>
		</ul>
		<div class="row">
			<div class="dathang col-lg-12">
				<?php global $product; do_action( 'woocommerce_' . $product->product_type . '_add_to_cart'  ); ?>
			</div>
		</div>
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div>
				<?php
				/**
				 * woocommerce_after_single_product_summary hook
				 *
				 * @hooked woocommerce_output_product_data_tabs - 10
				 * @hooked woocommerce_upsell_display - 15
				 * @hooked woocommerce_output_related_products - 20
				 */
				do_action( 'woocommerce_after_single_product_summary' );
				?>
			</div>
		</div>
	</div>
</div>
<?php do_action( 'woocommerce_after_single_product' ); ?>
