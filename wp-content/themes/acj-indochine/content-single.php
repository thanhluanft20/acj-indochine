<?php while ( have_posts() ) : the_post();?>
    <?php
        $acj_view = get_post_meta( get_the_ID(), "_acj_view", true );
        $acj_view = !empty( $acj_view ) ? $acj_view : 0;
        $acj_view = $acj_view + 1;
        update_post_meta( get_the_ID(), '_acj_view', $acj_view );
//        echo "View: ", get_post_meta( get_the_ID(), "_acj_view", true );
    ?>
    <div class="top-banner-tintuc">
        <div class="date">
            <p><?php the_date();?></p>
        </div>
        <?php the_post_thumbnail('single_post');?>
        <h3><?php the_title();?></h3>
    </div>
    <div class="author">
        <p class="author"><?php the_author();?></p>
        <p class="device"><?php the_tags(''); ?></p>
    </div>
    <div class="content">
        <?php the_content();?>
    </div>
<?php endwhile; ?>
<div class="tintuc-share">

    <ul>
        <li><p><?php _e( "Chia Sẻ", THEMEDOMAIN ); ?></p></li>
        <li><a class="face" href="https://developers.facebook.com/docs/plugins/"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-face.png" alt=""/><div class="fb-share-button" data-href="https://developers.facebook.com/docs/plugins/" data-layout="button_count"></div></a></li>
        <li><a class="twitter" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-twitter.png" alt=""/></a></li>
        <li><a class="camera" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-camera.png" alt=""/></a></li>
        <li><a class="google" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-google.png" alt=""/></a></li>
    </ul>
</div>

        <?php
            global $post;
            $current_slug = array();
            $categories = get_the_terms( $post->id, 'category' );
        if (!empty($categories)){
            foreach($categories as $slug){
                $current_slug[]=$slug->slug;
            }
            $args = array(
                'orderby'           =>'date',
                'posts_per_page'    => 3,
                'post_type'         => 'post',
                'post_status'       => 'publish',
                'post__not_in'      => array($post->ID),
                'tax_query'         => array(
                    array(
                        'taxonomy'  => 'category',
                        'field'     => 'slug',
                        'terms'     =>  $current_slug
                    ),
                ),

            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) {?>
                <div class="news-recent">
                    <div class="heading-title">
                        <h3><span><?php _e( 'TIN TỨC GẦN ĐÂY', THEMEDOMAIN ); ?></span></h3>
                    </div>
                    <div class="row">
                        <?php

                        while ( $the_query->have_posts() ) {
                            $the_query->the_post();
                            ?>

                            <div class="col-lg-4 col-md-4 col-sm-6 col-xs-12">
                                <!--                        <img class="img-responsive text-center" src="--><?php //echo get_template_directory_uri(); ?><!--/assets/images/tt-tinmoi-1.png" alt=""/>-->
                                <p class="img"><?php the_post_thumbnail('first_news_post');?></p>
                                <p class="out-title"><a  class="title" href="<?php echo get_the_permalink();?>"><?php echo get_the_title();?></a></p>
                                <p><?php echo wp_trim_words(get_the_excerpt(),15);?></p>
                                <a class="read-more" href="<?php echo get_the_permalink();?>"> <?php _e('Đọc thêm',THEMEDOMAIN);?><span class="glyphicon glyphicon-menu-right"></span></a>

                            </div>
                        <?php  } ?>
                    </div>
                </div><!--END news-recent-->
            <?php
            } else {
                // no posts found
            }
            /* Restore original Post Data */
            wp_reset_postdata();
        }?>

