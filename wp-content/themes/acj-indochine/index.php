<?php get_header(); ?>
<div class="slider" id="slider">
    <div class="slider-content">
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <?php
                    $slider_args = array(
                        'post_type' => 'acj_slider',
                        'posts_per_page' => 3
                    );
                    $sliders = new WP_Query( $slider_args );
                ?>
                <?php if($sliders->have_posts()): ?>
                    <?php while($sliders->have_posts()): $sliders->the_post(); ?>
                        <div class="item active">
                            <?php the_post_thumbnail('full') ?>
                            <div class="slider-caption">
                                <div class="container">
                                    <h2 class="slider-title"><span class="slider-left"></span><span class="slider-text">PIRELL</span><span class="slider-right"></span></h2>
                                    <h3 class="slider-title-2"><?php the_title(); ?></h3>
                                    <h4 class="slider-title-3"><?php the_excerpt(); ?></h4>
                                    <a href="" class="btn btn-slider"><?php _e( "Chi tiết", THEMEDOMAIN ) ?></a>
                                </div>
                            </div>
                        </div>
                    <?php endwhile; ?>
                <?php endif; ?>

            </div>
            <!-- Indicators -->
            <div class="container" style="position: relative;">
                <ul class="carousel-indicators">
                    <?php if($sliders->have_posts()): ?>
                    <?php while($sliders->have_posts()): $sliders->the_post(); ?>
                            <li data-target="#carousel-example-generic" data-slide-to="0" class="active">
                                <?php the_post_thumbnail('small') ?>
                            </li>
                        <?php endwhile; ?>
                    <?php endif; wp_reset_postdata(); ?>
                </ul>
            </div>
        </div>
        <div class="slider-form">
            <div class="container">
                <div class="col-md-4 col-md-offset-8 co-sm-12">
                    <div class="slider-form-content">
                        <div class="form-inner">
                            <div class="top-search-form-title">
                                <span class="form-title-left"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/caption-tile-left.png"></span>
                                <span class="form-title-text"><?php _e( "TÌM LỐP XE PHÙ HỢP", THEMEDOMAIN ); ?></span>
                                <span class="form-title-right"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/caption-tile-right.png"></span>
                            </div>
                            <div class="top-search-form-tabs">
                                <div class="tab-item active" id="type"><?php _e( "Tìm theo xe", THEMEDOMAIN ); ?></div>
                                <div class="tab-item" id="size"><?php _e( "Tìm theo size", THEMEDOMAIN ); ?></div>
                                <div class="clear"></div>
                            </div>
                            <div class="form-search-content" id="search-for-type">
                                <form name="" action="" method="POST">
                                    <ul class="list-sub-tabs">
                                        <li>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/lop-oto.png">
                                            <p><?php _e( "Lốp xe tải", THEMEDOMAIN ); ?></p>
                                        </li>
                                        <li>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/lop-xe-tai.png">
                                            <p><?php _e( "Lốp ôtô", THEMEDOMAIN ); ?></p>
                                        </li>
                                        <li>
                                            <img src="<?php echo get_template_directory_uri(); ?>/assets/images/lop-xe-may.png">
                                            <p><?php _e( "Lốp xe máy", THEMEDOMAIN ); ?></p>
                                        </li>
                                    </ul>
                                    <div class="form-input">
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option><?php _e( "Hãng xe", THEMEDOMAIN ); ?></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option><?php _e( "Mẫu xe", THEMEDOMAIN ); ?></option>
                                            </select>
                                        </div>
                                        <div class="form-group">
                                            <select class="form-control">
                                                <option><?php _e( "Năm sản xuất xe", THEMEDOMAIN ); ?></option>
                                            </select>
                                        </div>
                                        <input type="submit" value="<?php _e( "Tìm lốp xe", THEMEDOMAIN ); ?>" />
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div><!-- END .slider-form-content -->
                </div>
            </div>
        </div>
    </div><!-- END .slider-content -->
</div><!-- END #slider -->
<script type="text/javascript">
    jQuery(document).ready(function($){
        $('.carousel').carousel({
            interval: 5000
        })
    });
</script>
<div class="container">
    <div class="col-md-12 home-services">
        <div class="heading-title">
            <h3><span><?php _e( "Các sản phẩm và dịch vụ", THEMEDOMAIN ); ?></span></h3>
        </div>
        <ul class="list-home-products">
            <?php
            $argc =array(
                'post_type'         => 'tin-dich-vu',
                'orderby'           => 'date',
                'post_per_page'     =>12
            );
            $the_query = new WP_Query($argc);
            if ( $the_query->have_posts() ) {
                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    ?>
                    <li>
                        <a href="<?php the_permalink(); ?>" class="home-product-img"><img class="icon" src="<?php echo get_post_meta( get_the_ID(), '_acj_icon_home', true ); ?>" alt=""/></a>
                        <a href="<?php the_permalink(); ?>" class="home-product-title"><?php the_title();?></a>
                    </li>

                    <?php
                }
            }
            /* Restore original Post Data */
            wp_reset_postdata();

            ?>
        </ul>
    </div>
    <div class="main col-md-9" id="main">
        <div class="heading-title">
            <h3><span><?php _e( "CHÀO MỪNG ĐẾN VỚI ACJ INDOCHINE", THEMEDOMAIN ); ?></span></h3>
        </div>
        <div class="row">
            <div class="col-md-4">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/about-us.jpg" alt="About Us" />
            </div>
            <div class="col-md-8">
                <div class="row">
                    <div class="col-md-12">
                        <h4>ACJ INDOCHINE HÂN HẠNH ĐƯỢC PHỤC VỤ QUÝ KHÁCH</h4>
                        <p>Với uy tín trong ngành, đội ngũ kỹ thuật viên giàu kinh nghiệm, chế độ bảo hành từ chính hãng, máy móc và cơ sở vật chất được đầu tư vào loại hiện đại bậc nhất hiện nay, ACJ INDOCHINE tự tin mang đến sự hài lòng cho quý khách khi mua sản phẩm và sử dụng dịch vụ của chúng tôi. </p>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/chuyen-nghiep-tan-tam.png" alt="Chuyên nghiệp - tận tâm" />
                        </div>
                        <div class="service-text">
                            <h4>CHUYÊN NGHIỆP - TẬN TÂM</h4>
                            <p>Đội ngũ kỹ thuật viên giàu kinh nghiệm và cơ sở vật chất được đầu tư vào loại hiện đại bậc nhất hiện nay</p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/mau-ma-da-dang.png" alt="MẪU MÃ ĐA DẠNG" />
                        </div>
                        <div class="service-text">
                            <h4>MẪU MÃ ĐA DẠNG</h4>
                            <p><strong>ACJ INDOCHINE</strong> là đại lý phân phối của hãng lốp xe hàng đầu thế giới PIRELLI.</p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/uu-dai-tot.png" alt="ƯU ĐÃI TỐT" />
                        </div>
                        <div class="service-text">
                            <h4>ƯU ĐÃI TỐT</h4>
                            <p>Khi thay lốp xe tại ACJ INDOCHINE, quý khách sẽ được lắp ráp, cân mâm và bấm chì miễn phí. </p>
                        </div>
                    </div>
                    <div class="col-md-6 col-sm-12 col-xs-12">
                        <div class="service-image">
                            <img src="<?php echo get_template_directory_uri(); ?>/assets/img/dich-vu-chuyen-sau.png" alt="Dịch vụ chuyên sâu" />
                        </div>
                        <div class="service-text">
                            <h4>Dịch vụ chuyên sâu</h4>
                            <p>Ngoài ra, chúng tôi còn cung cấp dịch vụ: Chỉnh thước tay lái, Sửa chữa nhanh, thay nhớt, Bảo dưỡng gầm...</p>
                        </div>
                    </div>
                </div>
            </div>
        </div><!-- END .row -->

        <div class="heading-title">
            <h3><span><?php _e( "Sản phẩm mới", THEMEDOMAIN ); ?></span></h3>
        </div>
        <div class="row product-items">
            <div class="col-md-4 product-item text-center">
                <div class="product-img">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-1.png" alt="Product 1" /></a>
                </div>
                <div class="product-description">
                    <h4>ICE ZERO</h4>
                    <p>Kích thước: 205/45R17 / TT - TĐ: 88W <br />Made in ROMANI</p>
                </div>
            </div><!-- END .product-item -->

            <div class="col-md-4 product-item text-center">
                <div class="product-img">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-2.png" alt="Product 2" /></a>
                </div>
                <div class="product-description">
                    <h4>ICE ZERO</h4>
                    <p>Kích thước: 205/45R17 / TT - TĐ: 88W <br />Made in ROMANI</p>
                </div>
            </div><!-- END .product-item -->

            <div class="col-md-4 product-item text-center">
                <div class="product-img">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-3.png" alt="Product 3" /></a>
                </div>
                <div class="product-description">
                    <h4>ICE ZERO</h4>
                    <p>Kích thước: 205/45R17 / TT - TĐ: 88W <br />Made in ROMANI</p>
                </div>
            </div><!-- END .product-item -->
        </div><!-- END .row.product-items -->
        <div class="row product-items">
            <div class="col-md-4 product-item text-center">
                <div class="product-img">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-1.png" alt="Product 1" /></a>
                </div>
                <div class="product-description">
                    <h4>ICE ZERO</h4>
                    <p>Kích thước: 205/45R17 / TT - TĐ: 88W <br />Made in ROMANI</p>
                </div>
            </div><!-- END .product-item -->

            <div class="col-md-4 product-item text-center">
                <div class="product-img">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-2.png" alt="Product 2" /></a>
                </div>
                <div class="product-description">
                    <h4>ICE ZERO</h4>
                    <p>Kích thước: 205/45R17 / TT - TĐ: 88W <br />Made in ROMANI</p>
                </div>
            </div><!-- END .product-item -->

            <div class="col-md-4 product-item text-center">
                <div class="product-img">
                    <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-3.png" alt="Product 3" /></a>
                </div>
                <div class="product-description">
                    <h4>ICE ZERO</h4>
                    <p>Kích thước: 205/45R17 / TT - TĐ: 88W <br />Made in ROMANI</p>
                </div>
            </div><!-- END .product-item -->
        </div><!-- END .row.product-items -->
    </div><!-- END #main -->
    <div class="sidebar col-md-3" id="sidebar">
        <div class="widget">
            <a href="" class="adv">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/adv-1.jpg" alt="Advertisement 1" />
            </a>
            <a href="" class="adv">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/img/adv-2.jpg" alt="Advertisement 2" />
            </a>
        </div>
        <div class="widget">
            <div class="heading-title">
                <h3><span>Bán chạy nhất</span></h3>
            </div>
            <div class="widget-products">
                <ul class="ul-widget-products">
                    <li>
                        <div class="pr-img">
                            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-1.png" alt="Product 1" /></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a href="" class="widget-price-button">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-1.png" alt="Product 1" /></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a href="" class="widget-price-button">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-1.png" alt="Product 1" /></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a href="" class="widget-price-button">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-1.png" alt="Product 1" /></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a href="" class="widget-price-button">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/img/product-im-1.png" alt="Product 1" /></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a href="" class="widget-price-button">$234.79</a>
                            </p>
                        </div>
                    </li>

                </ul>
            </div><!-- END .widget-products -->
        </div>
    </div><!-- END sidebar -->

</div><!-- END .container -->

<?php get_footer(); ?>