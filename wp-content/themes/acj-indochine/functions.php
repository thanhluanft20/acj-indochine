<?php

if (!isset($content_width)) {
    $content_width = 330;
}

if (!class_exists('ACJ_Theme')):
    class ACJ_Theme
    {

        /**
         * Construct class
         */
        function __construct()
        {
            $this->defines();
            $this->includes();
            add_action( 'after_setup_theme', array( __CLASS__, 'theme_setup' ) );
            add_action( 'wp_enqueue_scripts', array( __CLASS__, 'theme_scripts' ) );
            add_action( 'widgets_init', array( __CLASS__, 'widgets_init' ) );
            add_action( 'admin_menu', array( __CLASS__, 'add_menu_item' ) );
            add_action( 'nav_menu_item_title', array( __CLASS__, 'change_menu_title' ), 10, 4 );
            add_action( 'init', array( __CLASS__, 'custom_post_type_tuvan' ) );
            add_action( 'add_meta_boxes', array( __CLASS__, 'post_meta_boxes' ) );
            add_action( 'save_post', array( __CLASS__, 'save_post_data' ) );
            add_action( 'theme_breadcrumb', array( __CLASS__, 'theme_breadcrumb') );
            add_action( 'init', array( __CLASS__, 'init' ), 1 );
        }
        function init(){
            remove_action( 'woocommerce_before_main_content', 'woocommerce_breadcrumb', 20, 0 );
            remove_action( 'woocommerce_before_shop_loop', 'woocommerce_catalog_ordering', 30, 0 );
            remove_filter( 'woocommerce_product_tabs', 'woocommerce_default_product_tabs' );
            add_filter( 'woocommerce_product_tabs', array( __CLASS__, 'woocommerce_default_product_tabs' ) );

        }

        /**
         * Define all constants
         */
        function defines()
        {
            define('THEMEDOMAIN', 'acj-theme');
        }

        function includes(){
            include( 'inc/taxonomy-options.php' );
            include( 'inc/product-custom.php' );
            include( 'inc/product-search.php' );
            include( 'inc/widgets/class-dich-vu-widget.php' );
            include( 'inc/widgets/class-kien-thuc-widget.php' );
            include( 'inc/widgets/class-san-pham-widget.php' );
            include( 'inc/class-primary-menu-walker.php' );
        }

        /**
         * Setup options for theme
         */
        function theme_setup()
        {
            /*
             * Make theme available for translation.
             * Translations can be filed in the /languages/ directory.
             */
            load_theme_textdomain(THEMEDOMAIN, get_template_directory() . '/languages');

            // Add default posts and comments RSS feed links to head.
            add_theme_support('automatic-feed-links');
            add_theme_support('woocommerce');

            /*
             * Let WordPress manage the document title.
             * By adding theme support, we declare that this theme does not use a
             * hard-coded <title> tag in the document head, and expect WordPress to
             * provide it for us.
             */
            add_theme_support('title-tag');

            /*
             * Enable support for Post Thumbnails on posts and pages.
             *
             * See: https://codex.wordpress.org/Function_Reference/add_theme_support#Post_Thumbnails
             */
            add_theme_support('post-thumbnails');
            set_post_thumbnail_size(825, 510, true);

            // This theme uses wp_nav_menu() in two locations.
            register_nav_menus(array(
                'primary' => __('Menu chính', THEMEDOMAIN),
                'footer' => __('Menu cuối trang', THEMEDOMAIN),
            ));

            /*
             * Switch default core markup for search form, comment form, and comments
             * to output valid HTML5.
             */
            add_theme_support('html5', array(
                'search-form', 'comment-form', 'comment-list', 'gallery', 'caption'
            ));

            /*
             * Enable support for Post Formats.
             *
             * See: https://codex.wordpress.org/Post_Formats
             */
            add_theme_support('post-formats', array(
                'aside', 'image', 'video', 'quote', 'link', 'gallery', 'status', 'audio', 'chat'
            ));
        }

        /**
         * Register and load all scripts
         */
        function theme_scripts()
        {

            // Style
            wp_enqueue_style('bootstrap-style', get_template_directory_uri() . "/assets/css/bootstrap.min.css");
            wp_enqueue_style('Roboto-Font', "https://fonts.googleapis.com/css?family=Roboto:400,700,900,400italic,100&subset=latin,vietnamese");
            wp_enqueue_style('font-awesome-style', get_template_directory_uri() . "/assets/css/font-awesome.min.css");
            wp_enqueue_style('acj-style', get_stylesheet_uri());
            wp_enqueue_style('acj-style-responsive', get_template_directory_uri()."/responsive.css" );

            // Script
            wp_enqueue_script('jquery', get_template_directory_uri() . '/assets/js/jquery-1.11.3.min.js', array(), '1.11.3', false);
            wp_enqueue_script('bootstrap-js', get_template_directory_uri() . '/assets/js/bootstrap.min.js', array('jquery'), '3.3.6', true);
        }

        /**
         * Register all widgets for theme
         */
        function widgets_init(){
            register_sidebar( array(
                'name' => __( ' Văn Bản Email Khách Hàng', THEMEDOMAIN ),
                'id' => 'van-ban-email-khach-hang',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở dưới trang', THEMEDOMAIN ),
                'before_widget' => '<div id="%1$s" class="widget  %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Email Khách Hàng', THEMEDOMAIN ),
                'id' => 'email-khach-hang',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở dưới trang', THEMEDOMAIN ),
                'before_widget' => '<div id="%1$s" class="widget  %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );

            register_sidebar( array(
                'name' => __( 'Khu vực cuối trang', THEMEDOMAIN ),
                'id' => 'footer-area',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở dưới trang', THEMEDOMAIN ),
                'before_widget' => '<div id="%1$s" class="widget col-md-3 %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Thanh bên trang dịch vụ chi tiết', THEMEDOMAIN ),
                'id' => 'service-sidebar',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở dưới trang', THEMEDOMAIN ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );
            register_sidebar( array(
                'name' => __( 'Thanh bên trang sản phẩm', THEMEDOMAIN ),
                'id' => 'shop-sidebar',
                'description' => __( 'Tất cả các khung bạn muốn đặt ở bên trái trang sản phẩm', THEMEDOMAIN ),
                'before_widget' => '<div id="%1$s" class="widget %2$s">',
                'after_widget'  => '</div><!-- END .widget -->',
                'before_title'  => '<h2 class="widget-title">',
                'after_title'   => '</h2>',
            ) );
        }

        /**
         * Add menu items
         */
        function add_menu_item(){
            //add_menu_page( __( 'Tùy chỉnh giao diện', THEMEDOMAIN ), __( 'Tùy chỉnh giao diện', THEMEDOMAIN ), 'manage_options', 'theme-option', array( __CLASS__, 'theme_option_panel' ), null, 99);
        }

        /**
         * Include theme option panel
         */
        function theme_option_panel(){
            ?>
            <div class="wrap">
                <h1><?php _e( "Tùy chỉnh giao diện", THEMEDOMAIN); ?></h1>
                <form method="post" action="options.php">
                    <?php
                    settings_fields("section");
                    do_settings_sections("theme-options");
                    submit_button();
                    ?>
                </form>
            </div>
            <?php
        }
        function change_menu_title( $title, $item, $args, $depth ){
            $title = '<span>'.$title.'</span>';
            return $title;
        }
        // Register Custom Post Type
        function custom_post_type_tuvan() {

            $labels = array(
                'name'                  => __( 'Tư Vấn Kỹ Thuật', THEMEDOMAIN ),
                'singular_name'         => __( 'Tư Vấn Kỹ Thuật', THEMEDOMAIN ),
                'menu_name'             => __( 'Tư Vấn Kỹ Thuật', THEMEDOMAIN ),
                'name_admin_bar'        => __( 'Tư Vấn Kỹ Thuật', THEMEDOMAIN ),
                'archives'              => __( 'Tư Vấn Kỹ Thuật', THEMEDOMAIN ),
                'parent_item_colon'     => __( 'Tư Vấn Kỹ Thuật hiện tại:', THEMEDOMAIN ),
                'all_items'             => __( 'Tất cả', THEMEDOMAIN ),
                'add_new_item'          => __( 'Thêm mới tư vấn kỹ thuật', THEMEDOMAIN ),
                'add_new'               => __( 'Thêm mới', THEMEDOMAIN ),
                'new_item'              => __( 'Tư vấn kỹ thuật mới', THEMEDOMAIN ),
                'edit_item'             => __( 'Sửa', THEMEDOMAIN ),
                'update_item'           => __( 'Cập nhật', THEMEDOMAIN ),
                'view_item'             => __( 'Xem', THEMEDOMAIN ),
                'search_items'          => __( 'Tìm kiếm', THEMEDOMAIN ),
                'not_found'             => __( 'Không tìm thấy', THEMEDOMAIN ),
                'not_found_in_trash'    => __( 'Không tìm thấy trong thùng rác', THEMEDOMAIN ),
                'featured_image'        => __( 'Hình ảnh nổi bật', THEMEDOMAIN ),
                'set_featured_image'    => __( 'Đặt hình ảnh nổi bật', THEMEDOMAIN ),
                'remove_featured_image' => __( 'Xóa hình ảnh nổi bật', THEMEDOMAIN ),
                'use_featured_image'    => __( 'Sử dụng hình này làm hình ảnh nổi bật', THEMEDOMAIN ),
                'insert_into_item'      => __( 'Chèn thêm', THEMEDOMAIN ),
                'uploaded_to_this_item' => __( 'Đăng tải tư vấn kỹ thuật này', THEMEDOMAIN ),
                'items_list'            => __( 'Danh sách tư vấn kỹ thuật', THEMEDOMAIN ),
                'items_list_navigation' => __( 'Danh mục tư vấn kỹ thuật', THEMEDOMAIN ),
                'filter_items_list'     => __( 'Lọc danh sách tư vấn kỹ thuật', THEMEDOMAIN ),
            );
            $args = array(
                'label'                 => __( 'Tư Vấn Kỹ Thuật', THEMEDOMAIN ),
                'description'           => __( 'Tất cả thông tin về tư vấn và kỹ thuật', THEMEDOMAIN ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'tuvankythuat', $args );

            $labels = array(
                'name'                  => __( 'Sliders', THEMEDOMAIN ),
                'singular_name'         => __( 'Sliders', THEMEDOMAIN ),
                'menu_name'             => __( 'Sliders', THEMEDOMAIN ),
                'name_admin_bar'        => __( 'Sliders', THEMEDOMAIN ),
                'archives'              => __( 'Slider Archives', THEMEDOMAIN ),
                'parent_item_colon'     => __( 'Parent Slider:', THEMEDOMAIN ),
                'all_items'             => __( 'All Sliders', THEMEDOMAIN ),
                'add_new_item'          => __( 'Add New Slider', THEMEDOMAIN ),
                'add_new'               => __( 'Add New', THEMEDOMAIN ),
                'new_item'              => __( 'New Slider', THEMEDOMAIN ),
                'edit_item'             => __( 'Edit Slider', THEMEDOMAIN ),
                'update_item'           => __( 'Update Slider', THEMEDOMAIN ),
                'view_item'             => __( 'View Slider', THEMEDOMAIN ),
                'search_items'          => __( 'Search Slider', THEMEDOMAIN ),
                'not_found'             => __( 'Not found', THEMEDOMAIN ),
                'not_found_in_trash'    => __( 'Not found in Trash', THEMEDOMAIN ),
                'featured_image'        => __( 'Featured Image', THEMEDOMAIN ),
                'set_featured_image'    => __( 'Set featured image', THEMEDOMAIN ),
                'remove_featured_image' => __( 'Remove featured image', THEMEDOMAIN ),
                'use_featured_image'    => __( 'Use as featured image', THEMEDOMAIN ),
                'insert_into_item'      => __( 'Insert into item', THEMEDOMAIN ),
                'uploaded_to_this_item' => __( 'Uploaded to this item', THEMEDOMAIN ),
                'items_list'            => __( 'Sliders list', THEMEDOMAIN ),
                'items_list_navigation' => __( 'Sliders list navigation', THEMEDOMAIN ),
                'filter_items_list'     => __( 'Filter sliders list', THEMEDOMAIN ),
            );
            $args = array(
                'label'                 => __( 'Sliders', THEMEDOMAIN ),
                'description'           => __( 'Sliders', THEMEDOMAIN ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'acj_slider', $args );

            $labels = array(
                'name'                  => __( 'Tuyển dụng', 'Tuyển dụng', THEMEDOMAIN ),
                'singular_name'         => __( 'Tuyển dụng', THEMEDOMAIN ),
                'menu_name'             => __( 'Tuyển dụng', THEMEDOMAIN ),
                'name_admin_bar'        => __( 'Tuyển dụng', THEMEDOMAIN ),
                'archives'              => __( 'Tuyển dụng Lưu trữ', THEMEDOMAIN ),
                'parent_item_colon'     => __( 'Tuyển dụng hiện tại:', THEMEDOMAIN ),
                'all_items'             => __( 'Tất cả tuyển dụng', THEMEDOMAIN ),
                'add_new_item'          => __( 'Thêm mới tuyển dụng', THEMEDOMAIN ),
                'add_new'               => __( 'Thêm mới', THEMEDOMAIN ),
                'new_item'              => __( 'Tuyển dụng mới', THEMEDOMAIN ),
                'edit_item'             => __( 'Sửa tuyển dụng', THEMEDOMAIN ),
                'update_item'           => __( 'Cập nhật tuyển dụng', THEMEDOMAIN ),
                'view_item'             => __( 'Xem tuyển dụng', THEMEDOMAIN ),
                'search_items'          => __( 'Tìm kiếm tuyển dụng', THEMEDOMAIN ),
                'not_found'             => __( 'Không tìm thấy', THEMEDOMAIN ),
                'not_found_in_trash'    => __( 'Không tìm thấy trong thùng rác', THEMEDOMAIN ),
                'featured_image'        => __( 'Hình ảnh nổi bật', THEMEDOMAIN ),
                'set_featured_image'    => __( 'Đặt hình ảnh nổi bật', THEMEDOMAIN ),
                'remove_featured_image' => __( 'Xóa hình ảnh nổi bật', THEMEDOMAIN ),
                'use_featured_image'    => __( 'Sử dụng với hình ảnh nổi bật', THEMEDOMAIN ),
                'insert_into_item'      => __( 'Chèn tin tuyển dụng', THEMEDOMAIN ),
                'uploaded_to_this_item' => __( 'Đăng tin này', THEMEDOMAIN ),
                'items_list'            => __( 'Danh sách tin', THEMEDOMAIN ),
                'items_list_navigation' => __( 'Danh sách menu tuyển dụng', THEMEDOMAIN ),
                'filter_items_list'     => __( 'Lọc tin tuyển dụng', THEMEDOMAIN ),
            );
            $args = array(
                'label'                 => __( 'Tuyển dụng', THEMEDOMAIN ),
                'description'           => __( 'Tuyển dụng', THEMEDOMAIN ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'tin-tuyen-dung', $args );


            // Register Custom Post Type
            $labels = array(
                'name'                  => __( 'Dịch Vụ', THEMEDOMAIN ),
                'singular_name'         => __( 'Dịch Vụ', THEMEDOMAIN ),
                'menu_name'             => __( 'Dịch Vụ', THEMEDOMAIN ),
                'name_admin_bar'        => __( 'Dịch Vụ', THEMEDOMAIN ),
                'archives'              => __( 'Dịch vụ lưu trử', THEMEDOMAIN ),
                'parent_item_colon'     => __( 'Dịch vụ cha', THEMEDOMAIN ),
                'all_items'             => __( 'Tất cả dịch vụ', THEMEDOMAIN ),
                'add_new_item'          => __( 'Thêm Mới dịch vụ', THEMEDOMAIN ),
                'add_new'               => __( 'Thêm Mới ', THEMEDOMAIN ),
                'new_item'              => __( 'Thêm Mới dịch vụ', THEMEDOMAIN ),
                'edit_item'             => __( 'Sửa dịch vụ', THEMEDOMAIN ),
                'update_item'           => __( 'Cập nhật dịch vụ', THEMEDOMAIN ),
                'view_item'             => __( 'Xem dịch vụ', THEMEDOMAIN ),
                'search_items'          => __( 'Tìm kiếm dịch vụ', THEMEDOMAIN ),
                'not_found'             => __( 'Không tìm thấy', THEMEDOMAIN ),
                'not_found_in_trash'    => __( 'Không tìm thấy trong thùng rác', THEMEDOMAIN ),
                'featured_image'        => __( 'Hình ảnh nổi bật', THEMEDOMAIN ),
                'set_featured_image'    => __( 'Đặt hình ảnh nổi bật', THEMEDOMAIN ),
                'remove_featured_image' => __( 'Xóa hình ảnh nổi bật', THEMEDOMAIN ),
                'use_featured_image'    => __( 'Sử dụng với hình ảnh nổi bật', THEMEDOMAIN ),
                'insert_into_item'      => __( 'Chèn tin tuyển dụng', THEMEDOMAIN ),
                'uploaded_to_this_item' => __( 'Đăng tin này', THEMEDOMAIN ),
                'items_list'            => __( 'Danh sách tin', THEMEDOMAIN ),
                'items_list_navigation' => __( 'Danh sách menu dịch vụ', THEMEDOMAIN ),
                'filter_items_list'     => __( 'Lọc tin dịch vụ', THEMEDOMAIN ),
            );
            $args = array(
                'label'                 => __( 'Dịch Vụ', THEMEDOMAIN ),
                'description'           => __( 'Post Type Description', THEMEDOMAIN ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'excerpt', 'author', 'thumbnail', 'comments', 'trackbacks', 'revisions', 'custom-fields', 'page-attributes', 'post-formats', ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'tin-dich-vu', $args );
            
            // Register Custom Post Type
            $labels = array(
                'name'                  => __( 'Kết quả kiểm tra', THEMEDOMAIN ),
                'singular_name'         => __( 'Kết quả kiểm tra', THEMEDOMAIN ),
                'menu_name'             => __( 'Kết quả kiểm tra', THEMEDOMAIN ),
                'name_admin_bar'        => __( 'Kết quả kiểm tra', THEMEDOMAIN ),
                'archives'              => __( 'Kết quả kiểm tra lưu trử', THEMEDOMAIN ),
                'parent_item_colon'     => __( 'Kết quả kiểm tra cha', THEMEDOMAIN ),
                'all_items'             => __( 'Tất cả Kết quả kiểm tra', THEMEDOMAIN ),
                'add_new_item'          => __( 'Thêm Mới Kết quả kiểm tra', THEMEDOMAIN ),
                'add_new'               => __( 'Thêm Mới ', THEMEDOMAIN ),
                'new_item'              => __( 'Thêm Mới Kết quả kiểm tra', THEMEDOMAIN ),
                'edit_item'             => __( 'Sửa Kết quả kiểm tra', THEMEDOMAIN ),
                'update_item'           => __( 'Cập nhật Kết quả kiểm tra', THEMEDOMAIN ),
                'view_item'             => __( 'Xem Kết quả kiểm tra', THEMEDOMAIN ),
                'search_items'          => __( 'Tìm kiếm Kết quả kiểm tra', THEMEDOMAIN ),
                'not_found'             => __( 'Không tìm thấy', THEMEDOMAIN ),
                'not_found_in_trash'    => __( 'Không tìm thấy trong thùng rác', THEMEDOMAIN ),
                'featured_image'        => __( 'Hình ảnh nổi bật', THEMEDOMAIN ),
                'set_featured_image'    => __( 'Đặt hình ảnh nổi bật', THEMEDOMAIN ),
                'remove_featured_image' => __( 'Xóa hình ảnh nổi bật', THEMEDOMAIN ),
                'use_featured_image'    => __( 'Sử dụng với hình ảnh nổi bật', THEMEDOMAIN ),
                'insert_into_item'      => __( 'Chèn tin Kết quả kiểm tra', THEMEDOMAIN ),
                'uploaded_to_this_item' => __( 'Đăng tin này', THEMEDOMAIN ),
                'items_list'            => __( 'Danh sách tin', THEMEDOMAIN ),
                'items_list_navigation' => __( 'Danh sách menu Kết quả kiểm tra', THEMEDOMAIN ),
                'filter_items_list'     => __( 'Lọc tin Kết quả kiểm tra', THEMEDOMAIN ),
            );
            $args = array(
                'label'                 => __( 'Kết quả kiểm tra', THEMEDOMAIN ),
                'description'           => __( 'Mô tả kết quả kiểm tra', THEMEDOMAIN ),
                'labels'                => $labels,
                'supports'              => array( 'title', 'editor', 'author', 'thumbnail' ),
                'hierarchical'          => false,
                'public'                => true,
                'show_ui'               => true,
                'show_in_menu'          => true,
                'menu_position'         => 5,
                'show_in_admin_bar'     => true,
                'show_in_nav_menus'     => true,
                'can_export'            => true,
                'has_archive'           => true,
                'exclude_from_search'   => false,
                'publicly_queryable'    => true,
                'capability_type'       => 'page',
            );
            register_post_type( 'tin-ket-qua-kiem-tra', $args );
        }
        function post_meta_boxes(){
            add_meta_box(
                'tuyen-dung-meta-box-options',
                __( 'Tùy chỉnh tuyển dụng', THEMEDOMAIN ),
                array( __CLASS__, 'tuyen_dung_meta_box_options' ),
                'tin-tuyen-dung',
                'side'
            );
            add_meta_box(
                'dich-vu-meta-box-options',
                __( 'Tùy chỉnh dịch vụ', THEMEDOMAIN ),
                array( __CLASS__, 'dich_vu_meta_box_options' ),
                'tin-dich-vu',
                'side'
            );
            add_meta_box(
                'home-dich-vu-meta-box-options',
                __( 'Icon trang chủ', THEMEDOMAIN ),
                array( __CLASS__, 'home_dich_vu_meta_box_options' ),
                'tin-dich-vu',
                'side'
            );
            add_meta_box(
                'opticab-top-setting',
                __( 'Tùy chỉnh trang', THEMEDOMAIN ),
                array( __CLASS__, 'top_page_setting' ),
                'page'
            );
            add_meta_box(
                'acj-test-result-settings',
                __( 'Tùy chỉnh kết quả kiểm tra', THEMEDOMAIN ),
                array( __CLASS__, 'test_result_settings_meta_box' ),
                'tin-ket-qua-kiem-tra'
            );
        }
        function home_dich_vu_meta_box_options($post){
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
            //wp_register_script('my-upload', WP_PLUGIN_URL.'/my-script.js', array('jquery','media-upload','thickbox'));
            //wp_enqueue_script('my-upload');
            wp_nonce_field( 'acj_theme_dich_vu_home_nonce', 'acj_theme_nonce' );
            $acj_icon_home = get_post_meta( $post->ID, '_acj_icon_home', true );
            ?>
            <p>
                <label for="_acj_icon"><?php _e( 'Chọn Icon', THEMEDOMAIN ); ?></label><br />
                <input id="acj_icon_hom" type="text" style="width: 100%;"  name="_acj_icon_home" value="<?php echo esc_attr( $acj_icon_home ); ?>" />
                <div class="image-icon-home">
                    <img src="<?php echo esc_attr( $acj_icon_home ); ?>" alt=""/>
                </div>
                <input id="upload_image_button_home" type="button" value="Upload Image" />
            </p>
            <script type="text/javascript">
                jQuery(document).ready( function( $ ) {

                    $('#upload_image_button_home').click(function() {

                        formfield = $('#acj_icon_hom').attr('name');
                        tb_show( '', 'media-upload.php?type=image&amp;TB_iframe=true' );
                        return false;
                    });

                    window.send_to_editor = function(html) {
                        $('.image-icon-home').html(html);
                        $('#acj_icon_hom').val($('.image-icon-home img').attr('src'));
                        tb_remove();
                    }

                });
            </script>
        <?php
        }

        function tuyen_dung_meta_box_options( $post ){
            // Add a nonce field so we can check for it later.
            wp_nonce_field( 'acj_theme_tuyen_dung_nonce', 'acj_theme_nonce' );
            $position = get_post_meta( $post->ID, '_acj_position', true );
            $employee_number = get_post_meta( $post->ID, '_acj_employee_number', true );
            $acj_status = get_post_meta( $post->ID, '_acj_status', true );
            ?>
            <p>
                <label for="acj_position"><?php _e( 'Vị trí', THEMEDOMAIN ); ?></label><br />
                <input type="text" name="_acj_position" id="acj_position" value="<?php echo esc_attr( $position ); ?>" />
            </p>
            <p>
                <label for="acj_employee_number"><?php _e( 'Số lượng', THEMEDOMAIN ); ?></label><br />
                <input type="text" name="_acj_employee_number" id="acj_employee_number" value="<?php echo esc_attr( $employee_number ); ?>" />
            </p>
            <p>
                <label for="acj_status"><?php _e( 'Trạng thái', THEMEDOMAIN ); ?></label><br />
                <input type="text" name="_acj_status" id="acj_status" value="<?php echo esc_attr( $acj_status ); ?>" />
            </p>
            <?php
        }
        function dich_vu_meta_box_options($post){
            wp_enqueue_script('jquery');
            wp_enqueue_media();
            wp_enqueue_script('media-upload');
            wp_enqueue_script('thickbox');
            wp_nonce_field( 'acj_theme_dich_vu_nonce', 'acj_theme_nonce' );
            $acj_icon = get_post_meta( $post->ID, '_acj_icon', true );
            ?>
            <p>
                <label for="_acj_icon"><?php _e( 'Chọn Icon', THEMEDOMAIN ); ?></label><br />
                <input id="acj_icon" type="text" style="width: 100%;"  name="_acj_icon" value="<?php echo esc_attr( $acj_icon ); ?>" />
                <div class="image-icon">
                    <img src="<?php echo esc_attr( $acj_icon ); ?>" alt=""/>
                </div>
                <input id="upload_image_button" class="upload_image_button" type="button" value="Upload Image" />
            </p>
            <script type="text/javascript">

            // Uploading files
            var file_frame;
            var ID = null;

            jQuery( document ).on( 'click', '.upload_image_button', function( event ) {

                event.preventDefault();

                // If the media frame already exists, reopen it.
                if ( file_frame ) {
                    file_frame.open();
                    return;
                }

                // Create the media frame.
                file_frame = wp.media.frames.downloadable_file = wp.media({
                    title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                    button: {
                        text: '<?php _e( "Use image", "woocommerce" ); ?>'
                    },
                    multiple: false
                });

                // When an image is selected, run a callback.
                file_frame.on( 'select', function() {
                    var attachment = file_frame.state().get( 'selection' ).first().toJSON();
                    jQuery('#acj_icon').val( attachment.sizes.full.url );
                    jQuery('.image-icon').find( 'img' ).attr( 'src', attachment.sizes.full.url );
                });

                // Finally, open the modal.
                file_frame.open();
            });

            </script>
        <?php
        }
        function save_post_data( $post_id ){

            // Verify that the nonce is valid.
            if ( wp_verify_nonce( $_POST['acj_theme_nonce'], 'acj_theme_tuyen_dung_nonce' ) ) {
                // Make sure that it is set.
                if ( isset( $_POST['_acj_position'] ) ) {
                    // Update the meta field in the database.
                    update_post_meta( $post_id, '_acj_position', sanitize_text_field( $_POST['_acj_position'] ) );
                }
                if ( isset( $_POST['_acj_employee_number'] ) ) {
                    // Update the meta field in the database.
                    update_post_meta( $post_id, '_acj_employee_number', sanitize_text_field( $_POST['_acj_employee_number'] ) );
                }
                if ( isset( $_POST['_acj_status'] ) ) {
                    // Update the meta field in the database.
                    update_post_meta( $post_id, '_acj_status', sanitize_text_field( $_POST['_acj_status'] ) );
                }
            }
            // Update the meta field in the database.
            update_post_meta( $post_id, '_page_top_setting_title', $_POST['_page_top_setting_title'] );
            update_post_meta( $post_id, '_page_top_setting_content', $_POST['_page_top_setting_content'] );
            update_post_meta( $post_id, '_breadcrumb_description', $_POST['_breadcrumb_description'] );

            if (isset($_POST['_acj_icon'])){
                update_post_meta( $post_id, '_acj_icon', sanitize_text_field( $_POST['_acj_icon'] ) );
            }
            if ( wp_verify_nonce( $_POST['acj_theme_nonce'], 'acj_theme_dich_vu_home_nonce' ) ) {
                if (isset($_POST['_acj_icon_home'])){
                    update_post_meta( $post_id, '_acj_icon_home', sanitize_text_field( $_POST['_acj_icon_home'] ) );
                }
            }
            if ( wp_verify_nonce( $_POST['acj_theme_nonce'], 'acj_theme_test_result_nonce' ) ) {
                update_post_meta( $post_id, '_acj_product', $_POST['_acj_product'] );
                update_post_meta( $post_id, '_acj_date', $_POST['_acj_date'] );
                update_post_meta( $post_id, '_acj_rate', $_POST['_acj_rate'] );
                update_post_meta( $post_id, '_acj_company', $_POST['_acj_company'] );
                update_post_meta( $post_id, '_acj_logo', $_POST['_acj_logo'] );
                update_post_meta( $post_id, '_acj_name_car', $_POST['_acj_name_car'] );
                update_post_meta( $post_id, '_acj_car_logo', $_POST['_acj_car_logo'] );
            }
        }
        function top_page_setting($post){
            // Add a nonce field so we can check for it later.
            wp_nonce_field( THEMEDOMAIN, 'page_top_setting' );
            /*
             * Use get_post_meta() to retrieve an existing value
             * from the database and use the value for the form.
             */
            $value = get_post_meta( $post->ID, '_page_top_setting_content', true );
            $page_title = get_post_meta( $post->ID, '_page_top_setting_title', true );
            $description = get_post_meta( $post->ID, '_breadcrumb_description', true );
            $page_title = !empty($page_title) ? $page_title : $post->post_title;
            ?>
            <div id="titlewrap">
                <label><?php _e( "Tiêu đề", THEMEDOMAIN ); ?>:</label>
                <input style="width: 100%;" type="text" name="_page_top_setting_title" size="30" value="<?php echo $page_title; ?>" id="title" spellcheck="true" autocomplete="off">
            </div>
            <div id="titlewrap">
                <label><?php _e( "Mô tả điều hướng", THEMEDOMAIN ); ?>:</label>
                <textarea name="_breadcrumb_description" style="width: 100%;height: 100px;"><?php echo $description; ?></textarea>
            </div>
            <?php
            wp_editor( $value, '_page_top_setting_content', $settings = array() );
        }
        function theme_breadcrumb(){
            $listBreadcrumbs = array(
                'home' => array(
                    'title' => __('Trang chủ', THEMEDOMAIN ),
                    'link' => home_url(),
                    'class' => NULL,
                    'id' => NULL
                )
            );
            if(is_page() ){
                $listBreadcrumbs['page'] = array(
                    'title' => get_the_title(),
                    'link' => get_the_permalink(),
                    'class' => NULL,
                    'id' => NULL
                );
            }
            if(is_archive()){
                $title = post_type_archive_title( NULL, false );
                $listBreadcrumbs['page'] = array(
                    'title' => $title,
                    'link' => get_post_type_archive_link( get_post_type() ),
                    'class' => NULL,
                    'id' => NULL
                );
            }
            if(is_single()){
                global $post;
                $obj = get_post_type_object( $post->post_type );
                $title = $obj->labels->name;
                $link = get_post_type_archive_link( $post->post_type );
                $listBreadcrumbs['page'] = array(
                    'title' => $title,
                    'link' => $link,
                    'class' => NULL,
                    'id' => NULL
                );
                $listBreadcrumbs['single'] = array(
                    'title' => get_the_title(),
                    'link' => get_the_permalink(),
                    'class' => NULL,
                    'id' => NULL
                );
            }
            $listBreadcrumbs = apply_filters( 'sts_breadcrumb_filter', $listBreadcrumbs );
            $end = end($listBreadcrumbs);
            $count = count($listBreadcrumbs);
            $i = 1;
            ?>
            <ul class="p-breadcrumb">
                <?php foreach($listBreadcrumbs as $k=>$v): ?>
                    <?php if($i == $count ): ?>
                        <li><?=$v['title']?></li>
                    <?php else: ?>
                        <li id="<?=$v['id']?>" class="<?=$v['class']?>"><a href="<?=$v['link']?>"><?=$v['title']?></a> <span class="breadcrumb-arrow">&gt;</span></li>
                    <?php endif; ?>
                <?php $i++; endforeach; ?>
            </ul>
            <?php
        }
        function woocommerce_default_product_tabs( $tabs = array() ){
            global $product, $post;

            $tabs['hieu-suat'] = array(
                'title'    => __( 'Hiệu suất', THEMEDOMAIN ),
                'priority' => 10,
                'callback' => array( __CLASS__, 'woocommerce_hieu_suat_tab' )
            );

            $tabs['kich-thuoc'] = array(
                'title'    => __( 'Kích thước', THEMEDOMAIN ),
                'priority' => 20,
                'callback' => array( __CLASS__, 'woocommerce_kich_thuoc_tab' )
            );

            $tabs['ket-qua-kiem-tra'] = array(
                'title'    => __( 'Kết quả kiểm tra', THEMEDOMAIN ),
                'priority' => 30,
                'callback' => array( __CLASS__, 'woocommerce_ket_qua_kiem_tra_tab' )
            );

            return $tabs;
        }
        function woocommerce_hieu_suat_tab(){
            wc_get_template( 'single-product/tabs/hieu-suat.php' );
        }
        function woocommerce_kich_thuoc_tab(){
            wc_get_template( 'single-product/tabs/kich-thuoc.php' );
        }
        function woocommerce_ket_qua_kiem_tra_tab(){
            wc_get_template( 'single-product/tabs/ket-qua-kiem-tra.php' );
        }
        function test_result_settings_meta_box( $post ){
            $_acj_product = get_post_meta( $post->ID, '_acj_product', true );
            $_acj_date = get_post_meta( $post->ID, '_acj_date', true );
            $_acj_rate = get_post_meta( $post->ID, '_acj_rate', true );
            $_acj_company = get_post_meta( $post->ID, '_acj_company', true );
            $_acj_logo = get_post_meta( $post->ID, '_acj_logo', true );
            $_acj_name_car = get_post_meta( $post->ID, '_acj_name_car', true );
            $_acj_car_logo = get_post_meta( $post->ID, '_acj_car_logo', true );

            $_acj_logo_thumb_id = absint( $_acj_logo );
            if ( $_acj_logo_thumb_id ) {
                $image_logo = wp_get_attachment_thumb_url( $_acj_logo_thumb_id );
            } else {
                $image_logo = get_template_directory_uri().'/assets/images/placeholder.png';
            }

            $_acj_car_logo_thumb_id = absint( $_acj_car_logo );
            if ( $_acj_logo_thumb_id ) {
                $image_car_logo = wp_get_attachment_thumb_url( $_acj_car_logo_thumb_id );
            } else {
                $image_car_logo = get_template_directory_uri().'/assets/images/placeholder.png';
            }

            $products = new WP_Query( array( 'post_type'=>'product', 'posts_per_page' => -1 ) );
            wp_nonce_field( 'acj_theme_test_result_nonce', 'acj_theme_nonce' );
            ?>
            <table class="form-table">
                <tbody>
                    <tr>
                        <th scope="row"><label for="_acj_product"><?php _e( 'Kiểm tra cho lốp xe', THEMEDOMAIN ); ?></label></th>
                        <td>
                            <select name="_acj_product" id="_acj_product">
                                <option value=""><?php _e( "Chọn lốp xe", THEMEDOMAIN ); ?></option>
                                <?php if( $products->have_posts() ): ?>
                                    <?php while( $products->have_posts() ): $products->the_post(); ?>
                                        <option <?php selected( get_the_ID(), $_acj_product ); ?> value="<?php the_ID(); ?>"><?php the_title(); ?></option>
                                    <?php endwhile; ?>
                                <?php endif; wp_reset_postdata(); ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="_acj_date"><?php _e( 'Ngày kiểm tra', THEMEDOMAIN ); ?></label></th>
                        <td>
                            <input type="text" name="_acj_date" id="_acj_date" class="regular-text" value="<?php echo esc_attr( $_acj_date ); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="_acj_rate"><?php _e( 'Thứ hạng', THEMEDOMAIN ); ?></label></th>
                        <td>
                            <input type="text" name="_acj_rate" id="_acj_rate" class="regular-text" value="<?php echo esc_attr( $_acj_rate ); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="_acj_company"><?php _e( 'Hãng kiểm tra', THEMEDOMAIN ); ?></label></th>
                        <td>
                            <input type="text" name="_acj_company" id="_acj_company" class="regular-text" value="<?php echo esc_attr( $_acj_company ); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="_acj_logo"><?php _e( 'Logo hãng kiểm tra', THEMEDOMAIN ); ?></label></th>
                        <td>
                            <div id="_acj_logo_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( $image_logo ); ?>" width="100" height="100" /></div>
                            <div style="line-height: 60px;">
                                <input type="hidden" id="_acj_logo_thumb_id" name="_acj_logo" value="<?php echo $_acj_logo; ?>" />
                                <button type="button" class="_acj_logo_thumb_btn button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
                            </div>
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="_acj_name_car"><?php _e( 'Tên xe kiểm tra', THEMEDOMAIN ); ?></label></th>
                        <td>
                            <input type="text" name="_acj_name_car" id="_acj_name_car" class="regular-text" value="<?php echo esc_attr( $_acj_name_car ); ?>" />
                        </td>
                    </tr>
                    <tr>
                        <th scope="row"><label for="_acj_car_logo"><?php _e( 'Hình xe minh họa', THEMEDOMAIN ); ?></label></th>
                        <td>
                            <div id="_acj_car_logo_thumbnail" style="float: left; margin-right: 10px;"><img src="<?php echo esc_url( $image_car_logo ); ?>" width="100" height="100" /></div>
                            <div style="line-height: 60px;">
                                <input type="hidden" id="_acj_car_logo_thumb_id" name="_acj_car_logo" value="<?php echo $_acj_car_logo; ?>" />
                                <button type="button" class="_acj_car_logo_thumb_btn button"><?php _e( 'Upload/Add image', 'woocommerce' ); ?></button>
                            </div>
                        </td>
                    </tr>
                </tbody>
            </table>
            <script type="text/javascript">
                // Uploading files
                var file_frame;

                jQuery( document ).on( 'click', '._acj_logo_thumb_btn', function( event ) {

                    event.preventDefault();

                    // If the media frame already exists, reopen it.
                    if ( file_frame ) {
                        file_frame.open();
                        return;
                    }

                    // Create the media frame.
                    file_frame = wp.media.frames.downloadable_file = wp.media({
                        title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                        button: {
                            text: '<?php _e( "Use image", "woocommerce" ); ?>'
                        },
                        multiple: false
                    });

                    // When an image is selected, run a callback.
                    file_frame.on( 'select', function() {
                        var attachment = file_frame.state().get( 'selection' ).first().toJSON();

                        jQuery( '#_acj_logo_thumb_id' ).val( attachment.id );
                        jQuery( '#_acj_logo_thumbnail' ).find( 'img' ).attr( 'src', attachment.sizes.full.url );
                    });

                    // Finally, open the modal.
                    file_frame.open();
                });

                var file_frame2;
                jQuery( document ).on( 'click', '._acj_car_logo_thumb_btn', function( event ) {

                    event.preventDefault();

                    // If the media frame already exists, reopen it.
                    if ( file_frame2 ) {
                        file_frame2.open();
                        return;
                    }

                    // Create the media frame.
                    file_frame2 = wp.media.frames.downloadable_file = wp.media({
                        title: '<?php _e( "Choose an image", "woocommerce" ); ?>',
                        button: {
                            text: '<?php _e( "Use image", "woocommerce" ); ?>'
                        },
                        multiple: false
                    });

                    // When an image is selected, run a callback.
                    file_frame2.on( 'select', function() {
                        var attachment = file_frame2.state().get( 'selection' ).first().toJSON();

                        jQuery( '#_acj_car_logo_thumb_id' ).val( attachment.id );
                        jQuery( '#_acj_car_logo_thumbnail' ).find( 'img' ).attr( 'src', attachment.sizes.full.url );
                    });

                    // Finally, open the modal.
                    file_frame2.open();
                });
            </script>
        <?php
        }
    }

    new ACJ_Theme();
endif;
function language_selector_flags(){
    pll_the_languages(
        array(
            'show_flags' => 1,
            'dropdown' => 0,
            'hide_if_empty' => 0
        )
    );
}
/**include function file**/
include 'templates/functions.php';