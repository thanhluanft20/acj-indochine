<?php
/**
* Template Name: Home Page
 */
?>
<?php get_header(); ?>
    <div class="slider" id="slider">
        <div class="slider-content">
            <div id="carousel-example-generic" class="carousel slide" data-ride="carousel">
                <!-- Wrapper for slides -->
                <div class="carousel-inner" role="listbox">
                    <?php
                    $slider_args = array(
                        'post_type' => 'acj_slider',
                        'posts_per_page' => 3
                    );
                    $sliders = new WP_Query( $slider_args );
                    ?>
                    <?php if($sliders->have_posts()): ?>
                        <?php $i=1; while($sliders->have_posts()): $sliders->the_post(); $class = ""; if( $i == 1) $class = ' active'; ?>
                            <div class="item<?php echo $class; ?>">
                                <?php the_post_thumbnail('full') ?>
                                <div class="slider-caption">
                                    <div class="container">
                                        <h2 class="slider-title"><span class="slider-left"></span><span class="slider-text">PIRELL</span><span class="slider-right"></span></h2>
                                        <h3 class="slider-title-2"><?php the_title(); ?></h3>
                                        <h4 class="slider-title-3"><?php the_excerpt(); ?></h4>
                                        <a href="" class="btn btn-slider"><?php _e( "Chi tiết", THEMEDOMAIN ) ?></a>
                                    </div>
                                </div>
                            </div>
                        <?php $i++; endwhile; ?>
                    <?php endif; ?>

                </div>
                <!-- Indicators -->
                <div class="container" style="position: relative;">
                    <ul class="carousel-indicators">
                        <?php if($sliders->have_posts()): ?>
                            <?php $j = 1; while($sliders->have_posts()): $sliders->the_post(); $class = ""; if( $j == 1) $class = ' active'; ?>
                                <li data-target="#carousel-example-generic" data-slide-to="0" class="<?=$class;?>">
                                    <?php the_post_thumbnail('small') ?>
                                </li>
                            <?php $j++; endwhile; ?>
                        <?php endif; wp_reset_postdata(); ?>
                    </ul>
                </div>
            </div>
            <div class="slider-form">
                <div class="container">
                    <div class="col-lg-4 col-lg-offset-8 col-md-8 col-md-offset-2">
                        <div class="slider-form-content">
                            <div class="form-inner">
                                <div class="top-search-form-title">
                                    <span class="form-title-left"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/caption-tile-left.png"></span>
                                    <span class="form-title-text"><?php _e( "TÌM LỐP XE PHÙ HỢP", THEMEDOMAIN ); ?></span>
                                    <span class="form-title-right"><img src="<?php echo get_template_directory_uri(); ?>/assets/images/caption-tile-right.png"></span>
                                </div>
                                <div class="top-search-form-tabs">
                                    <div class="tab-item active" id="type"><?php _e( "Tìm theo xe", THEMEDOMAIN ); ?></div>
                                    <div class="tab-item" id="size"><?php _e( "Tìm theo size", THEMEDOMAIN ); ?></div>
                                    <div class="clear"></div>
                                </div>
                                <div class="form-search-content" id="search-for-type">
                                    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                                        <ul class="list-sub-tabs">
                                            <?php $terms = get_terms('loai_xe', array('hide_empty' => false, 'orderby' => 'term_id', 'order' => 'DESC')); ?>
                                            <?php if (!empty($terms)): ?>
                                                <?php $i = 1;
                                                    foreach ($terms as $term):
                                                        $class = '';
                                                        if ($i == 1) $class = 'active'; ?>
                                                        <?php
                                                        $thumbnail_id = absint(get_term_meta($term->term_id, 'acj_tax_thumbnail_id', true));
                                                        if ($thumbnail_id) {
                                                            $thumb = wp_get_attachment_image_src( $thumbnail_id, array( 100, 40 ) );
                                                            $image = $thumb[0];
                                                        } else {
                                                            $image = get_template_directory_uri() . '/assets/images/placeholder.png';
                                                        }

                                                        $thumbnail_gray_id = absint(get_term_meta($term->term_id, 'acj_tax_thumbnail_gray_id', true));
                                                        if ($thumbnail_gray_id) {
                                                            $thumb = wp_get_attachment_image_src( $thumbnail_gray_id, array( 100, 40 ) );
                                                            $image_gray = $thumb[0];
                                                        } else {
                                                            $image_gray = get_template_directory_uri() . '/assets/images/placeholder.png';
                                                        }
                                                    ?>
                                                        <li data-term="<?=$term->term_id;?>" class="<?=$class;?>">
                                                            <div class="img-active"><img src="<?=$image;?>" alt="<?=$term->name;?>" /></div>
                                                            <div class="img-not-active"><img src="<?=$image_gray;?>" alt="<?=$term->name;?>" /></div>
                                                            <p><?=$term->name;?></p>
                                                        </li>
                                                <?php $i++; endforeach; ?>
                                            <?php endif; ?>
                                        </ul>
                                        <input type="hidden" name="loai_xe" value="" />
                                        <input type="hidden" name="search_type" value="product" />
                                        <div class="form-input">
                                            <div class="form-group">
                                                <select class="form-control hang-xe" name="hang_xe">
                                                    <option value=""><?php _e( "Hãng xe", THEMEDOMAIN ); ?></option>
                                                    <?php $term_hang_xe = get_terms( 'hang_xe', array('hide_empty'=> false) ); ?>
                                                    <?php if( !empty( $term_hang_xe ) ): ?>
                                                        <?php foreach( $term_hang_xe as $term ): ?>
                                                            <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control mau-xe" name="mau_xe">
                                                    <option value=""><?php _e( "Mẫu xe", THEMEDOMAIN ); ?></option>
                                                    <?php $term_hang_xe = get_terms( 'mau_xe', array('hide_empty'=> false) ); ?>
                                                    <?php if( !empty( $term_hang_xe ) ): ?>
                                                        <?php foreach( $term_hang_xe as $term ): ?>
                                                            <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                                                        <?php endforeach; ?>
                                                    <?php endif; ?>
                                                </select>
                                            </div>
                                            <div class="form-group">
                                                <select class="form-control nam-san-xuat" name="nam_san_xuat">
                                                    <option value=""><?php _e( "Năm sản xuất xe", THEMEDOMAIN ); ?></option>
                                                    <?php for( $i = 1970; $i <= date('Y'); $i++ ): ?>
                                                        <option><?php echo $i; ?></option>
                                                    <?php endfor; ?>
                                                </select>
                                            </div>
                                            <input type="submit" value="<?php _e( "Tìm lốp xe", THEMEDOMAIN ); ?>" />
                                        </div>
                                    </form>
                                </div><!-- END #search-for-type -->
                                <div class="form-search-content" id="search-by-size">
                                    <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
                                        <input type="hidden" name="search_type" value="product" />
                                        <div class="select-parent">
                                            <select class="select" name="product_size">
                                                <?php $term_hang_xe = get_terms( 'kich_thuoc', array( 'hide_empty'=> false, 'parent' => 0 ) ); ?>
                                                <?php if( !empty( $term_hang_xe ) ): ?>
                                                    <?php foreach( $term_hang_xe as $term ): ?>
                                                        <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                                                        <?php
                                                        $child_args = array('parent'=> $term->term_id);
                                                        $child_terms = get_terms( 'kich_thuoc', $child_args );
                                                        ?>
                                                        <?php if( !empty( $child_terms ) ): ?>
                                                            <?php foreach( $child_terms as $child_term ): ?>
                                                                <option value="<?=$child_term->term_id;?>">--<?=$child_term->name;?></option>
                                                            <?php endforeach; ?>
                                                        <?php endif; ?>
                                                    <?php endforeach; ?>
                                                <?php endif; ?>
                                            </select>
                                            <input type="submit" value="<?php _e('TÌM LỐP XE', THEMEDOMAIN ); ?>" class="submid-vehiche"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div><!-- END .slider-form-content -->
                    </div>
                </div>
            </div>
        </div><!-- END .slider-content -->
    </div><!-- END #slider -->
    <script type="text/javascript">
        jQuery(document).ready(function($){
            $('.carousel').carousel({
                interval: 5000
            });
            $(".list-sub-tabs li").click(function(){
                var termID = $(this).data('term');
                $('input[name="loai_xe"]').val(termID);
                $('.list-sub-tabs li').removeClass('active')
                $(this).addClass('active');
                return false;
            });
            $(".top-search-form-tabs #type").click(function(){
                $('.form-search-content').removeClass('active').hide();
                $('.tab-item').removeClass('active');
                $(this).addClass('active');
                $("#search-for-type").addClass('active').show();
                return false;
            });
            $(".top-search-form-tabs #size").click(function(){
                $('.form-search-content').removeClass('active').hide();
                $('.tab-item').removeClass('active');
                $(this).addClass('active');
                $("#search-by-size").addClass('active').show();
                return false;
            });
        });
    </script>
    <div class="container">
        <div class="col-md-12 home-services">
            <div class="heading-title">
                <h3><span><?php _e( "Các sản phẩm và dịch vụ", THEMEDOMAIN ); ?></span></h3>
            </div>
            <ul class="list-home-products">
                <?php
                $argc =array(
                    'post_type'         => 'tin-dich-vu',
                    'orderby'           => 'date',
                    'post_per_page'     =>12
                );
                $the_query = new WP_Query($argc);
                if ( $the_query->have_posts() ) {
                    while ( $the_query->have_posts() ) {
                        $the_query->the_post();
                        ?>
                        <li>
                            <a href="<?php the_permalink(); ?>" class="home-product-img"><img class="icon" src="<?php echo get_post_meta( get_the_ID(), '_acj_icon_home', true ); ?>" alt=""/></a>
                            <a href="<?php the_permalink(); ?>" class="home-product-title"><?php the_title();?></a>
                        </li>

                    <?php
                    }
                }
                /* Restore original Post Data */
                wp_reset_postdata();

                ?>
                <div class="clear"></div>
            </ul>
        </div>
        <?php
        while(have_posts()): the_post();
            the_content();
        endwhile;
        ?>


        <div class="main col-md-9" id="main">

            <div class="heading-title">
                <h3><span><?php _e( "Sản phẩm mới", THEMEDOMAIN ); ?></span></h3>
            </div>
            <div class="row product-items">
                <?php
                global $product;
                $args = array(
                    'post_type' => 'product',
                    'posts_per_page' => 12
                );
                $loop = new WP_Query( $args );
                if ( $loop->have_posts() ) {
                    while ( $loop->have_posts() ) : $loop->the_post();?>
                        <div class="col-md-4 col-sm-6 product-item text-center">
                            <div class="product-img">
                                <a href="<?php  the_permalink(); ?>"><?php the_post_thumbnail('product-normal');?></a>
                            </div>
                            <div class="product-description">
                                <h4><?php the_title();?></h4>
                                <p><?php the_excerpt();?></p>
                            </div>
                        </div><!-- END .product-item -->
                    <?php endwhile;
                } else {
                    echo __( 'No products found' );
                }
                wp_reset_postdata();
                ?>

            </div><!-- END .row.product-items -->
        </div><!-- END #main -->
        <div class="sidebar col-md-3" id="sidebar">
            <div class="widget">
                <div class="heading-title">
                    <h3><span><?php _e( "Bán chạy nhất", THEMEDOMAIN ); ?></span></h3>
                </div>
                <div class="widget-products">
                        <?php
                        global $product;
                        $args = array(
                            'post_type'         => 'product',
                            'posts_per_page'    => 4,
                            'meta_key'          => 'total_sales',
                            'orderby'           => 'meta_value_num'
                        );
                        $loop = new WP_Query( $args );

                        if ( $loop->have_posts() ) {?>
                            <ul class="ul-widget-products">
                        <?php
                            while ( $loop->have_posts() ) : $loop->the_post();
                                $price = get_post_meta( get_the_ID(), '_regular_price', true);
                                $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                                $currency = get_post_meta( get_the_ID(), 'woocommerce_currency', true);
                                global $product;
                                ?>

                                <li>
                                    <div class="pr-img">
                                        <a href="<?php  the_permalink(); ?>"><?php the_post_thumbnail('product-small');?></a>
                                    </div>
                                    <div class="pr-des">
                                        <p>
                                            <?php the_excerpt();?>
                                        </p>
                                        <p>
                                            <a href="<?php  the_permalink(); ?>" class="widget-price-button"><?php echo $product->get_price_html(); ?></a>
                                        </p>
                                    </div>
                                </li>
                            <?php endwhile;?>
                                </ul>
                        <?php }
                        wp_reset_postdata();
                        ?>
                </div><!-- END .widget-products -->
            </div>
        </div><!-- END sidebar -->

    </div><!-- END .container -->

<?php get_footer(); ?>