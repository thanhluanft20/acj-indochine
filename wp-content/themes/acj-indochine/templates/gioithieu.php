<?php
/**
 *Template Name: Gioi Thieu
 */
?>
<?php get_header(); ?>
    <div class="container-flue">
        <div class="top-banner">
            <?php
            while(have_posts()):the_post();
                echo get_post_meta( get_the_ID(), '_page_top_setting_content', true );
            endwhile;
            ?>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column">
                        <?php
                            $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                            $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                            $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                            if(empty($page_title)){ $page_title = get_the_title(); }
                            if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                        ?>
                        <h1 class="title"><?php echo $page_title; ?></h1>
                        <p class="description"><?php echo $description; ?></p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column text-right">
                        <?php do_action( 'theme_breadcrumb' ); ?>
                    </div>
                </div>
            </div>
        </div><!--END breadcrumbs-->
    </div>
<div class="container">
    <?php
    // Start the loop.
    while ( have_posts() ) : the_post();
        the_content();
        // End the loop.
    endwhile;
    ?>

 </div>
</div>
<?php get_footer(); ?>