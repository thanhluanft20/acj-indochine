<?php
/**
 *Template Name: Tin tuc chi tiet
 */
?>
<?php get_header(); ?>
<div class="container-fluid">
    <div class="top-banner">
        <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-top-tintuc.png" alt=""/>
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xm-6 column">
                    <?php
                    $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                    $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                    $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                    if(empty($page_title)){ $page_title = get_the_title(); }
                    if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                    ?>
                    <h1 class="title"><?php echo $page_title; ?></h1>
                    <p class="description"><?php echo $description; ?></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xm-6 column text-right">
                    <?php do_action( 'theme_breadcrumb' ); ?>
                </div>
            </div>
        </div>
    </div><!--END breadcrumbs-->
</div>
<div class="container tintuc">
    <div id="main" class="main col-md-9">
        <div class="top-banner-tintuc">
            <div class="date">
                <p>03/11/2015</p>
            </div>
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-tintuc.png" alt=""/>
            <h3>CHỈNH THƯỚC LÁI VỚI CÔNG NGHỆ HIỆN ĐẠI NHẤT</h3>
        </div>
        <div class="author">
            <p class="author">Liem Nguyen</p>
            <p class="device">Mobile, Android</p>
        </div>
        <div class="content">
            <p>
                <p>Chúng ta cùng tìm hiểu tại sao cần phải chỉnh thước lái nhé:</p>

                <p>- Hiện tượng đầu tiên mà bạn thường hay gặp phải đó chính là khi đi trên đường thẳng thì thấy vô lăng không cân theo hướng thẳng và người điều khiển phải thường xuyên ghì tay lái, giữ cho vô lăng thẳng bằng hai tay.</p>

                <p>- Hiện tượng thứ hai là thấy lốp xe bị ăn mòn không đều.</p>

                <p>- Hiện tượng thứ ba là xe mất khả năng bám đường, dễ bị trượt.</p>

                <p>-  …</p>

                <p>Vậy, nếu các bạn gặp các trường hợp trên thì đồng nghĩa với việc chiếc xe của bạn cần phải được cân chỉnh thước lái lại để đảm bảo cho việc di chuyển trên đường.</p>

                <p>Với mong muốn đem đến độ chính xác tuyệt đối trong việc cân chỉnh thước lái và bảo đảm an toàn cho quý khách, chúng tôi đã mạnh dạn đầu tư hệ thống chỉnh thước lái tiên tiến nhất hiện nay của hãng JohnBean, Mỹ với số model là V3400 trị giá hơn 1 tỷ đồng. Đặc điểm nổi bật của hệ thống cân chỉnh thước lái này là công nghệ quét hình 3D, Wireless đảm bảo cho kết quả chính xác nhất.</p>

                <p>Giới thiệu sơ lược về JohnBean:</p>

                <p>- Nằm trong Top 10 công ty cung cấp thiết bị tốt nhất tại hội chợ triển lãm Auto Tech ở Jacksonville, Florida vào tháng 5 năm 2015</p>

                <p>- Tiêu chí mà JohnBean muốn truyền tải cho khách hàng về máy chỉnh thước lái V3400 đó là: Nhanh chóng và Chính xác tuyệt đối.</p>
            </p>
        </div>
        <div class="tintuc-share">

            <ul>
                <li><p>Chia Sẻ</p></li>
                <li><a class="face" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-face.png" alt=""/></a></li>
                <li><a class="twitter" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-twitter.png" alt=""/></a></li>
                <li><a class="camera" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-camera.png" alt=""/></a></li>
                <li><a class="google" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-google.png" alt=""/></a></li>
            </ul>
        </div>
        <div class="news-recent">
            <div class="heading-title">
                <h3><span>TIN TỨC GẦN ĐÂY</span></h3>
            </div>
            <div class="row">
                <div class="col-lg-4 col-md-4 col-sm-12">
                    <img class="img-responsive text-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-gioithieu-1.png" alt=""/>
                    <div class="">
                        <p class="out-title"><a class="title" href="">TẠI SAO PHẢI CHỈNH THƯỚC LÁI (ĐỘ CHỤM BÁNH XE)?</a></p>
                        <p>“Mất lái” - cụm từ phản ánh hiện tượng người điều khiển phương tiện không thể kiểm soát được chiếc xe. Trong mọi tai nạn giao thông, ....</p>
                        <a class="read-more" href=""> Đọc thêm<span class="glyphicon glyphicon-menu-right"></span></a>
                    </div>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <img class="img-responsive text-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-gioithieu-2.png" alt=""/>
                    <p class="out-title"><a  class="title" href="">NGUYÊN NHÂN KHIẾN ĐỘNG CƠ ÔTÔ NHANH XUỐNG CẤP
                    </a></p>
                    <p>Nguyên nhân là do khi động cơ xe không hoạt động, dầu nhớt sẽ chảy xuống các-te thay vì lưu lại ở các phần động cơ quan trọng như piston và xi-lanh... </p>
                    <a class="read-more" href=""> Đọc thêm<span class="glyphicon glyphicon-menu-right"></span></a>
                </div>

                <div class="col-lg-4 col-md-4 col-sm-12">
                    <img class="img-responsive text-center" src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-gioithieu-3.png" alt=""/>
                    <p class="out-title"><a class="title" href="">8 MẸO TIẾT KIỆM XĂNG TÀI XẾ CẦN NHỚ
                    </a></p>
                    <p>Tiết kiệm xăng phụ thuộc phần lớn vào cách lái xe, thói quen đạp ga, đạp phanh, đánh lái của tài xế, nhưng bên cạnh đó còn có những mẹo nhỏ nhưng ảnh hưởng ...</p>
                    <a class="read-more" href=""> Đọc thêm <span class="glyphicon glyphicon-menu-right"></span></a>
                </div>
            </div>
            <div></div>
        </div><!--END news-recent-->
    </div><!--END main-->

    <div id="sidebar" class="sidebar col-md-3">
        <div class="widget">
            <div class="search">
                <input type="text" name="search" id="search" placeholder="Nhập từ khoá tìm kiếm..."/>
                <button type="submit"><span class="glyphicon glyphicon-search"></span></button>
            </div>
        </div>
        <div class="widget tintuc">
            <div class="heading-title">
                <h3><span>Tin tức đọc nhiều nhất</span></h3>
            </div>
            <div class="ul-widget">
                <ul class="ul-widget-info">
                    <li>
                        <div class="pr-img">
                            <a title="" data-original-title="" href=""><img src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-image-1.png" alt="Product 1"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a title="" data-original-title="" href="">Cách xử lý khi xe trượt bánh trước</a>
                            </p>
                            <p>
                                Trượt bánh trước khi vào cua được gọi là hiện...
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-menu-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a title="" data-original-title="" href=""><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-image-4.png" alt="Product 1"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a title="" data-original-title="" href="">Cách xử lý khi xe trượt bánh trước</a>
                            </p>
                            <p>
                                Trượt bánh trước khi vào cua được gọi là hiện...
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-menu-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a title="" data-original-title="" href=""><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-image-2.png" alt="Product 1"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a title="" data-original-title="" href="">Cách xử lý khi xe trượt bánh trước</a>
                            </p>
                            <p>
                                Trượt bánh trước khi vào cua được gọi là hiện...
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-menu-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a title="" data-original-title="" href=""><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-image-3.png" alt="Product 1"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a title="" data-original-title="" href="">Cách xử lý khi xe trượt bánh trước</a>
                            </p>
                            <p>
                                Trượt bánh trước khi vào cua được gọi là hiện...
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-menu-right"></span></a>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a title="" data-original-title="" href=""><img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/tt-image-4.png" alt="Product 1"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a title="" data-original-title="" href="">Cách xử lý khi xe trượt bánh trước</a>
                            </p>
                            <p>
                                Trượt bánh trước khi vào cua được gọi là hiện...
                            </p>
                            <a class="read-more" href="">Đọc thêm<span class="glyphicon glyphicon-menu-right"></span></a>
                        </div>
                    </li>

                </ul>
            </div>

        </div>
        <div class="widget">
            <div class="heading-title">
                <h3><span>Bán chạy nhất</span></h3>
            </div>
            <div class="widget-products">
                <ul class="ul-widget-products">
                    <li>
                        <div class="pr-img">
                            <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                            </p>
                        </div>
                    </li>
                    <li>
                        <div class="pr-img">
                            <a href="" data-original-title="" title=""><img class="img-responsive" alt="Product 1" src="http://localhost/acj-indochine/wp-content/themes/acj-indochine/assets/img/product-im-1.png"></a>
                        </div>
                        <div class="pr-des">
                            <p>
                                <a href="" data-original-title="" title="">The New Fr:01 Ii And Tr:01 Ii Tyres Are At Home In Even The Most Varied Of Usage Conditions.</a>
                            </p>
                            <p>
                                <a class="widget-price-button" href="" data-original-title="" title="">$234.79</a>
                            </p>
                        </div>
                    </li>

                </ul>
            </div><!-- END .widget-products -->
        </div>
    </div><!-- END .sidebar -->
</div><!-- END .container.tintuc -->
<?php get_footer(); ?>