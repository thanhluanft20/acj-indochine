<?php
/**
 *Template Name: Dich Vu
 */
?>
<?php get_header(); ?>
    <div class="container-flue">
        <div class="top-banner">
           <?php
            while(have_posts()):the_post();
                echo get_post_meta( get_the_ID(), '_page_top_setting_content', true );
            endwhile;
            ?>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column">
                        <?php
                            $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                            $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                            $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                            if(empty($page_title)){ $page_title = get_the_title(); }
                            if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                        ?>
                        <h1 class="title"><?php echo $page_title; ?></h1>
                        <p class="description"><?php echo $description; ?></p>
                    </div>
                    <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column text-right">
                        <?php do_action( 'theme_breadcrumb' ); ?>
                    </div>
                </div>
            </div>
        </div><!--END breadcrumbs-->
    </div>
    <div class="container dichvu" >
        <?php
            $argc =array(
                'post_type'         => 'tin-dich-vu',
                'orderby'           => 'date',
                'post_per_page'     =>4
            );
        $the_query = new WP_Query($argc);
        if ( $the_query->have_posts() ) { $i=0?>
            <div class="row">
                <?php

                while ( $the_query->have_posts() ) {
                    $the_query->the_post();
                    ?>
                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-3">
                        <div class="thumbnail">
                            <a href="<?php the_permalink(); ?>"><?php the_post_thumbnail();?></a>
                            <div class="caption">
                                <a href="<?php the_permalink(); ?>"><img class="icon" src="<?php echo get_post_meta( get_the_ID(), '_acj_icon', true ); ?>" alt=""/></a>
                                <a href="<?php the_permalink(); ?>"><h3><?php the_title();?></h3></a>
                                <p><?php echo wp_trim_words(get_the_excerpt(), 10); ?></p>
                                <a class="read-more" href="<?php the_permalink(); ?>"><?php _e('Đọc thêm',THEMEDOMAIN);?></a>
                            </div>
                        </div>
                    </div>
                <?php
                    $i++;
                    if( $i % 4 == 0 ){

                        ?>
                        </div>
                        <div class="row">
                    <?php
                    }
            }?>
            </div>
            <?php

        }
        /* Restore original Post Data */
        wp_reset_postdata();

        ?>
        </div><!--End row-->
    </div><!--End Container-->
    <div class="container-flued submit">
        <div class="container">
            <div class="row">
                <div class="text">
                    <?php if ( is_active_sidebar( 'van-ban-email-khach-hang' ) ) : ?>
                            <?php dynamic_sidebar( 'van-ban-email-khach-hang' ); ?>
                    <?php endif; ?>
                <div class="col-lg-6 col-lg-offset-3">
               <?php if ( is_active_sidebar( 'email-khach-hang' ) ) : ?>
                    <ul id="sidebar">
                        <?php dynamic_sidebar( 'email-khach-hang' ); ?>
                    </ul>
                <?php endif; ?>
                </div>
            </div>
        </div>
    </div>


<?php get_footer(); ?>