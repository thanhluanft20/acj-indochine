<?php
/**
* Template name: Dich vu chi tiet
 */
?>
<?php get_header(); ?>
    <div class="container-flue">
        <div class="top-banner">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-dichvu.png" alt=""/>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xm-6 column">
                        <?php
                        $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                        $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                        $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                        if(empty($page_title)){ $page_title = get_the_title(); }
                        if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                        ?>
                        <h1 class="title"><?php echo $page_title; ?></h1>
                        <p class="description"><?php echo $description; ?></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xm-6 column text-right">
                        <?php do_action( 'theme_breadcrumb' ); ?>
                    </div>
                </div>
            </div>
        </div><!--END breadcrumbs-->
    </div><!--END container-flue-->

    <div class="container dichvuchitiet">
        <div class="row">
            <div class="col-lg-3">
                <div class="all-service">
                    <h3 class="title"> Tất cả dịch vụ</h3>
                    <ul class="nav nav-tabs nav-stacked">
                        <li class="active"><a class="lopxe" data-toggle="tab" href="#lopxe">Lốp Xe</a></li>
                        <li><a class="phuongchamam" data-toggle="tab" href="#canbangdong">Cân bằng động</a></li>
                        <li><a class="sumenh" data-toggle="tab" href="#lazang">LaZăng</a></li>
                        <li><a class="nentang" data-toggle="tab" href="#chinhdocum">chỉnh độ cụm</a></li>
                        <li><a class="nentang" data-toggle="tab" href="#ongxa">ống xả</a></li>
                        <li><a class="nentang" data-toggle="tab" href="#acquy">ắc quy</a></li>
                        <li><a class="nentang" data-toggle="tab" href="#maphanh">má phanh</a></li>
                        <li><a class="nentang" data-toggle="tab" href="#thaydau">thay dầu</a></li>
                        <li><a class="nentang" data-toggle="tab" href="#gatmua">gạt mưa</a></li>
                        <li><a class="nentang" data-toggle="tab" href="#carcare">car care</a></li>
                    </ul>
                </div>
                <div class="address">
                    <p class="beforeh2"></p>
                    <h3>Văn Phòng</h3>
                    <h4>Địa chỉ:</h4>
                    <p>302 An Dương Vương, Phường 4
                        Quận 5, Tp. HCM, Vietnam</p>
                    <h4>Địên thoại:</h4>
                    <p>0908157639 - 0908157639</p>
                    <h4>Email:</h4>
                    <p>acj.indochine@gmail.com</p>
                </div>
            </div>
            <div class="col-lg-9">
                <div class="tab-content">
                    <div id="lopxe" class="tab-pane fade in active">
                        <div class="top-dvct">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-dvct.png" alt=""/>
                            <p class="description">Công ty vỏ xe ACJ INDOCHINE chúng tôi cung cấp đầy đủ các dịch vụ chăm sóc , bảo dưỡng ô tô cũng như thay thế các loại lốp xe ô tô thông dụng :</p>
                        </div>
                        <div class="row detail">
                            <div class="col-lg-4">
                                <a href=""> <h3>Tháo lắp & thay lốp xe</h3></a>
                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/dvct-img-1.png" alt=""/>
                                <div>
                                    <h4>Lợi ích:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Nâng cao tính an toàn và giảm chi phí vận hành</p>
                                    <p><span class="glyphicon glyphicon-ok"></span>Khi đi lốp xe củ hoăc mòn quá mức quy định nhà xản xuất lốp, lớp dể bị nổ hoặc xì do vật nhon đâm vào không an toàn và để gay tai nạn</p>
                                    <h4>Ưu điểm:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Tránh các tai nạn xảy ra bất thường khi lưu thông  trên quảng đường dài.</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <a href=""> <h3>Kiểm tra áp suất lốp xe</h3></a>
                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/dvct-img-2.png" alt=""/>
                                <div>
                                    <h4>Lợi ích:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Nâng cao tính an toàn và giảm chi phí vận hành</p>
                                    <p><span class="glyphicon glyphicon-ok"></span>Khi đi lốp xe củ hoăc mòn quá mức quy định nhà xản xuất lốp, lớp dể bị nổ hoặc xì do vật nhon đâm vào không an toàn và để gay tai nạn</p>
                                    <h4>Ưu điểm:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Tránh các tai nạn xảy ra bất thường khi lưu thông  trên quảng đường dài.</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <a href=""> <h3>Đảo vị trí lốp oto</h3></a>
                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/dvct-img-3.png" alt=""/>
                                <div>
                                    <h4>Lợi ích:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Nâng cao tính an toàn và giảm chi phí vận hành</p>
                                    <p><span class="glyphicon glyphicon-ok"></span>Khi đi lốp xe củ hoăc mòn quá mức quy định nhà xản xuất lốp, lớp dể bị nổ hoặc xì do vật nhon đâm vào không an toàn và để gay tai nạn</p>
                                    <h4>Ưu điểm:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Tránh các tai nạn xảy ra bất thường khi lưu thông  trên quảng đường dài.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="canbangdong" class="tab-pane fade in">
                        <div class="top-dvct">
                            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-dvct.png" alt=""/>
                            <p class="description">Công ty vỏ xe ACJ INDOCHINE chúng tôi cung cấp đầy đủ các dịch vụ chăm sóc , bảo dưỡng ô tô cũng như thay thế các loại lốp xe ô tô thông dụng :</p>
                        </div>
                        <div class="row detail">
                            <div class="col-lg-4">
                                <a href=""> <h3>Tháo lắp & thay lốp xe</h3></a>
                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/dvct-img-1.png" alt=""/>
                                <div>
                                    <h4>Lợi ích:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Nâng cao tính an toàn và giảm chi phí vận hành</p>
                                    <p><span class="glyphicon glyphicon-ok"></span>Khi đi lốp xe củ hoăc mòn quá mức quy định nhà xản xuất lốp, lớp dể bị nổ hoặc xì do vật nhon đâm vào không an toàn và để gay tai nạn</p>
                                    <h4>Ưu điểm:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Tránh các tai nạn xảy ra bất thường khi lưu thông  trên quảng đường dài.</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <a href=""> <h3>Kiểm tra áp suất lốp xe</h3></a>
                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/dvct-img-2.png" alt=""/>
                                <div>
                                    <h4>Lợi ích:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Nâng cao tính an toàn và giảm chi phí vận hành</p>
                                    <p><span class="glyphicon glyphicon-ok"></span>Khi đi lốp xe củ hoăc mòn quá mức quy định nhà xản xuất lốp, lớp dể bị nổ hoặc xì do vật nhon đâm vào không an toàn và để gay tai nạn</p>
                                    <h4>Ưu điểm:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Tránh các tai nạn xảy ra bất thường khi lưu thông  trên quảng đường dài.</p>
                                </div>
                            </div>
                            <div class="col-lg-4">
                                <a href=""> <h3>Đảo vị trí lốp oto</h3></a>
                                <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/dvct-img-3.png" alt=""/>
                                <div>
                                    <h4>Lợi ích:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Nâng cao tính an toàn và giảm chi phí vận hành</p>
                                    <p><span class="glyphicon glyphicon-ok"></span>Khi đi lốp xe củ hoăc mòn quá mức quy định nhà xản xuất lốp, lớp dể bị nổ hoặc xì do vật nhon đâm vào không an toàn và để gay tai nạn</p>
                                    <h4>Ưu điểm:</h4>
                                    <p><span class="glyphicon glyphicon-ok"></span>Tránh các tai nạn xảy ra bất thường khi lưu thông  trên quảng đường dài.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="lazang" class="tab-pane fade in">

                    </div>
                    <div id="chinhdocum" class="tab-pane fade in"></div>
                    <div id="ongxa" class="tab-pane fade in"></div>
                    <div id="acquy" class="tab-pane fade in"></div>
                    <div id="maphanh" class="tab-pane fade in"></div>
                    <div id="thaydau" class="tab-pane fade in"></div>
                    <div id="gatmua" class="tab-pane fade in"></div>
                    <div id="carcare" class="tab-pane fade in"></div>

                </div><!-- END tab-content-->
            </div><!--- END col-lg-9-->
        </div><!-- END row-->
    </div><!--END container-->
<?php get_footer(); ?>