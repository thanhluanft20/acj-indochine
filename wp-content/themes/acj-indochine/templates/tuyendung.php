<?php
/**
 *Template Name: Tuyen Dung
 */
?>
<?php get_header(); ?>

<div class="container-flue">
    <div class="top-banner">
        <?php
        while(have_posts()):the_post();
            echo get_post_meta( get_the_ID(), '_page_top_setting_content', true );
        endwhile;
        ?>
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column">
                    <?php
                    $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                    $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                    $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                    if(empty($page_title)){ $page_title = get_the_title(); }
                    if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                    ?>
                    <h1 class="title"><?php echo $page_title; ?></h1>
                    <p class="description"><?php echo $description; ?></p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column text-right">
                    <?php do_action( 'theme_breadcrumb' ); ?>
                </div>
            </div>
        </div>
    </div><!--END breadcrumbs-->
</div><!--END container-flue-->
<div class="container tuyendung">
    <div class="row heading">
        <div class="col-md-3 col-md-offset-1 col-sm-3 col-sm-offset-1 col-xs-3 col-xs-offset-1 "><h4><?php _e( 'Ngày tạo', THEMEDOMAIN ); ?></h4></div>
        <div class="col-md-4 col-sm-4 col-xs-4"><h4 class="text-center"><?php _e( 'Vị trí', THEMEDOMAIN ); ?></h4></div>
        <div class="col-md-2 col-sm-2 col-xs-2"><h4 class="text-center"><?php _e( 'Số lượng', THEMEDOMAIN ); ?></h4></div>
        <div class="col-md-2 col-sm-2 col-xs-2"><h4 class="text-center"><?php _e( 'Trạng thái', THEMEDOMAIN ); ?></h4></div>
    </div>
</div>
<div class="danh-sach-tuyen-dung">
    <?php
        $tuyendung_args = array(
            'post_type'         => 'tin-tuyen-dung',
            'posts_per_page'    => -1
        );
        $tuyendung = new WP_Query( $tuyendung_args );
    ?>
    <?php if( $tuyendung->have_posts() ): ?>
        <?php while( $tuyendung->have_posts() ): $tuyendung->the_post(); ?>
            <?php
                $position = get_post_meta( get_the_ID(), '_acj_position', true );
                $employee_number = get_post_meta( get_the_ID(), '_acj_employee_number', true );
                $acj_status = get_post_meta( get_the_ID(), '_acj_status', true );
            ?>
            <div class="tuyen-dung-tab">
                <div class="container">
                    <div class="row">
                        <div class="col-md-1 col-sm-1 col-xs-1">
                            <span class="icon"></span>
                        </div>
                        <div class="col-md-3 col-sm-3 col-xs-3">
                            <span class="tuyen-dung-ngay"><?php the_date(); ?></span>
                        </div>
                        <div class="col-md-4 col-sm-4 col-xs-4 text-center">
                            <span class="tuyen-dung-vi-tri"><?php echo $position; ?></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                            <span class="tuyen-dung-so-luong"><?php echo $employee_number; ?></span>
                        </div>
                        <div class="col-md-2 col-sm-2 col-xs-2 text-center">
                            <span class="tuyen-dung-trang-thai"><?php echo $acj_status; ?></span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tuyen-dung-content">
                <div class="container">
                    <div class="row">
                        <div class="col-md-11 col-md-offset-1">
                            <?php the_content(); ?>
                        </div>
                    </div>
                </div>
            </div>
        <?php endwhile; ?>
    <?php endif; wp_reset_postdata(); ?>
</div><!-- END .danh-sach-tuyen-dung-->
    <script>
        jQuery(document).ready(function($){
            $(".tuyen-dung-tab").click(function(){
                if( $(this).hasClass('active') ){
                    $(this).removeClass('active');
                    $(this).next('.tuyen-dung-content').removeClass('active').hide(500);
                }else {
                    $(".tuyen-dung-tab").removeClass('active');
                    $(".tuyen-dung-content").removeClass('active').hide(500);
                    $(this).addClass('active');
                    $(this).next('.tuyen-dung-content').show(500).addClass('active');
                }
            });
        });
    </script>
<?php get_footer(); ?>