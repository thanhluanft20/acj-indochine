<?php
/**
 *Template Name: San pham chi tiet
 */
?>
<?php get_header(); ?>
    <div class="container-fluid">
        <div class="top-banner">
            <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-sanpham.png" alt=""/>
        </div>
        <div class="breadcrumbs">
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 col-md-8 col-sm-6 col-xm-6 column">
                        <?php
                        $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                        $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                        $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                        if(empty($page_title)){ $page_title = get_the_title(); }
                        if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                        ?>
                        <h1 class="title"><?php echo $page_title; ?></h1>
                        <p class="description"><?php echo $description; ?></p>
                    </div>
                    <div class="col-lg-4 col-md-4 col-sm-6 col-xm-6 column text-right">
                        <?php do_action( 'theme_breadcrumb' ); ?>
                    </div>
                </div>
            </div>
        </div><!--END breadcrumbs-->
    </div><!--END container-flue-->
    <div class="container sanpham">
        <div class="row">
            <div class="col-lg-3">
                <div class="search-vehicle">
                    <h3>Tìm lốp xe phù hợp</h3>
                    <div class="follow-kind">
                        <h4>tìm theo xe</h4>
                        <ul class="nav nav-tabs">
                            <li class="col-lg-4 " class="active">
                                <a class="text-center" data-toggle="tab" href="#home">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-oto.png" alt=""/>
                                    <span>Home</span></a>
                            </li>

                            <li class="col-lg-4">
                                <a class="text-center" data-toggle="tab" href="#menu1">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-xetai.png" alt=""/>
                                    <span>Home</span></a>
                            </li>
                            <li class="col-lg-4">
                                <a class="text-center" data-toggle="tab" href="#menu2">
                                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-xemay.png" alt=""/>
                                    <span>Home</span></a>
                            </li>
                        </ul>
                        <div class="tab-content">
                            <div id="home" class="tab-pane fade in active">
                                <form action="">
                                    <input type="text" name="hangxe" placeholder="Hảng xe"/>
                                    <input type="text" name="mauxe" placeholder="Mẩu xe"/>
                                    <input type="text" name="namsanxuat" placeholder="Năm sản xuất"/>
                                    <input type="submit" name="follow_kind" value="TÌM LỐP XE" class="submid-vehiche"/>
                                </form>
                            </div>
                            <div id="menu1" class="tab-pane fade">
                                <form action="">
                                    <input type="text" name="hangxe" placeholder="Hảng xe"/>
                                    <input type="text" name="mauxe" placeholder="Mẩu xe"/>
                                    <input type="text" name="namsanxuat" placeholder="Năm sản xuất"/>
                                    <input type="submit" name="follow_kind" value="TÌM LỐP XE" class="submid-vehiche"/>
                                </form>
                            </div>
                            <div id="menu2" class="tab-pane fade">
                                <form action="">
                                    <input type="text" name="hangxe" placeholder="Hảng xe"/>
                                    <input type="text" name="mauxe" placeholder="Mẩu xe"/>
                                    <input type="text" name="namsanxuat" placeholder="Năm sản xuất"/>
                                    <input type="submit" name="follow_kind" value="TÌM LỐP XE" class="submid-vehiche"/>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="follow-size">
                        <h4>tìm theo size</h4>
                        <form action="">
                            <div class="select-parent">
                                <select class="select">
                                    <option value="">i.e. 195/65R15</option>
                                    <option value="">i.e. 195/65R16</option>
                                    <option value="">i.e. 195/65R17</option>
                                    <option value="">i.e. 195/65R18</option>
                                </select>
                            </div>
                            <input type="submit" name="follow_size" value="TÌM LỐP XE" class="submid-vehiche"/>
                        </form>
                    </div>
                </div>
                <div class="banner">
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-sp1.png" alt=""/>
                    <img src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-sp2.png" alt=""/>
                </div>
            </div><!--END- col-lag-3-->

            <div class="col-lg-9">
                <div class="row info">
                    <div class="col-lg-4">
                        <p class="text-center">
                         <img  src="<?php echo get_template_directory_uri(); ?>/assets/images/sp-img.png" alt=""/>
                        </p>
                    </div>
                    <div class="col-lg-8">
                        <img src="<?php echo get_template_directory_uri(); ?>/assets/images/sp-img-title.png" alt=""/>
                        <div>
                            <p class="title"><a  href="">THE WINTER BREAKER</a></p>
                            <p>Excellent traction and braking on snowy surfaces. High stability in lateral grip. With the generation of winter compounds used, it achieves excellent wet and snow performance.</p>
                            <p>Moreover the innovative base compound and bead geometry improve handling and stability of the vehicle also in dry road conditions.</p>
                        </div>
                        <ul class="row loai">
                            <li class="col-lg-4 loaixe"><p><span>Loại xe: </span>SVU </p></li>
                            <li class="col-lg-4 loailop"><p><span>Loại lốp: </span>Winter </p></li>
                            <li class="col-lg-4 extreme"><p><span>Extreme: </span>Extreme </p></li>
                        </ul>
                        <div class="row">
                           <p class="dathang col-lg-4"> <a  href=""><span>Đặt hàng</span></a></p>
                           <p class="hotronhanh col-lg-4"><a  href=""> <span>Hổ trợ nhanh</span></a></p>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12">
                    <div>
                        <div class="heading-title">
                            <ul class="nav nav-tabs ">
                                <li class="active"><a href="#out-sanphamchitiet" data-toggle="tab" data-original-title="" title="">Hiệu suất</a></li>
                                <li><a href="#khuyenmai" data-toggle="tab" data-original-title="" title="">kích thước</a></li>
                                <li><a href="#tintuccongty" data-toggle="tab" data-original-title="" title="">kết quả kiểm tra</a></li>
                            </ul>
                        </div>


                        <div class="tab-content">
                            <div class="tab-pane fade in active" id="out-sanphamchitiet">
                                <div class="row">
                                    <div class="col-lg-3">
                                        <p class="name-attr"><img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hs-1.png" alt=""/><span>DRY</span></p>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="hieusuats">
                                            <ul>
                                                <li class="hieusuat hieusuat-col-1"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-2"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-3"><div class="col-inner "></div></li>
                                                <li class="hieusuat hieusuat-col-4"><div class="col-inner w50"></div></li>
                                                <li class="hieusuat hieusuat-col-5"></li>
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <p class="name-attr"><img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hs-2.png" alt=""/><span>DRY</span></p>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="hieusuats">
                                            <ul>
                                                <li class="hieusuat hieusuat-col-1"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-2"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-3"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-4"><div class="col-inner w50"></div></li>
                                                <li class="hieusuat hieusuat-col-5"></li>
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <p class="name-attr"><img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hs-3.png" alt=""/><span>DRY</span></p>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="hieusuats">
                                            <ul>
                                                <li class="hieusuat hieusuat-col-1"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-2"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-3"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-4"><div class="col-inner w50"></div></li>
                                                <li class="hieusuat hieusuat-col-5"></li>
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <p class="name-attr"><img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hs-4.png" alt=""/><span>DRY</span></p>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="hieusuats">
                                            <ul>
                                                <li class="hieusuat hieusuat-col-1"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-2"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-3"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-4"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-5"><div class="col-inner w50"></div></li>
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <p class="name-attr"><img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hs-4.png" alt=""/><span>DRY</span></p>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="hieusuats">
                                            <ul>
                                                <li class="hieusuat hieusuat-col-1"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-2"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-3"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-4"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-5"></li>
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-3">
                                        <p class="name-attr"><img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/icon-hs-6.png" alt=""/><span>DRY</span></p>
                                    </div>
                                    <div class="col-lg-9">
                                        <div class="hieusuats">
                                            <ul>
                                                <li class="hieusuat hieusuat-col-1"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-2"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-3"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-4"><div class="col-inner"></div></li>
                                                <li class="hieusuat hieusuat-col-5"></li>
                                                <div class="clear"></div>
                                            </ul>
                                        </div>
                                    </div>
                                </div>

                            </div>
                            <div class="tab-pane fade" id="khuyenmai">
                                <div class="row">


                                </div>
                            </div><!--End --Khuyenmai-->

                            <div class="tab-pane fade" id="tintuccongty">
                                <div class="row">

                                </div>
                            </div>
                        </div>
                    </div>
                    </div>
                </div>
                <div class="sanphamtuongtu">
                    <div class="heading-title">
                        <h3><span>Sản phảm tương tự</span></h3>
                    </div>

                    <div class="row sanphams">
                        <ul>
                            <li class="col-lg-4">
                                <div class="text-center">

                                    <div> <a href="">
                                            <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/wheel-sp1.png" alt=""/>
                                        </a>
                                    </div>
                                    <div class="detail">
                                        <div> <a class="title" href="">ICE ZERO</a></div>
                                        <p class="description-prod">Kích thước: 205/45R17 / TT - TĐ: 88W
                                            Made in ROMANI</p>
                                        <p class="price"><span>$234.79</span></p>
                                    </div>
                                </div>
                            </li>
                            <li class="col-lg-4">
                                <div class="text-center">
                                    <a href="">
                                        <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/wheel-sp1.png" alt=""/>
                                    </a>
                                    <div class="detail">
                                        <a class="title" href="">ICE ZERO</a>
                                        <p class="description-prod">Kích thước: 205/45R17 / TT - TĐ: 88W
                                            Made in ROMANI</p>
                                        <p class="price"><span>$234.79</span></p>
                                    </div>
                                </div>
                            </li>
                            <li class="col-lg-4">
                                <div class="text-center">
                                    <a href="">
                                        <img class="" src="<?php echo get_template_directory_uri(); ?>/assets/images/wheel-sp1.png" alt=""/>
                                    </a>
                                    <div class="detail">
                                        <a class="title" href="">ICE ZERO</a>
                                        <p class="description-prod">Kích thước: 205/45R17 / TT - TĐ: 88W
                                            Made in ROMANI</p>
                                        <p class="price"><span>$234.79</span></p>
                                    </div>
                                </div>
                            </li>
                            <div class="clear"></div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php get_footer();?>