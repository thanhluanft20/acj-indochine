<?php
function acj_add_images_size(){
    add_image_size( 'single_post', 870,350 );
    add_image_size( 'first_news_post', 570,200 );
    add_image_size( 'acj_reads', 115,115 );
    add_image_size( 'normal_post', 263,200 );
    add_image_size( 'product-small', 100,110 );
    add_image_size( 'product-normal', 9999, 168, false);

}
add_action( 'after_setup_theme', 'acj_add_images_size' );

function acj_excerpt($limit) {
    return wp_trim_words(get_the_excerpt(), $limit);
}
add_action( 'init', 'woocommerce_clear_cart_url' );
function woocommerce_clear_cart_url() {
    global $woocommerce;

    if ( isset( $_GET['empty-cart'] ) ) {
        $woocommerce->cart->empty_cart();
    }
}

