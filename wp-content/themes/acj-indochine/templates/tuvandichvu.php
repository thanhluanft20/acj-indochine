<?php
/*
 * Template Name: Tu Van Ky Thuat
 */
?>

<?php get_header(); ?>


<div class="container-fluid">
    <div class="top-banner">
        <?php
        while(have_posts()):the_post();
            echo get_post_meta( get_the_ID(), '_page_top_setting_content', true );
        endwhile;
        ?>
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column">
                    <?php
                    $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                    $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                    $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                    if(empty($page_title)){ $page_title = get_the_title(); }
                    if(empty($description)){ $description = strip_tags(get_the_excerpt()); }
                    ?>
                    <h1 class="title"><?php echo $page_title; ?></h1>
                    <p class="description"><?php echo $description; ?></p>
                </div>
                <div class="col-lg-6 col-md-6 col-sm-6 col-xm-6 column text-right">
                    <?php do_action( 'theme_breadcrumb' ); ?>
                </div>
            </div>
        </div>
    </div><!--END breadcrumbs-->
</div>
<div class="container tuvankythuat">
    <div class="main col-md-9 ">
        <div>
             <?php
            $args = array (
                'post_type'                 => 'tuvankythuat',
                'orderby'                   => 'date',
                'posts_per_page'            => 6,
                'paged'                     => get_query_var('paged')

            );
            $the_query = new WP_Query( $args );
            if ( $the_query->have_posts() ) {
                ?>
            <div class="row">
                <?php
                while ( $the_query->have_posts() ) {
                $the_query->the_post();
                ?>
                    <div class="item-tvkt col-lg-12 col-md-6 col-sm-6 col-xs-12">
                        <div class="row">
                            <div class="col-md-4">
                                <?php the_post_thumbnail();?>
                            </div>
                            <div class="col-md-8">
                                <p class="out-title"><a href="<?php echo get_the_permalink();?>" class="title" data-original-title="" title=""><?php echo get_the_title();?></a></p>
                                <p class="cont"><?php echo get_the_excerpt();?></p>
                                <a href="<?php echo get_the_permalink();?>" class="read-more" data-original-title="" title=""> <?php _e('đọc thêm',THEMEDOMAIN);?><span class="glyphicon glyphicon-menu-right"></span></a>
                            </div>
                            <div class="clear"></div>
                            <div class="col-md-12"><div class="line-custom"></div></div>
                        </div>
                    </div>

                <?php                                                                                                                                                                     }
                } else {
                    // no posts found
                }
                /* Restore original Post Data */
                wp_reset_postdata();?>
            </div>
        </div>
        <nav class="text-right">
            <?php echo wp_pagenavi(array( 'query' => $the_query ));?>

        </nav>
    </div><!--END main-->
    <div class="sidebar col-md-3" id="sidebar">
        <div class="widget">
            <div class="search">
                <form role="search" method="get" id="searchform"
                      class="searchform" action="<?php echo esc_url( home_url( '/' ) ); ?>">
                    <input type="text" placeholder="<?php _e('Nhập từ khoá tìm kiếm...', THEMEDOMAIN); ?>" id="search" value="<?php echo get_search_query(); ?>" name="s" kl_virtual_keyboard_secure_input="on">
                    <button type="submit"><span class="glyphicon glyphicon-search"></span></button>
                </form>
            </div>
        </div>
        <div class="widget tintuc">
            <div class="heading-title">
                <h3><span><?php _e('Tin tức đọc nhiều nhất', THEMEDOMAIN)?></span></h3>
            </div>
            <div class="ul-widget">
                <ul class="ul-widget-info">
                    <?php
                    $argc = array(
                        'posts_per_page'    => 5,
                        'posts_type'        =>'post',
                        'meta_key'          => '_acj_view',
                        'orderby'           => 'meta_value_number',
                        'order'             => 'DESC'
                    );

                    $the_query = new WP_Query( $argc );
                    if ( $the_query->have_posts() ) {
                        while ( $the_query->have_posts() ) {
                            $the_query->the_post();
                            ?>
                            <li>
                                <div class="pr-img">
                                    <a title="" data-original-title="" href="<?php the_permalink();?>"><?php the_post_thumbnail('acj_reads');?></a>

                                </div>
                                <div class="pr-des">
                                    <p>
                                        <a title="" data-original-title="" href="<?php the_permalink();?>"><?php the_title();?></a>
                                    </p>
                                    <p>
                                        <?php echo wp_trim_words(get_the_excerpt(), 10); ?>
                                    </p>
                                    <a class="read-more" href="<?php the_permalink();?>"><?php _e('Đọc thêm',THEMEDOMAIN);?><span class="glyphicon glyphicon-menu-right"></span></a>
                                </div>
                            </li>
                        <?php                                                                                                                                                                     }
                    } else {
                        // no posts found
                    }
                    /* Restore original Post Data */
                    wp_reset_postdata();?>
                </ul>
            </div>

        </div>

        <div class="widget">
            <div class="heading-title">
                <h3><span><?php _e( "Bán chạy nhất", THEMEDOMAIN ); ?></span></h3>
            </div>
            <div class="widget-products">
                <?php
                global $product;
                $args = array(
                    'post_type'         => 'product',
                    'posts_per_page'    => 4,
                    'meta_key'          => 'total_sales',
                    'orderby'           => 'meta_value_num'
                );
                $loop = new WP_Query( $args );

                if ( $loop->have_posts() ) {?>
                    <ul class="ul-widget-products">
                        <?php
                        while ( $loop->have_posts() ) : $loop->the_post();
                            $price = get_post_meta( get_the_ID(), '_regular_price', true);
                            $sale = get_post_meta( get_the_ID(), '_sale_price', true);
                            $currency = get_post_meta( get_the_ID(), 'woocommerce_currency', true);
                            global $product;
                            ?>

                            <li>
                                <div class="pr-img">
                                    <a href="<?php  the_permalink(); ?>"><?php the_post_thumbnail('product-small');?></a>
                                </div>
                                <div class="pr-des">
                                    <p>
                                        <?php the_excerpt();?>
                                    </p>
                                    <p>
                                        <a href="<?php  the_permalink(); ?>" class="widget-price-button"><?php echo $product->get_price_html(); ?></a>
                                    </p>
                                </div>
                            </li>
                        <?php endwhile;?>
                    </ul>
                <?php }
                wp_reset_postdata();
                ?>
            </div><!-- END .widget-products -->
        </div><!--END widget-->
    </div><!--END sidebar-->
</div>
<?php get_footer();?>