<div class="search-vehicle">
    <h3><?php _e( 'Tìm lốp xe phù hợp', THEMEDOMAIN ); ?></h3>
    <div class="follow-kind">
        <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
            <h4><?php _e( 'Tìm theo xe', THEMEDOMAIN ); ?></h4>
            <ul class="nav nav-tabs">
                <?php $terms = get_terms( 'loai_xe', array('hide_empty'=> false, 'orderby'=> 'term_id', 'order'=>'DESC') ); ?>
                <?php $firstTerm = ''; if( !empty( $terms ) ): ?>
                    <?php $i=1; foreach( $terms as $term ): $class = ''; if( $i==1 ){ $class = 'active'; $firstTerm = $term->term_id;} ?>
                        <?php
                            $thumbnail_id = absint( get_term_meta( $term->term_id, 'acj_tax_thumbnail_id', true ) );
                            if ( $thumbnail_id ) {
                                $thumb = wp_get_attachment_image_src( $thumbnail_id, array( 100, 40 ) );
                                $image = $thumb[0];
                            } else {
                                $image = get_template_directory_uri().'/assets/images/placeholder.png';
                            }

                            $thumbnail_gray_id = absint( get_term_meta( $term->term_id, 'acj_tax_thumbnail_gray_id', true ) );
                            if ( $thumbnail_gray_id ) {
                                $thumb = wp_get_attachment_image_src( $thumbnail_gray_id, array( 100, 40 ) );
                                $image_gray = $thumb[0];
                            } else {
                                $image_gray = get_template_directory_uri().'/assets/images/placeholder.png';
                            }
                        ?>
                        <li class="col-lg-4 col-md-4 col-sm-4 col-xs-12 <?=$class;?>">
                           <a class="" data-term="<?=$term->term_id;?>" href="#">
                                <span class="img-active"><img src="<?php echo $image; ?>" alt="<?=$term->name;?>"/></span>
                                <span class="img-not-active"><img src="<?php echo $image_gray; ?>" alt="<?=$term->name;?>"/></span>
                                <span class="name"><?=$term->name;?></span>
                                <div class="clear"></div>
                            </a>
                        </li>
                    <?php $i++; endforeach; ?>
                <?php endif; ?>
                <input type="hidden" name="loai_xe" value="<?=$firstTerm;?>" />
                <input type="hidden" name="search_type" value="product" />
            </ul>
            <div class="tab-content">
                <div id="home" class="tab-pane fade in active">
                    <select name="hang_xe" class="hang-xe">
                        <option value=""><?php _e( "Hãng xe", THEMEDOMAIN ); ?></option>
                        <?php $term_hang_xe = get_terms( 'hang_xe', array('hide_empty'=> false) ); ?>
                        <?php if( !empty( $term_hang_xe ) ): ?>
                            <?php foreach( $term_hang_xe as $term ): ?>
                                <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <select name="mau_xe" class="mau-xe">
                        <option value=""><?php _e( "Mẫu xe", THEMEDOMAIN ); ?></option>
                        <?php $term_hang_xe = get_terms( 'mau_xe', array('hide_empty'=> false) ); ?>
                        <?php if( !empty( $term_hang_xe ) ): ?>
                            <?php foreach( $term_hang_xe as $term ): ?>
                                <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                            <?php endforeach; ?>
                        <?php endif; ?>
                    </select>
                    <select name="nam_san_xuat" class="nam-san-xuat">
                        <option value=""><?php _e( "Năm sản xuất", THEMEDOMAIN ); ?></option>
                        <?php for( $i = 1970; $i <= date('Y'); $i++ ): ?>
                            <option><?php echo $i; ?></option>
                        <?php endfor; ?>
                    </select>
                    <input type="submit" value="<?php _e('TÌM LỐP XE', THEMEDOMAIN ); ?>" class="submid-vehiche"/>
                </div>
            </div>
        </form>
    </div>
    <div class="follow-size">
        <h4><?php _e('Tìm theo size', THEMEDOMAIN ); ?></h4>
        <form role="search" method="get" class="search-form" action="<?php echo home_url( '/' ); ?>">
            <input type="hidden" name="search_type" value="product" />
            <div class="select-parent">
                <select class="select" name="product_size">
                    <?php $term_hang_xe = get_terms( 'kich_thuoc', array( 'hide_empty'=> false, 'parent' => 0 ) ); ?>
                    <?php if( !empty( $term_hang_xe ) ): ?>
                        <?php foreach( $term_hang_xe as $term ): ?>
                            <option value="<?php echo $term->term_id; ?>"><?php echo $term->name; ?></option>
                            <?php
                            $child_args = array('parent'=> $term->term_id);
                            $child_terms = get_terms( 'kich_thuoc', $child_args );
                            ?>
                            <?php if( !empty( $child_terms ) ): ?>
                                <?php foreach( $child_terms as $child_term ): ?>
                                    <option value="<?=$child_term->term_id;?>">--<?=$child_term->name;?></option>
                                <?php endforeach; ?>
                            <?php endif; ?>
                        <?php endforeach; ?>
                    <?php endif; ?>
                </select>
            </div>
            <input type="submit" value="<?php _e('TÌM LỐP XE', THEMEDOMAIN ); ?>" class="submid-vehiche"/>
        </form>
    </div>
</div>
<?php if ( is_active_sidebar( 'shop-sidebar' ) ) : ?>
    <?php dynamic_sidebar( 'shop-sidebar' ); ?>
<?php endif; ?>
<script type="text/javascript">
    jQuery( document).ready(function ($){
        $(".follow-kind .nav.nav-tabs li a").click(function(){
            var termID = $(this).data('term');
            $('input[name="loai_xe"]').val(termID);
            $('.follow-kind .nav.nav-tabs li').removeClass('active')
            $(this).parent().addClass('active');
            return false;
        });
    });
</script>
<style type="text/css">
    .follow-kind .nav.nav-tabs li a span.img{
        height: 50px;
        display: inline-block;
    }
    .follow-kind .nav.nav-tabs li a span.name{
        text-transform: uppercase;
    }
</style>