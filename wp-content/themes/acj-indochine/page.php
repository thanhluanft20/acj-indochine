<?php get_header(); ?>
    <div class="container">
        <?php while(have_posts()): the_post(); ?>
            <?php
            // Include the page content template.
            get_template_part( 'content', 'page' );

            // If comments are open or we have at least one comment, load up the comment template.
            if ( comments_open() || get_comments_number() ) :
                comments_template();
            endif;
            ?>
        <?php endwhile; ?>
    </div><!-- END .container -->
<?php get_footer(); ?>