<?php
/**
 * The Template for displaying product archives, including the main shop page which is a post type archive.
 *
 * Override this template by copying it to yourtheme/woocommerce/archive-product.php
 *
 * @author 		WooThemes
 * @package 	WooCommerce/Templates
 * @version     2.0.0
 */

if ( ! defined( 'ABSPATH' ) ) {
    exit; // Exit if accessed directly
}
get_header(); ?>
<div class="container-flue">
    <div class="top-banner">
        <img class="img-responsive" src="<?php echo get_template_directory_uri(); ?>/assets/images/banner-sanpham.png" alt=""/>
    </div>
    <div class="breadcrumbs">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 col-md-8 col-sm-6 col-xm-6 column">
                    <?php
                    $content = get_post_meta( get_the_ID(), '_page_top_setting_content', true );
                    $page_title = get_post_meta( get_the_ID(), '_page_top_setting_title', true );
                    $description = get_post_meta( get_the_ID(), '_breadcrumb_description', true );
                    if(empty($page_title)){ $page_title = __( 'Sản phẩm', THEMEDOMAIN ); }
                    if(empty($description)){ $description = __( 'ACJ INDOCHINE hân hạnh giới thiệu đến quý khách các sản phẩm của công ty', THEMEDOMAIN ); }
                    ?>
                    <h1 class="title"><?php echo $page_title; ?></h1>
                    <p class="description"><?php echo $description; ?></p>
                </div>
                <div class="col-lg-4 col-md-4 col-sm-6 col-xm-6 column text-right">
                    <?php do_action( 'theme_breadcrumb' ); ?>
                </div>
            </div>
        </div>
    </div><!--END breadcrumbs-->
</div><!--END container-flue-->
<div class="container sanpham">
    <div class="row">
        <div class="col-lg-3">
            <?php
            /**
             * woocommerce_sidebar hook
             *
             * @hooked woocommerce_get_sidebar - 10
             */
            do_action( 'woocommerce_sidebar' );
            ?>
        </div>
        <div class="col-lg-9">
            <?php
            /**
             * woocommerce_before_main_content hook
             *
             * @hooked woocommerce_output_content_wrapper - 10 (outputs opening divs for the content)
             * @hooked woocommerce_breadcrumb - 20
             */
            do_action( 'woocommerce_before_main_content' );
            ?>
            <?php if ( apply_filters( 'woocommerce_show_page_title', true ) ) : ?>

                <div class="heading-title product-archive-title">
                    <h3><span><?php woocommerce_page_title(); ?></span></h3>
                </div>

            <?php endif; ?>
            <div class="row sanphams">
                <?php
                /**
                 * woocommerce_archive_description hook
                 *
                 * @hooked woocommerce_taxonomy_archive_description - 10
                 * @hooked woocommerce_product_archive_description - 10
                 */
                do_action( 'woocommerce_archive_description' );
                $args = array( 'post_type'=> 'product', 'posts_per_page' => 12 );
                if( isset( $_REQUEST['loai_xe'] ) && !empty( $_REQUEST['loai_xe'] ) ){
                    $args['tax_query'][] = array(
                        'taxonomy' => 'loai_xe',
                        'field'    => 'ID',
                        'terms'    => $_REQUEST['loai_xe']
                    );
                }
                if( isset( $_REQUEST['mau_xe'] ) && !empty( $_REQUEST['mau_xe'] ) ){
                    $args['tax_query'][] = array(
                        'taxonomy' => 'mau_xe',
                        'field'    => 'ID',
                        'terms'    => $_REQUEST['mau_xe']
                    );
                }
                if( isset( $_REQUEST['nam_san_xuat'] ) && !empty( $_REQUEST['nam_san_xuat'] ) ){
                    $args['meta_query'][] = array(
                        'key'     => '_nam_san_xuat',
                        'value'   => $_REQUEST['nam_san_xuat'],
                        'compare' => '=',
                    );
                }
                if( isset( $_REQUEST['product_size'] ) && !empty( $_REQUEST['product_size'] ) ){
                    $args['tax_query'][] = array(
                        'taxonomy' => 'kich_thuoc',
                        'field'    => 'ID',
                        'terms'    => $_REQUEST['product_size']
                    );
                }
                if( sizeof( $args['tax_query'] ) ){
                    $args['tax_query']['relation'] = 'AND';
                }
                if( sizeof( $args['meta_query'] ) ){
                    $args['meta_query']['relation'] = 'AND';
                }
                query_posts( $args );
                ?>

                <?php if ( have_posts() ) : ?>
                    <div class="col-md-12">
                        <?php
                        /**
                         * woocommerce_before_shop_loop hook
                         *
                         * @hooked woocommerce_result_count - 20
                         * @hooked woocommerce_catalog_ordering - 30
                         */
                        do_action( 'woocommerce_before_shop_loop' );
                        ?>
                    </div>

                    <?php woocommerce_product_loop_start(); ?>

                    <?php while ( have_posts() ) : the_post(); ?>

                        <?php wc_get_template_part( 'content', 'product' ); ?>

                    <?php endwhile; // end of the loop. ?>

                    <?php woocommerce_product_loop_end(); ?>

                    <?php
                    /**
                     * woocommerce_after_shop_loop hook
                     *
                     * @hooked woocommerce_pagination - 10
                     */
                    do_action( 'woocommerce_after_shop_loop' );
                    ?>

                <?php elseif ( ! woocommerce_product_subcategories( array( 'before' => woocommerce_product_loop_start( false ), 'after' => woocommerce_product_loop_end( false ) ) ) ) : ?>

                    <div class="col-md-12">
                        <?php wc_get_template( 'loop/no-products-found.php' ); ?>
                    </div>

                <?php endif; ?>

                <?php
                /**
                 * woocommerce_after_main_content hook
                 *
                 * @hooked woocommerce_output_content_wrapper_end - 10 (outputs closing divs for the content)
                 */
                do_action( 'woocommerce_after_main_content' );
                ?>

            </div>
        </div>
    </div>
</div>
<?php get_footer();?>
